$(function  ()  {
  // Currency
  $(".currency").autoNumeric({ aSep: ".", aDec: ",", vMax: "999999999999999", vMin: "0" });
  // Others
  $('.next_input').bind('keyup',function() {
      $(this).each(function() {
          var max = $(this).attr('maxlength');            
          var len = $(this).val().length;
          if(max == len) {
              $(this).nextAll('.next_input:first').focus();
              $(this).nextAll('.next_input:first').select();
          }
      });
  });

  $('.button_loading').bind('click',function(e) {
      e.preventDefault();
      $('#divLoading').addClass('show');
      location.href = $(this).attr('href');
  });

  $('.button-checkbox').each(function () {
    // Settings
    var $widget = $(this),
      $button = $widget.find('button'),
      $checkbox = $widget.find('input:checkbox'),
      color = $button.data('color'),
      komoditas_id = $button.data('komoditas-id'),
      harga_hari_ini = $button.data('harga-hari-ini'),
      harga_kemarin = $button.data('harga-kemarin'),
      settings = {
          on: {
              icon: 'glyphicon glyphicon-check'
          },
          off: {
              icon: 'glyphicon glyphicon-unchecked'
          }
      };

    // Event Handlers
    $button.on('click', function () {
      $checkbox.prop('checked', !$checkbox.is(':checked'));
      $checkbox.triggerHandler('change');
      updateDisplay();
    });
    $checkbox.on('change', function () {
      updateDisplay();
    });

    // Actions
    function updateDisplay() {
      var isChecked = $checkbox.is(':checked');

      // Set the button's state
      $button.data('state', (isChecked) ? "on" : "off");

      // Set the button's icon
      $button.find('.state-icon')
          .removeClass()
          .addClass('state-icon ' + settings[$button.data('state')].icon);

      // Update the button's color
      if (isChecked) {
          $button
              .removeClass('btn-primary')
              .addClass('btn-' + color + ' active');
          $('#harga_hari_ini_'+ komoditas_id).val(harga_kemarin);
      }
      else {
          $button
              .removeClass('btn-' + color + ' active')
              .addClass('btn-primary');
          $('#harga_hari_ini_'+ komoditas_id).val(harga_hari_ini);
      }
    }

    // Initialization
    function init() {

      updateDisplay();

      // Inject the icon if applicable
      if ($button.find('.state-icon').length == 0) {
          $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
      }
    }
    init();
  });
  
  //
  $('.cb-all').click(function (e) {
    $('.cb-item').prop('checked', this.checked);
    var a = $('.cb-item').data('id');
    console.log(a);
  });
  //
  $('#delete-all').bind('click',function(e) {
    var a = $(this).attr('data-action');
    var l = $('.cb-item:checked').length;
    if(l == 0) {
      swal({
              text: "Maaf, Data harap dipilih dahulu !",
              icon: "warning",
              button: "OK",
            });
      return false;
    } else {
      swal({
        title: "",
        text: "Apakah Anda yakin akan menghapus data terpilih ini ?",
        icon: "warning",
        buttons: {
                cancel: "Batal",
                sucess: {
                  text: "Lanjut",
                },
              },
      })
      .then((willProcess) => {
        if (willProcess) {
          $('#form-index').attr('action',a).submit();
        }
      });
    }   
  });
  //
  $('#activated-all').bind('click',function(e) {
    var a = $(this).attr('data-action');
    var l = $('.cb-item:checked').length;
    if(l == 0) {
      swal({
              text: "Maaf, Data harap dipilih dahulu !",
              icon: "warning",
              button: "OK",
            });
      return false;
    } else {
      swal({
        title: "",
        text: "Apakah Anda yakin akan mengaktifkan data terpilih ini ?",
        icon: "warning",
        buttons: {
                cancel: "Batal",
                sucess: {
                  text: "Lanjut",
                },
              },
      })
      .then((willProcess) => {
        if (willProcess) {
          $('#form-index').attr('action',a).submit();
        }
      });
    }   
  });
  //
  $('#non-activated-all').bind('click',function(e) {
    var a = $(this).attr('data-action');
    var l = $('.cb-item:checked').length;
    if(l == 0) {
      swal({
              text: "Maaf, Data harap dipilih dahulu !",
              icon: "warning",
              button: "OK",
            });
      return false;
    } else {
      swal({
        title: "",
        text: "Apakah Anda yakin akan menonaktifkan data terpilih ini ?",
        icon: "warning",
        buttons: {
                cancel: "Batal",
                sucess: {
                  text: "Lanjut",
                },
              },
      })
      .then((willProcess) => {
        if (willProcess) {
          $('#form-index').attr('action',a).submit();
        }
      });
    }   
  });
  //
  $('#process-all').bind('click',function(e) {
    var a = $(this).attr('data-action');
    var l = $('.cb-item-usulan:checked').length;
    var no_usulan = $('#no_usulan');
    if(no_usulan.val() == '') {
      swal({
              text: "No. Usulan Belum Diisi",
              icon: "warning",
              button: "OK",
            });
            no_usulan.focus();
            return false;
    }else{
      if(l == 0) {
        swal({
                text: "Maaf, Data harap dipilih dahulu !",
                icon: "warning",
                button: "OK",
              });
        return false;
      } else {
        swal({
          title: "",
          text: "Apakah Anda yakin akan memproses data terpilih ini ?",
          icon: "warning",
          buttons: {
                  cancel: "Batal",
                  sucess: {
                    text: "Lanjut",
                  },
                },
        })
        .then((willProcess) => {
          if (willProcess) {
            $('#form-index').attr('action',a).submit();
          }
        });
      } 
    } 
  });
  $('.delete-item').bind('click',function(e) {
    e.preventDefault();
    swal({
      title: "",
      text: "Apakah anda yakin akan menghapus data ini ?",
      icon: "warning",
      buttons: {
              cancel: "Batal",
              danger: {
                text: "Hapus",
              },
            },
    })
    .then((willProcess) => {
      if (willProcess) {
        location.href = $(this).attr('href');
      }
    });
  }); 
  //
  $('.truncate-data').bind('click',function(e) {
    e.preventDefault();
    swal({
      title: "",
      text: "Apakah anda yakin akan menghapus semua data ?",
      icon: "warning",
      buttons: {
              cancel: "Batal",
              danger: {
                text: "Hapus",
              },
            },
    })
    .then((willProcess) => {
      if (willProcess) {
        location.href = $(this).attr('href');
      }
    });
  }); 
  //
  $('.activated-item').bind('click',function(e) {
    e.preventDefault();
    swal({
      title: "",
      text: "Apakah Anda yakin ingin mengaktifkan Pengguna ini ?",
      icon: "warning",
      buttons: {
              cancel: "Batal",
              success: {
                text: "Aktifkan",
              },
            },
    })
    .then((willProcess) => {
      if (willProcess) {
        location.href = $(this).attr('href');
      }
    });
  }); 
  //
  $('.non-activated-item').bind('click',function(e) {
    e.preventDefault();
    swal({
      title: "",
      text: "Apakah Anda yakin ingin menonaktifkan Pengguna ini ?",
      icon: "warning",
      buttons: {
              cancel: "Batal",
              danger: {
                text: "Non-Aktifkan",
              },
            },
    })
    .then((willProcess) => {
      if (willProcess) {
        location.href = $(this).attr('href');
      }
    });
  }); 
});
//
function commafy(num) {
    var str = num.toString().split('.');
    if (str[0].length >= 5) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1.');
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');
}
//
function validate_file_upload_size(limit_size, file_size, attribut) {    
  if(limit_size != '') {
    var limit_size_a = limit_size;
    var limit_size_b = parseFloat(limit_size) * 1024;
    //
    if(parseFloat(limit_size_b) < parseFloat(file_size)){
          alert('Maaf, Maksimal file yang diupload '+limit_size_a+' Kb !');
          $(attribut).val('');
          return false;
      } else {
          return true;
      }
  } else {
    return true;
  }     
}

$(function  ()  {
  //Preloading
  paceOptions = {
    startOnPageLoad: true,
    ajax: false, // disabled
    document: false, // disabled
    eventLag: false, // disabled
    elements: false
  };
  
  //
  $('.login-link').click(function(e) {
    e.preventDefault();
    href = $(this).attr('href');
    
    $('.login-wrapper').addClass('fadeOutUp');

    setTimeout(function() {
      window.location = href;
    }, 900);
      
    return false; 
  });
  
  //Sidebar menu dropdown
  $('aside li').hover(
       function(){ $(this).addClass('open') },
       function(){ $(this).removeClass('open') }
  )
  
  //Collapsible Sidebar Menu
  $('.openable > a').click(function() { 
    if(!$('#wrapper').hasClass('sidebar-mini')) {
      if( $(this).parent().children('.submenu').is(':hidden') ) {
        $(this).parent().siblings().removeClass('open').children('.submenu').slideUp();
        $(this).parent().addClass('open').children('.submenu').slideDown();
      }
      else  {
        $(this).parent().removeClass('open').children('.submenu').slideUp();
      }
    }
    
    return false;
  });
    
  //Toggle Menu
  $('#sidebarToggle').click(function()  {
    $('#wrapper').toggleClass('sidebar-display');
    $('.main-menu').find('.openable').removeClass('open');
    $('.main-menu').find('.submenu').removeAttr('style');
  });
  
  $('#sizeToggle').click(function() {
  
    $('#wrapper').off("resize");
  
    $('#wrapper').toggleClass('sidebar-mini');
    $('.main-menu').find('.openable').removeClass('open');
    $('.main-menu').find('.submenu').removeAttr('style');
  });

  //show/hide menu
  $('#menuToggle').click(function() {
    $('#wrapper').toggleClass('sidebar-hide');
    $('.main-menu').find('.openable').removeClass('open');
    $('.main-menu').find('.submenu').removeAttr('style');
  });
  
  //fixed Sidebar
  $('#fixedSidebar').click(function() {
    if($(this).prop('checked')) {
      $('aside').addClass('fixed');
    } 
    else  {
      $('aside').removeClass('fixed');
    }
  });
  
  //Inbox sidebar (inbox.html)
  $('#inboxMenuToggle').click(function()  {
    $('#inboxMenu').toggleClass('menu-display');
  });
  
  //Collapse panel
  $('.collapse-toggle').click(function()  {
  
    $(this).parent().toggleClass('active');
  
    var parentElm = $(this).parent().parent().parent().parent();
    
    var targetElm = parentElm.find('.panel-body');
    
    targetElm.toggleClass('collapse');
  });
  
  //Number Animation
  var currentVisitor = $('#currentVisitor').text();
  
  $({numberValue: 0}).animate({numberValue: currentVisitor}, {
    duration: 2500,
    easing: 'linear',
    step: function() { 
      $('#currentVisitor').text(Math.ceil(this.numberValue)); 
    }
  });
      
  var currentBalance = $('#currentBalance').text();
  
  $({numberValue: 0}).animate({numberValue: currentBalance}, {
    duration: 2500,
    easing: 'linear',
    step: function() { 
      $('#currentBalance').text(Math.ceil(this.numberValue)); 
    }
  });
  
  //Refresh Widget
  $('.refresh-widget').click(function() {
    var _overlayDiv = $(this).parent().parent().parent().parent().find('.loading-overlay');
    _overlayDiv.addClass('active');
    
    setTimeout(function() {
      _overlayDiv.removeClass('active');
    }, 2000);
    
    return false;
  });
    
  //Check all checkboxes
  $('#chk-all').click(function()  {
    if($(this).is(':checked'))  {
      $('.inbox-panel').find('.chk-item').each(function() {
        $(this).prop('checked', true);
        $(this).parent().parent().addClass('selected');
      });
    }
    else  {
      $('.inbox-panel').find('.chk-item').each(function() {
        $(this).prop('checked' , false);
        $(this).parent().parent().removeClass('selected');
      });
    }
  });
  
  $('.chk-item').click(function() {
    if($(this).is(':checked'))  {
      $(this).parent().parent().addClass('selected');   
    }
    else  {
      $(this).parent().parent().removeClass('selected');
    }
  });
  
  $('.chk-row').click(function()  {
    if($(this).is(':checked'))  {
      $(this).parent().parent().parent().addClass('selected');    
    }
    else  {
      $(this).parent().parent().parent().removeClass('selected');
    }
  });
  
  //Hover effect on touch device
  $('.image-wrapper').bind('touchstart', function(e) {
    $('.image-wrapper').removeClass('active');
    $(this).addClass('active');
    });
  
  //Dropdown menu with hover
  $('.hover-dropdown').hover(
       function(){ $(this).addClass('open') },
       function(){ $(this).removeClass('open') }
  )

  //upload file
  $('.upload-demo').change(function() {
    var filename = $(this).val().split('\\').pop();
    $(this).parent().find('span').attr('data-title',filename);
    $(this).parent().find('label').attr('data-title','Change file');
    $(this).parent().find('label').addClass('selected');
  });

  $('.remove-file').click(function()  {
    $(this).parent().find('span').attr('data-title','No file...');
    $(this).parent().find('label').attr('data-title','Select file');
    $(this).parent().find('label').removeClass('selected');

    return false;
  }); 

  //theme setting
  $("#theme-setting-icon").click(function() { 
    if($('#theme-setting').hasClass('open'))  {
      $('#theme-setting').removeClass('open');
      $('#theme-setting-icon').removeClass('open');
    }
    else  {
      $('#theme-setting').addClass('open');
      $('#theme-setting-icon').addClass('open');
    }

    return false;
  });
  
  //to do list
  $('.task-finish').click(function()  {
    if($(this).is(':checked'))  {
      $(this).parent().parent().addClass('selected');         
    }
    else  {
      $(this).parent().parent().removeClass('selected');
    }
  });

  //Delete to do list
  $('.task-del').click(function() {     
    var activeList = $(this).parent().parent();

    activeList.addClass('removed');
        
    setTimeout(function() {
      activeList.remove();
    }, 1000);
      
    return false;
  });
  
  // Popover
    $("[data-toggle=popover]").popover();
  
  // Tooltip
    $("[data-toggle=tooltip]").tooltip();
  
});

$(window).scroll(function(){
    
   var position = $(window).scrollTop();
  
   //Display a scroll to top button
   if(position >= 200)  {
    $('#scroll-to-top').attr('style','bottom:8px;');  
   }
   else {
    $('#scroll-to-top').removeAttr('style');
   }
});




$(document).ready(function() {
    $('.chosen-select').select2();
});
//


  // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  // ga('create', 'UA-46156385-1', 'cssscript.com');
  // ga('send', 'pageview');

function convertToRupiah(angka) {
  var rupiah = '';    
  var angkarev = angka.toString().split('').reverse().join('');
  for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
  return rupiah.split('',rupiah.length-1).reverse().join('');
}

// Disable button after click
window.onbeforeunload = function () {
    var inputs = document.getElementsByTagName("BUTTON");
    for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].id == "download-salinan") {
        inputs[i].disabled = true;
        inputs[i].innerHTML = '<b>Download Sedang Di Proses <i class="fa fa-spinner fa-pulse fa-fw"></i></b>';
      }else if (inputs[i].id == "loading") {
        inputs[i].disabled = true;
        inputs[i].innerHTML = '<b>Loading <i class="fa fa-spinner fa-pulse fa-fw"></i></b>';
      }
    }
};

// Sweeralert
$(document).ready(function(){
    $(".logout").click(function(e) {
    var location_href = document.getElementsByClassName('location-logout')[0].href;
      e.preventDefault()
        swal({
          title: "",
          text: "Apakah anda yakin akan keluar dari aplikasi ini ?",
          icon: "warning",
          buttons: {
            cancel: "❌ Batal",
            danger: {
              text: "Logout",
            },
          },
        })
        .then((willDelete) => {
            if (willDelete) {
                window.location=location_href;
                    swal({
                      title: "",
                      text: "Berhasil Logout",
                      icon: "success",
                      buttons: "Oke",
                    })
            } else {
                
            }
        });
    });
});