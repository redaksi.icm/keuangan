/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : keuangan_db

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 04/06/2019 14:08:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for _author
-- ----------------------------
DROP TABLE IF EXISTS `_author`;
CREATE TABLE `_author`  (
  `author_id` int(3) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `author_st` tinyint(1) NOT NULL COMMENT '1:active,0:not-active',
  PRIMARY KEY (`author_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 35 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of _author
-- ----------------------------
INSERT INTO `_author` VALUES (1, 'Administrator', 1);
INSERT INTO `_author` VALUES (2, 'Redaksi', 1);
INSERT INTO `_author` VALUES (4, 'g', 1);
INSERT INTO `_author` VALUES (5, 'f', 1);
INSERT INTO `_author` VALUES (6, 'tes', 1);
INSERT INTO `_author` VALUES (7, 'admin', 1);
INSERT INTO `_author` VALUES (8, 'administra', 1);
INSERT INTO `_author` VALUES (9, 'Bidang Sarpras', 1);
INSERT INTO `_author` VALUES (10, 'Dinas Pendidikan Kabupaten Kebumen', 1);
INSERT INTO `_author` VALUES (11, 'kurikulum dikdas', 1);
INSERT INTO `_author` VALUES (12, 'kurikulum SD', 1);
INSERT INTO `_author` VALUES (13, 'paud', 1);
INSERT INTO `_author` VALUES (14, 'dikmas', 1);
INSERT INTO `_author` VALUES (15, 'sadasd', 1);
INSERT INTO `_author` VALUES (16, 'asdasd', 1);
INSERT INTO `_author` VALUES (17, 'Administrator1', 1);
INSERT INTO `_author` VALUES (33, '1', 1);
INSERT INTO `_author` VALUES (34, 'Alfian Muhammad Ardianto', 1);

-- ----------------------------
-- Table structure for _post
-- ----------------------------
DROP TABLE IF EXISTS `_post`;
CREATE TABLE `_post`  (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(3) NOT NULL,
  `post_tp` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '1:post,2:page',
  `post_cat` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '1:news,2:agenda,3:info',
  `post_title` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `post_date` datetime(0) NOT NULL,
  `post_content` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `post_url` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'permalink',
  `post_hit` int(11) NOT NULL,
  `post_st` tinyint(1) NOT NULL COMMENT '1:publish,2:draft,3:not-active',
  `comment_st` tinyint(1) NOT NULL COMMENT '1:open,0:closed',
  `comment_count` int(5) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `author_id` int(3) NULL DEFAULT NULL,
  `posting_st` tinyint(1) NULL DEFAULT NULL,
  `pin_st` tinyint(1) NULL DEFAULT NULL,
  `kebumenkab_st` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of _post
-- ----------------------------
INSERT INTO `_post` VALUES (8, 27, '', '', 'Lowongan Account Officer (AO) dan Finnance and Administration (FAO) PT PNM MEKAR', '2019-01-22 15:25:49', '<p>Peluang berkarir di perusahaan BUMN PT Permodalan Nasional Madani yang bergerak dalam bidang pembiaayaan kelompok untuk perempuan plaku usaha mikro. </p><p>Posisi : Account Officer dan Finnance and Administration (FAO)</p><p> KUALIFUIKASI :  </p><p>1.Perempuan Usia 18-22 tahun  </p><p>2.Lulusan SMA/K Segala Jurusan (Khusus FAO jurusan SMK Akuntansi)  </p><p>3.Memiliki E-KTP, SIM C, dan mampu mengendarai motor  </p><p>4.Berkelakuan Baik dan Mampu Bersosialisasi  </p><p>5.Belum menikah /Hamil  </p><p><br></p><p>FASILITAS & BENEFIT  : </p><p>1.Gaji Awal UMK  </p><p>2.Mess,Sembako & THR  </p><p>3.Kendaraan Operasional & Bensin  </p><p>4.BPSJ Kesehatan & BPJS Ketenagakerjaan  </p><p>5.Asuransi Jiwa dan Jenjang Karir  </p><p><br></p><p>Kirimkan lamaran Lengkap ke Kantor Cabang PNM Mekar sebagaimana alamat terlampir pada gambar. <br><br></p>	', 'lowongan-account-officer-ao-dan-finnance-and-administration-fao-pt-pnm-mekar', 58, 1, 0, 0, 0, 1, NULL, 1, NULL);
INSERT INTO `_post` VALUES (10, 27, '', '', 'Bank Jateng Kebumen menggandeng kembali PLUT KUMKM Kebumen dalam pelatihan Simulasi Bisnis Bagi Usaha Mikro', '2019-01-22 15:15:24', '<p><span class=\"html-tag\" style=\"font-family: monospace; font-size: medium; white-space: pre-wrap;\"></span></p><p>\r\n\r\n<span style=\"font-variant-ligatures: normal;font-variant-caps: normal;\r\norphans: 2;text-align:start;white-space:pre-wrap;widows: 2;-webkit-text-stroke-width: 0px;\r\ntext-decoration-style: initial;text-decoration-color: initial;float:none;\r\nword-spacing:0px\"></span></p><p class=\"MsoNormal\"><span class=\"html-tag\"><span style=\"font-size:13.5pt;\r\nline-height:115%;font-family:\" arial\",\"sans-serif\"\"=\"\"></span></span></p><p><span style=\"font-size: 18px; white-space: pre-wrap;\"></span></p><p><font face=\"monospace\" size=\"3\"><span style=\"white-space: pre-wrap;\"><p>KEBUMEN- Kepala Bidang Usaha Mikro dan Hubungan Industrial Dinas Tenaga Kerja dan KUKM Kab. Kebumen Akhmad Sudiyono membuka kegiatan pelatihan simulasi bisnis mikro UMKM (Micro Business Simulation) yang merupakan kerjasama antara PLUT KUMKM Kebumen dengan BPD Bank Jateng Kebumen tanggal 22-23 November  2018 di lt. 2 PLUT KUMKM Kabupaten Kebumen, diikuti 11 Pelaku UMKM se- Kabupaten Kebumen.</p><p><br>Kegiatan selama dua hari tersebut berbentuk pelatihan manajemen keuangan berupa simulasi/praktek dalam perhitungan keuangan usaha dengan tools dari Jerman dengan peralatan yang digunakan berupa buku panduan, selembar kertas besar untuk perhitungan, dan koin yaitu biasa di sebut cuila, diharapkan praktek simulasi ini bisa memberi pemahaman bagi UMKM pentingnya keuangan usaha dan pemisahan keuangan pribadi dan keuangan usaha.” Kata Akhmad Sudiyono.</p><p><br> “Sinergitas antara PLUT-KUMKM dengan BPD Bank Jateng Kebumen dalam menyelenggarakan kegiatan seperti ini sejak akhir tahun 2016 ( 2 tahun) dengan penandatangan MoU antara Kepala Dinas Tenaga Kerja dan KUKM Kabupaten Kebumen dengan Pimpinan BPD Bank Jateng Cabang Kebumen. Hal ini sebagai upaya Dinas untuk menggandeng perbankan dan mengenalkan keberadaan PLUT KUMKM Kebumen bagi pelaku UMKM pada khusunya dan masyarakat Kebumen pada umumnya.” Ujar Akhmad Sudiyono.</p><p><br>Mohammad Yasin Narasumber Micro Simulation Bisnis dari BPD Bank Jateng Provinsi Jawa Tengah menyampaikan saya melatih pelaku UMKM di Kebumen sejak tahun 2017 dan sudah ada sekitar 100 UMKM di wilayah Kebumen. Pada tahun ini saya sudah melatih sebanyak 20 angkatan di wilayah Kedu Provinsi Jawa Tengah termasuk Kebumen” ucap M. Yasin.<br>Materi yang kami sampaikan ada 8 babak selama dua hari.  Setiap harinya dua babak simulasi dipraktekkan. Simulasi ini dibuat secara berkelompok dimana setiap kelompok ada 4 peserta dan semuanya berjumlah tiga kelompok. Materi yang diajarkan merupakan adopsi materi dari Bank Daerah Jerman yaitu Finanz Gruppe yang telah disesuaikan dengan kondisi dan culture Pelaku Usaha di Jawa Tengah, dimana materi tentang perhitungan keuangan usaha meliputi pencatatan transaksi, perhitungan laba rugi dan perhitungan perubahan modal. “ujar M. Yasin.<br>“Sinergitas yang terjalin antara BPD Bank Jateng Kebumen dengan PLUT KUMKM Kebumen sangat baik dan bisa dicontoh oleh daerah lain karena sudah menjalin kerjasama selama kurang lebih dua tahun. Peserta merupakan binaan dari PLUT Kebumen. Kami mengalami kesulitan kerjasama melaksanakan kegiatan MBS jika di suatu daerah tidak terdapat PLUT”. Ucap Yasin. (ags)<br></p><span style=\"white-space:pre\">	</span></span></font><br></p><p></p><span style=\"font-family:\" arial\",\"sans-serif\"\"=\"\"><o:p></o:p></span><p></p><p></p>', 'bank-jateng-kebumen-menggandeng-kembali-plut-kumkm-kebumen-dalam-pelatihan-simulasi-bisnis-bagi-usaha-mikro', 44, 1, 0, 0, 0, 1, NULL, 1, NULL);
INSERT INTO `_post` VALUES (12, 27, '', '', 'Kasi. Penempatan Tenaga Kerja Disnakerkukm Kab. Kebumen menghadiri undangan Refleksi Perlindungan Buruh Migran di Desbumi Summit', '2019-01-22 15:26:41', '<div>Acara yang dibuka secara resmi oleh Kepala Staf Kepresidenan RI Jenderal TNI<br></div><div>(Purna.) Dr. Moeldoko, S.IP dan dihadiri oleh 500 peserta dari berbagai desa<br></div><div>tersebut direncanakan akan berlangsung selama tiga hari. Dengan rancangan<br></div><div>beragam kegiatan mulai dari Dialog Kebijakan, Workshop Thematik, Pameran Produk<br></div><div>dan Ekspresi Budaya dan Eksposure ke DESBUMI Banyuwangi dan Jember.<p></p><p><br></p></div><div><p></p><p>BURUH migran perempuan, berdaya dari desa menjadi kata kunci dalam<br></p></div><div>merefleksikan perjuangan perlindungan buruh migran melalui inisiatif desa<br></div><div>peduli buruh migran (Desbumi) yang dibentuk oleh Migrant CARE sejak 2013.<br></div><div>Perjalanan lima tahun Desbumi mulai dari proses persiapan, pengelolaan dan<br></div><div>pengembangan inisiatif lokal dalam perlindungan buruh migran, siap diurai dalam<br></div><div>tiga hari Pertemuan Nasional Penggerak Desa Peduli Buruh Migran Indonesia, yang<br></div><div>selanjutnya disebut Desbumi Summit.</div><div><br><p></p><p>Wahyu Susilo sebagai Direktur Eksekutif Migrant CARE dalam sambutannya<br></p></div><div>menyatakan kegiatan ini merupakan inisiatif untuk mengakumulasi<br></div><div>pengalaman-pengalaman yang telah dijalankan dalam perlindungan buruh migran di<br></div><div>berbagai daerah yang bisa dikontribusikan untuk tata kelola perlindungan<br></div><div>Pekerja Migran Indonesia berdasar UU nomor 18 Tahun 2017.<p></p><p><br></p></div><div><p></p><p>“Harapannya, inisiatif-inisiatif itu di-scaling up dan direplikasi baik oleh<br></p></div><div>pemerintah nasional, daerah, maupun pemerintah desa, terutama di<br></div><div>kantong-kantong buruh migran,” ujarnya melalui keterangan resmi yang diterima<br></div><div>Media Indonesia, Selasa (27/11).</div><div><br><p></p><p>Pihaknya berharap hal ini dapat mencegah dan mengurangi beragam dampak buruk<br></p></div><div>yang rentan dialami Pekerja Migran Indonesia di negara penerima, terutama<br></div><div>Perempuan Pekerja Migran yang mendominasi Pekerja Migran Indonesia.</div><div><br></div><div><br></div><div>Dengan dukungan Kemitraan Australia-Indonesia untuk Kesetaraan Gender dan<br></div><div>Pemberdayaan Perempuan (MAMPU), Desbumi telah terbentuk di 36 desa yang<br></div><div>tersebar di 8 kabupaten, 5 provinsi di Indonesia. Perwakilan Kedutaan Besar<br></div><div>Australia di Indonesia Darrell Hawkins menilai perempuan pekerja migran berada<br></div><div>di garis depan upaya pemberdayaan dan advokasi di tingkat akar rumput. Mereka<br></div><div>bersuara dan berjejaring dengan pemerintah daerah dan organisasi masyarakat<br></div><div>sipil untuk mempengaruhi kebijakan perlindungan pekerja migran.<p></p><p><br></p></div><div>Sumber berita : http://mediaindonesia.com<br><p></p><p>\"Pemerintah Australia mengapresiasi  Migrant CARE, mitra lokal,<br></p></div><div>pemerintah desa, dan Kementerian Tenaga Kerja  yang telah bekerja sama<br></div><div>untuk  perlindungan pekerja migran Indonesia,” ujar Darrell.</div><div><br><p></p><p>Bupati Banyuwangi Abdullah Azwar Anas pada kesempatan tersebut menambahkan<br></p></div><div>bahwa persoalan perlindungan buruh migran adalah persoalan semua pihak.<br></div><div>Pihaknya mengapresiasi program Migrant CARE yang telah merintis perlindungan<br></div><div>untuk buruh migran berbasis desa, sehingga dapat bersinergi dengan<br></div><div>program-program pemerintah.<p></p></div>', 'kasi-penempatan-tenaga-kerja-disnakerkukm-kab-kebumen-menghadiri-undangan-refleksi-perlindungan-buruh-migran-di-desbumi-summit', 11, 1, 0, 0, 0, 1, NULL, 1, NULL);
INSERT INTO `_post` VALUES (13, 27, '', '', 'UMK KABUPATEN KEBUMEN TAHUN 2019', '2019-01-22 15:26:58', '<p><p>Gubernur Jawa Tengah, Ganjar Pranowo menandatangani surat keputusan terkait upah minimum Kabupaten/Kota (UMK) tahun 2019. Penandatanganan dilakukan di ruang kerja Gubernur.</p><p>Surat yang ditandatangani yaitu Surat Keputusan Gubeenur Jateng Nomor 560/68 tahun 2018 tanggal 21 Nop 2018 tentang Upah Minimum pada 35 Kabupaten Kota di Provinsi Jateng tahun 2019.<br></p><p><br></p><p>Kenaikan upah minimum tahun 2019 memang tidak sebesar tahun 2018 yaitu tahun 2019 sebesar 8,03 persen dan tahun 2018 sebesar 8,71 persen. Ada 11 Kabupaten/Kota yang sepakat sesuai PP Nomor 78 Tahun 2015, kemudian ada 22 Kabupaten/Kota yang naik diatas PP tersebut yaitu antara Rp 232 sampai Rp 33.403,30.</p><p><br></p><p>Adapun UMK Kab. Kebumen Rp 1.686.000,- sedangkan UMK tertinggi untuk tahun 2019 yaitu Kota Semarang Rp 2.498.587,53, terendah Kabupaten Banjarnegara Rp 1.610.000, <br></p><p>Sedangkan 2 Kabupaten yang didorong agar disesuaikan KHL yaitu Kabupaten Pati yang naik 9,91 persen atau menjadi Rp 1.742.000 dan Kabupaten Batang naik 8,58 atau menjadi Rp 1.900.000.</p><br></p>', 'umk-kabupaten-kebumen-tahun-2019', 19, 1, 0, 0, 0, 1, NULL, 1, NULL);
INSERT INTO `_post` VALUES (14, 27, '', '', 'DISNAKERKUKM KAB. KEBUMEN RAIH JUARA 1 DALAM PEMERINGKATAN WEBSITE OPD', '2019-01-22 15:16:30', '<p>Rabu (5/11/2018) Di Pendopo Kab. Kebumen telah dilaksnakan Sosialisasi dan Pemeringkatan Website di Kab. Kebumen. Di Kabupaten Kebumen ada 530 subdomain yang dikelola pemerintah kabupaten kebumen. Ke 530 subdomain tersebut terdiri dari 36 subdomain OPD, 26 subdomain Kecamatan, 6 subdomain organisasi, 1 subdomain instansi vertikal, 1 subdomain Puskesmas, serta 460 Subdomain Desa.</p><p>Dengan banyaknya subdomain yang mendukung pelayanan keterbukaan informasi publik, Wakil Bupati berharap pemerintah dapat semakin transparan dalam memenuhi hak masyarakat<br></p><p><br></p><p>Turut hadir dalam kesempatan tersebut Komisioner Komisi Informasi  Provinsi Jawa Tengah DR. Wijaya, SH., MH., Sekretaris Daerah H. Ujang Sugiono, SH., Asisten 3 Sekda, Widiatmoko, SH, MH,  Kepala Dinas Kominfo Kab Kebumen Cokro Aminoto SIP MKes, serta pinpinan OPD dan pejabat terkait di lingkungan pemerintah kabupaten kebumen.</p><p><br></p><p>Turut hadir dalam kesempatan tersebut Komisioner Komisi Informasi  Provinsi Jawa Tengah DR. Wijaya, SH., MH., Sekretaris Daerah H. Ujang Sugiono, SH., Asisten 3 Sekda, Widiatmoko, SH, MH,  Kepala Dinas Kominfo Kab Kebumen Cokro Aminoto SIP MKes, serta pinpinan OPD dan pejabat terkait di lingkungan pemerintah kabupaten kebumen.</p><p><br></p><p>Pada kesempatan itu, Wakil Bupati Kebumen menyerahkan hadiah bagi OPD dan Desa yang menempati peringkat teratas hingga ketiga.</p><p>Berdasarkan Dari hasil pemeringkatan website OPD, Kecamatan, organisasi dan Desa tahun 2018, peringkat pertama diraih dari Disnaker KUKM dengan nilai 9,62. Peringkat dua web OPD  Satpol PP dengan nilai 8,40, dan disusul Diskominfo dengan nilai 8,17.<br></p><p><br></p><p>Sedangkan Website Desa, peringkat pertama diraih  oleh website Desa Seboro Kec. Sadang dengan nilai 7.75. Peringkat dua, Desa Balingasal Kec. Padureso dengan nilai 7,73, disusul Desa Kalibeji Kec. Sempor dengan nilai 7,67.<br></p><p>Kepala Disnakerkukm Kab. Kebumen hadir dan menerima Hadiah Juara 1 Pemeringkatan Website OPD.<br></p><p><br></p><p>Wakil Bupati Kebumen menyampaikan Selamat Kepada Para Juara dan berharap menjadikan motivasi pada TIM Website Kebumen ataupun lainnya untuk semakin meningkat lebih baik lagi untuk terus berkarya untuk Kabupaten Kebumen.</p><p><br></p><p>Sumber berita : kominfo.kebumenkab.go.id</p>', 'disnakerkukm-kab-kebumen-raih-juara-1-dalam-pemeringkatan-website-opd', 131, 1, 0, 0, 0, 1, NULL, 1, NULL);

-- ----------------------------
-- Table structure for app_config
-- ----------------------------
DROP TABLE IF EXISTS `app_config`;
CREATE TABLE `app_config`  (
  `config_id` int(2) NOT NULL AUTO_INCREMENT,
  `app_title` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `app_title_admin` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `app_title_header1` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `app_title_header1_admin` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `app_title_header2` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `app_title_header2_admin` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `app_running_text` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `app_running_text_admin` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tentang_aplikasi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `dinas_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kabupaten` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `telp` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fax` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fb` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `twitter` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instagram` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gplus` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `vimeo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `website` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `copyright` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kadin_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kadin_nip` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kadin_jabatan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kadin_foto` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kadin_description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `theme` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_slideshow` tinyint(1) NULL DEFAULT NULL,
  `is_polling` tinyint(1) NULL DEFAULT NULL,
  `is_statistic` tinyint(1) NULL DEFAULT NULL,
  `is_marquee` tinyint(1) NULL DEFAULT NULL,
  `is_link` tinyint(1) NULL DEFAULT NULL,
  `is_fb_fanspage` tinyint(1) NULL DEFAULT NULL,
  `is_sosmed` tinyint(1) NULL DEFAULT NULL,
  `is_download` tinyint(1) NULL DEFAULT NULL,
  `is_gallery` tinyint(1) NULL DEFAULT NULL,
  `is_news_index` tinyint(1) NULL DEFAULT NULL,
  `is_news_popular` tinyint(1) NULL DEFAULT NULL,
  `is_news_slide` tinyint(1) NULL DEFAULT NULL,
  `is_profile` tinyint(1) NULL DEFAULT NULL,
  `is_kepala` tinyint(1) NULL DEFAULT NULL,
  `is_link_institusi` tinyint(1) NULL DEFAULT NULL,
  `subdomain` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `meta_description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `meta_keywords` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `meta_content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `max_related_news` int(2) NULL DEFAULT NULL,
  `max_recent_news` int(2) NULL DEFAULT NULL,
  `max_others_news` int(3) NULL DEFAULT NULL,
  `max_popular_news` int(3) NULL DEFAULT NULL,
  `max_new_news` int(3) NULL DEFAULT NULL,
  `fb_plugin_tp` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '1:fanspage,2:lencana/badges',
  `fb_plugin_src` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `dashboard_image` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_subject` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_user` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_header_1` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email_header_2` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email_content_1` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email_content_2` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email_footer` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `protocol` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `smtp_host` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `smtp_port` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `smtp_timeout` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `smtp_user` varchar(80) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `smtp_pass` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `charset` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `newline` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `mailtype` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `validation` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `from_email` varchar(80) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `from_dinas` varchar(80) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `subject` varchar(80) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'email_server',
  `laporan_header` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `time_interval` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logo_img` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `logo_img_mini` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `logo_desc_1` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logo_desc_2` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logo_desc_3` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url_play_store` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url_download_apk` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_config
-- ----------------------------
INSERT INTO `app_config` VALUES (1, 'SIMBOK - Sistem Informasi Kebutuhan Pokok', 'Dashboard - Sistem Informasi Kebutuhan Pokok Masyarakat', 'Pemerintah Kabupaten Kebumen', 'Dashboard KEPOKMAS Kabupaten Kebumen', 'Sistem Informasi Kebutuhan Pokok Masyarakat', 'Pemerintah Kabupaten Kebumen', 'Selamat Datang di Aplikasi SIMBOK (Sistem Informasi Kebutuhan Pokok) - Dinas Perindustrian dan Perdagangan Kabupaten Kebumen', 'Selamat Datang di Dashboard Sistem Informasi Kebutuhan Pokok Masyarakat Kabupaten Kebumen', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book', 'Dinas Perindustrian Dan Perdagangan', 'Kabupaten Kebumen', 'Jl. HM Sarbini No. 17, Kebumen, Bumirejo, Kec. Kebumen, Kabupaten Kebumen, Jawa Tengah 54311', '(0287) 384434', '(0287) 384434', 'dinasperindag@kebumenkab.go.id', NULL, NULL, NULL, '-', '-', 'simbok.kebumenkab.go.id', 'Copyright © 2018 Sistem Informasi Kebutuhan Pokok Masyarakat Kabupaten Kebumen. All rights reserved.', 'H. Ahmad Ujang Sugiono, S.H', '196411171992011002', 'Kepala Dinas Pendidikan', 'disdik.kebumenkab.go.id.250417-kepala-instansi.jpg', '', 'red', 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 'simbok.kebumenkab.go.id', 'Informasi harga kebutuhan pokok di pasar pasar wilayah Kabupaten Kebumen', 'harga pasar, harga pasar hari ini, harga pasar kemarin, harga pasar kebumen, kebumen, pasar, pasar, komoditas, komoditi, indonesia, informasi harga', 'Informasi harga kebutuhan pokok di pasar pasar wilayah Kabupaten Kebumen', 10, 3, 3, 10, 1, '2', 'facebook.com/DinasPendidikan.KabupatenKebumen?fref=ts', NULL, 'DKP Provinsi Jawa Tengah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'smtp', 'ssl://smtp.gmail.com', '465', '7', 'alfian.muhammad1998@gmail.com', 'alfian1234', 'utf-8', '\\r\\n', 'html', 'TRUE', 'admin_bkud_semarangkab@gmail.com', 'Admin BKUD Semarang Kab', 'Password Download e-SPPT', 'PEMERINTAH KABUPATEN KEBUMEN', '5000', 'disperindag.kebumenkab.go.id.100419-logo-kepokmas-kebumen.png', 'disperindag.kebumenkab.go.id.100419-logo-mini-kepokmas-kebumen.png', 'KEPOKMAS KABUPATEN KEBUMEN', 'Sistem Informasi', 'Kebutuhan Pokok Masyarakat', 'https://play.google.com/store', 'https://www.google.com/');

-- ----------------------------
-- Table structure for app_menu_dashboard
-- ----------------------------
DROP TABLE IF EXISTS `app_menu_dashboard`;
CREATE TABLE `app_menu_dashboard`  (
  `menu_id` int(3) NOT NULL AUTO_INCREMENT,
  `label_menu_id` int(3) NULL DEFAULT NULL,
  `menu_parent` int(3) NOT NULL,
  `sub_menu_level_1` int(3) NOT NULL,
  `menu_title` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `menu_order` int(3) NOT NULL,
  `menu_icon` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_st` tinyint(1) NOT NULL COMMENT '1:active,0:not-active',
  `menu_url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `menu_active` varchar(80) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_showing` int(2) NULL DEFAULT NULL COMMENT '1:Developer,2:Semua',
  `menu_webmin` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_category` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'I' COMMENT 'I:internal,E:external',
  `show_home` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '0:tidak,1:iya',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of app_menu_dashboard
-- ----------------------------
INSERT INTO `app_menu_dashboard` VALUES (1, NULL, 0, 0, 'HALAMAN UTAMA', 1, 'fa fa-home', 1, 'manage/index/location/index', 'index', 2, NULL, 'I', '0');
INSERT INTO `app_menu_dashboard` VALUES (15, 4, 0, 0, 'MASTER USER', 2, 'fa fa-list', 1, '#', 'master_user', 2, NULL, 'I', '0');
INSERT INTO `app_menu_dashboard` VALUES (16, 4, 15, 0, 'Menu Dashboard', 1, '', 1, 'manage/index/location/master_menu_dashboard', 'master_user', 2, NULL, 'I', '0');
INSERT INTO `app_menu_dashboard` VALUES (17, 4, 15, 0, 'Group User', 2, '', 1, 'manage/index/location/master_usergroup', 'master_user', 2, NULL, 'I', '0');
INSERT INTO `app_menu_dashboard` VALUES (18, 4, 15, 0, 'Manajemen User', 3, '', 1, 'manage/index/location/master_user', 'master_user', 2, NULL, 'I', '0');
INSERT INTO `app_menu_dashboard` VALUES (19, 4, 0, 0, 'KONFIGURASI', 3, 'fa fa-list', 1, '#', 'konfigurasi', 2, NULL, 'I', '0');
INSERT INTO `app_menu_dashboard` VALUES (20, 4, 19, 0, 'Konfigurasi Aplikasi', 1, '', 1, 'manage/index/location/master_aplikasi', 'konfigurasi', 2, NULL, 'I', '0');
INSERT INTO `app_menu_dashboard` VALUES (29, 4, 0, 0, 'DESIGN WEB', 4, 'fa fa-paint-brush', 1, 'manage/index/location/design_web', 'design', 1, NULL, 'I', '');

-- ----------------------------
-- Table structure for app_token_login
-- ----------------------------
DROP TABLE IF EXISTS `app_token_login`;
CREATE TABLE `app_token_login`  (
  `token_id` int(80) NOT NULL AUTO_INCREMENT,
  `token_login` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `token_date` datetime(0) NOT NULL,
  PRIMARY KEY (`token_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 299 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_token_login
-- ----------------------------
INSERT INTO `app_token_login` VALUES (1, '26b0b6a79b3d858f03562e1f45fe976f', '114.142.170.13', '2019-05-13 12:07:44');
INSERT INTO `app_token_login` VALUES (2, '66458576248e4984191439fb995539cd', '114.142.170.13', '2019-05-13 14:58:30');
INSERT INTO `app_token_login` VALUES (3, '9e6066517c8987a9df408d2faff42fe6', '114.142.170.13', '2019-05-13 15:04:08');
INSERT INTO `app_token_login` VALUES (4, '6159c74b2a3ba922823ca329ab2c982a', '114.142.170.13', '2019-05-14 07:53:28');
INSERT INTO `app_token_login` VALUES (5, '5159639b1ece35bb394c179a39bcfc76', '114.142.170.13', '2019-05-14 07:56:40');
INSERT INTO `app_token_login` VALUES (6, '5e03c54078465fcf193edb7a4350dd2b', '114.142.170.13', '2019-05-14 09:27:33');
INSERT INTO `app_token_login` VALUES (7, '302854c72174cae9b3b7734385532f79', '114.142.170.13', '2019-05-15 09:05:26');
INSERT INTO `app_token_login` VALUES (8, 'f57fe87a2558cdcc1e66fb319839d627', '114.142.170.13', '2019-05-15 10:04:28');
INSERT INTO `app_token_login` VALUES (9, 'a1f2f40de27bbe6a66b7fff9cf17b9c3', '114.142.170.13', '2019-05-15 14:35:06');
INSERT INTO `app_token_login` VALUES (10, '80848fdaf8841ddc452a3d2a61adfad5', '114.142.170.13', '2019-05-16 08:41:34');
INSERT INTO `app_token_login` VALUES (11, 'cafa6da8c785f031abe84c4d37bd9f09', '114.142.170.13', '2019-05-16 11:45:06');
INSERT INTO `app_token_login` VALUES (12, '877c9eba584d03050d95ed2fc1610c16', '114.142.170.13', '2019-05-16 14:03:37');
INSERT INTO `app_token_login` VALUES (13, '5d077a88a95278ad739befbb24581663', '114.142.170.13', '2019-05-16 14:12:57');
INSERT INTO `app_token_login` VALUES (14, 'b6c02789bbc00a502436b01a43707948', '114.142.170.13', '2019-05-16 14:13:22');
INSERT INTO `app_token_login` VALUES (15, 'a2d65c3ea43c1ea7cbf9dbac2126b43d', '114.142.170.13', '2019-05-17 08:17:57');
INSERT INTO `app_token_login` VALUES (16, '0e98c23809a66f6fa544f9d2e6969c4a', '114.142.170.13', '2019-05-17 18:50:11');
INSERT INTO `app_token_login` VALUES (17, 'ae5c02cc9ce131e7fe5227839e2b457e', '114.142.170.13', '2019-05-18 00:01:15');
INSERT INTO `app_token_login` VALUES (18, '55dcc2e65adfa7beed10c373628ead2e', '114.142.170.13', '2019-05-18 08:56:50');
INSERT INTO `app_token_login` VALUES (19, 'b1081ba1f94968bf13de304404e68566', '114.142.170.13', '2019-05-18 08:57:40');
INSERT INTO `app_token_login` VALUES (20, '10b16bd44081b971d11813331f08a06d', '114.142.170.13', '2019-05-18 13:49:08');
INSERT INTO `app_token_login` VALUES (21, 'ed83de48551171534512f203a4755e3c', '114.142.170.13', '2019-05-18 14:23:00');
INSERT INTO `app_token_login` VALUES (22, '511d24c9069865cc2945ee6ae288cbc7', '114.142.170.13', '2019-05-18 14:23:19');
INSERT INTO `app_token_login` VALUES (23, 'a0de4295d3abd4e9d153d3ce9ebff2ee', '114.142.170.13', '2019-05-18 14:23:24');
INSERT INTO `app_token_login` VALUES (24, '103a2a2f866b3975bbcef047865ed961', '114.142.170.13', '2019-05-18 14:23:30');
INSERT INTO `app_token_login` VALUES (25, '75a425e642c3574a1cfcd31b99210192', '114.142.170.13', '2019-05-18 14:24:30');
INSERT INTO `app_token_login` VALUES (26, 'e6322367cf6537c17c7d9a55060e23f6', '114.142.170.13', '2019-05-18 14:24:43');
INSERT INTO `app_token_login` VALUES (27, '64375aeafcceabb1a60530e5a7cde2a1', '114.142.170.13', '2019-05-18 14:29:13');
INSERT INTO `app_token_login` VALUES (28, 'eaf306a59a4cf9fe8a45f7a7b3a4ddf6', '114.142.170.13', '2019-05-18 14:29:19');
INSERT INTO `app_token_login` VALUES (29, '2fdfde61a7be66898e48e04fc9e3dc0c', '114.142.170.13', '2019-05-18 14:29:33');
INSERT INTO `app_token_login` VALUES (30, '5db8e1dc6ef5dae3363058cc4f48c7d7', '114.142.170.13', '2019-05-18 14:29:49');
INSERT INTO `app_token_login` VALUES (31, '3c9053c8c632c1979a6e9df4328a8e6b', '114.142.170.13', '2019-05-18 14:30:04');
INSERT INTO `app_token_login` VALUES (32, '0d93079399795858797e5412cde9b7e0', '114.142.170.13', '2019-05-19 05:41:26');
INSERT INTO `app_token_login` VALUES (33, 'ad160c98ccc29b9ffbd47b673641cf88', '114.142.170.13', '2019-05-19 05:50:43');
INSERT INTO `app_token_login` VALUES (34, '7460244378dd35119118e330e7d5a71a', '114.142.170.13', '2019-05-19 05:51:24');
INSERT INTO `app_token_login` VALUES (35, 'd3c6abbca7ebd8011d878ce6a2553025', '114.142.170.13', '2019-05-19 05:51:31');
INSERT INTO `app_token_login` VALUES (36, 'e58945bbe5921693a4a0587720d72473', '114.142.170.13', '2019-05-19 05:51:41');
INSERT INTO `app_token_login` VALUES (37, '87bca5bfe38bacb10b763ea7c6a9bf9e', '114.142.170.13', '2019-05-19 05:52:13');
INSERT INTO `app_token_login` VALUES (38, '6d3d2e3b639663d5ebf0ca239b62fba0', '114.142.170.13', '2019-05-19 05:52:19');
INSERT INTO `app_token_login` VALUES (39, '9f78a28b6fecf640182b9a466166ef4d', '114.142.170.13', '2019-05-19 05:52:22');
INSERT INTO `app_token_login` VALUES (40, 'e566939fb823a6875e0ef7337b07060c', '114.142.170.13', '2019-05-19 05:52:25');
INSERT INTO `app_token_login` VALUES (41, 'fb78a84593acbc3817599eac929fb0c4', '114.142.170.13', '2019-05-19 05:52:28');
INSERT INTO `app_token_login` VALUES (42, '4c98ead57989c7ee65b7d138ad82e030', '114.142.170.13', '2019-05-19 05:52:31');
INSERT INTO `app_token_login` VALUES (43, 'e554bc46c744a3533c4baee5b6e06aec', '114.142.170.13', '2019-05-19 05:52:32');
INSERT INTO `app_token_login` VALUES (44, 'c3296c4bb30c86c87b72506f1dea094b', '114.142.170.13', '2019-05-19 05:52:34');
INSERT INTO `app_token_login` VALUES (45, 'b9659ddd7c8146277cb8772f0a8fe43d', '114.142.170.13', '2019-05-19 05:52:35');
INSERT INTO `app_token_login` VALUES (46, '98525a2ba94158e7a3c871666bd674f0', '114.142.170.13', '2019-05-19 05:53:00');
INSERT INTO `app_token_login` VALUES (47, '0926bf4eb53b45aaa59300d88f18881f', '114.142.170.13', '2019-05-19 05:53:01');
INSERT INTO `app_token_login` VALUES (48, '55d98f6359b021f85ac0c2562ff3cac7', '114.142.170.13', '2019-05-19 05:54:54');
INSERT INTO `app_token_login` VALUES (49, '857fc541260026c583ccfe07c517f237', '114.142.170.13', '2019-05-19 05:55:33');
INSERT INTO `app_token_login` VALUES (50, '529d441e7302c4f5c2a58a629a908cb6', '114.142.170.13', '2019-05-19 05:55:46');
INSERT INTO `app_token_login` VALUES (51, 'cbb485ebc77e0a7727077afa2799e9a9', '114.142.170.13', '2019-05-19 05:56:10');
INSERT INTO `app_token_login` VALUES (52, '2106a96b0b5815270f3f2553753e8c74', '114.142.170.13', '2019-05-19 05:56:25');
INSERT INTO `app_token_login` VALUES (53, '6a92b1f1f1a6dee24fdd6f9d0a03fc57', '114.142.170.13', '2019-05-19 05:56:41');
INSERT INTO `app_token_login` VALUES (54, '3fbb0c943d354ed5c66d6cf866bbad29', '114.142.170.13', '2019-05-19 05:57:03');
INSERT INTO `app_token_login` VALUES (55, '95fe4ad321a69090d14653b52c4bbb42', '114.142.170.13', '2019-05-19 05:57:23');
INSERT INTO `app_token_login` VALUES (56, '8f9d8ec7f7ca9093c954684f3ca3c0a7', '114.142.170.13', '2019-05-19 09:33:21');
INSERT INTO `app_token_login` VALUES (57, 'f32f99454df9474108927aa392a90400', '114.142.170.13', '2019-05-19 09:33:34');
INSERT INTO `app_token_login` VALUES (58, '94384eeb2ee28b69d234742b1f1f0aa9', '114.142.170.13', '2019-05-19 09:33:48');
INSERT INTO `app_token_login` VALUES (59, 'f66057ad39cff826f1820ca771cf7bbe', '114.142.170.13', '2019-05-19 09:33:57');
INSERT INTO `app_token_login` VALUES (60, 'fe2cd225f4d2bd72132c96948e5b45f7', '114.142.170.13', '2019-05-19 09:34:08');
INSERT INTO `app_token_login` VALUES (61, '17a7e24e1514001d435a11f2fd12d939', '114.142.170.13', '2019-05-19 10:06:21');
INSERT INTO `app_token_login` VALUES (62, 'f3f97261288096cb478bf2269dee1e80', '114.142.170.13', '2019-05-19 14:45:52');
INSERT INTO `app_token_login` VALUES (63, '8ffdb62146e242b368eb96967c52d2fb', '114.142.170.13', '2019-05-20 12:46:05');
INSERT INTO `app_token_login` VALUES (64, '26f9ea65c3528dbb05af51e329260878', '114.142.170.13', '2019-05-20 13:44:04');
INSERT INTO `app_token_login` VALUES (65, '5a505831058387886e8efad4382e8c8f', '114.142.170.13', '2019-05-20 13:58:54');
INSERT INTO `app_token_login` VALUES (66, 'e3ba1b1d05938d34f7061eea826cc625', '114.142.170.13', '2019-05-21 08:12:31');
INSERT INTO `app_token_login` VALUES (67, '473eb69e129e6a5e5cded705245f2720', '114.142.170.13', '2019-05-21 11:38:13');
INSERT INTO `app_token_login` VALUES (68, '1364a6ededb0f4c23353ac84f4dd2167', '114.142.170.13', '2019-05-21 13:11:37');
INSERT INTO `app_token_login` VALUES (69, '5b998d8846751d69b66b361561bab8e7', '114.142.170.13', '2019-05-22 10:00:46');
INSERT INTO `app_token_login` VALUES (70, '2404dccd40e772291c722bfcd08dbed3', '127.0.0.1', '2019-05-22 20:34:26');
INSERT INTO `app_token_login` VALUES (71, 'ca2dcf624ad8002f71e69a5b809acafa', '127.0.0.1', '2019-05-23 09:01:07');
INSERT INTO `app_token_login` VALUES (72, '28791d300fae54c3fa8c2a4646a5a678', '127.0.0.1', '2019-05-23 09:01:32');
INSERT INTO `app_token_login` VALUES (73, '550c8f23a676607492be498f33171539', '127.0.0.1', '2019-05-23 09:05:56');
INSERT INTO `app_token_login` VALUES (74, 'cc9e8eb33e18ae863b6c38b50b520207', '127.0.0.1', '2019-05-23 09:07:37');
INSERT INTO `app_token_login` VALUES (75, '25f415bfae64d01188ecdf7465f8100b', '114.142.170.13', '2019-05-24 18:45:36');
INSERT INTO `app_token_login` VALUES (76, '71ed7e4134a0a23c1b9228bde7da76ca', '114.142.170.13', '2019-05-24 18:46:42');
INSERT INTO `app_token_login` VALUES (77, 'e979bda5c42cf1889bd943a20b2f8b49', '114.142.170.13', '2019-05-24 18:48:02');
INSERT INTO `app_token_login` VALUES (78, 'c957fffee9c18490344608811867600c', '114.142.170.13', '2019-05-24 18:48:15');
INSERT INTO `app_token_login` VALUES (79, '09a53d0442c7bb9d1bbccea34b4d87d8', '114.142.170.13', '2019-05-24 18:48:22');
INSERT INTO `app_token_login` VALUES (80, '76d78106236b62501b2dd2656915e4a4', '114.142.170.13', '2019-05-24 18:48:29');
INSERT INTO `app_token_login` VALUES (81, '5aa041ff59c1c2b4d025c868da02f618', '114.142.170.13', '2019-05-24 18:48:35');
INSERT INTO `app_token_login` VALUES (82, '3120cec6feeca435b4db44d3c85f5689', '114.142.170.13', '2019-05-24 18:48:49');
INSERT INTO `app_token_login` VALUES (83, '6a40ef37e4245205f1cd49c68af65130', '114.142.170.13', '2019-05-24 18:48:54');
INSERT INTO `app_token_login` VALUES (84, '4dfd7704920abd7771a5e43f25fed895', '114.142.170.13', '2019-05-24 18:49:04');
INSERT INTO `app_token_login` VALUES (85, '2089f1217ccaf8f31da6424d9da89468', '114.142.170.13', '2019-05-24 18:49:13');
INSERT INTO `app_token_login` VALUES (86, 'b4d338816d8ca9fdf9238e5367bdf733', '114.142.170.13', '2019-05-24 18:49:27');
INSERT INTO `app_token_login` VALUES (87, '77d26d8ebf79d803e5ae2b821bc9a953', '114.142.170.13', '2019-05-24 18:49:34');
INSERT INTO `app_token_login` VALUES (88, '3222a6e3c61fed19d9785779cba0ef61', '114.142.170.13', '2019-05-24 18:49:58');
INSERT INTO `app_token_login` VALUES (89, 'e1840d31f4384052eba9b5200cc98f2f', '114.142.170.13', '2019-05-24 18:50:12');
INSERT INTO `app_token_login` VALUES (90, '49f3b68cdc0e31d7beeac6dca405f8e7', '114.142.170.13', '2019-05-24 18:50:16');
INSERT INTO `app_token_login` VALUES (91, 'e676cda9d18adcb390dd56048519384c', '114.142.170.13', '2019-05-24 18:50:28');
INSERT INTO `app_token_login` VALUES (92, 'f6c8ea5f785d6530d22137446ca000f2', '114.142.170.13', '2019-05-24 18:50:45');
INSERT INTO `app_token_login` VALUES (93, '20c48adca2a60fe68f837746df73c124', '114.142.170.13', '2019-05-24 18:50:50');
INSERT INTO `app_token_login` VALUES (94, '1744580594d379cb2f53cc71c69c7d28', '114.142.170.13', '2019-05-24 18:51:01');
INSERT INTO `app_token_login` VALUES (95, 'db0438aa4ebadcc8c3ec4c18eedae72a', '114.142.170.13', '2019-05-24 18:51:07');
INSERT INTO `app_token_login` VALUES (96, '44ca47628ae650a4936f936165aa6266', '114.142.170.13', '2019-05-24 18:52:13');
INSERT INTO `app_token_login` VALUES (97, '03b9ff98e8db0807ed516d15298c3eee', '114.142.170.13', '2019-05-24 18:53:41');
INSERT INTO `app_token_login` VALUES (98, '572b3a80c765d8ae4e5321d2395f394b', '114.142.170.13', '2019-05-24 19:38:38');
INSERT INTO `app_token_login` VALUES (99, 'd6ad28a387576ec02cfdb49017ef96ac', '114.142.170.13', '2019-05-24 19:38:47');
INSERT INTO `app_token_login` VALUES (100, '625ae02ca63b4ea2671fd556d36f6cb0', '114.142.170.13', '2019-05-24 19:38:51');
INSERT INTO `app_token_login` VALUES (101, 'f2b1e383c221462cc536fd8707c088d6', '114.142.170.13', '2019-05-24 19:38:53');
INSERT INTO `app_token_login` VALUES (102, '3018dea3e65cf6a4ccbee033c181ffac', '114.142.170.13', '2019-05-24 19:39:08');
INSERT INTO `app_token_login` VALUES (103, 'e47347030a9b0fb61177f7530f93db21', '114.142.170.13', '2019-05-24 19:39:46');
INSERT INTO `app_token_login` VALUES (104, 'f67d36cff6fa99e8334d8b47e194f21a', '114.142.170.13', '2019-05-24 19:40:05');
INSERT INTO `app_token_login` VALUES (105, '3a72ad473bb6c2c16104470c10bf2171', '114.142.170.13', '2019-05-24 19:40:17');
INSERT INTO `app_token_login` VALUES (106, '9ed92c3ac784526c758ba1104b585b4d', '114.142.170.13', '2019-05-24 19:40:29');
INSERT INTO `app_token_login` VALUES (107, 'c846f58d6cf04435f00c6096e091c16e', '114.142.170.13', '2019-05-24 19:40:36');
INSERT INTO `app_token_login` VALUES (108, 'fac4634803541204624e533c5d00a57a', '114.142.170.13', '2019-05-24 20:35:41');
INSERT INTO `app_token_login` VALUES (109, '257934e7ba3aa71a665c7b4a1e82249f', '114.142.170.13', '2019-05-24 20:35:49');
INSERT INTO `app_token_login` VALUES (110, '471e18f84663c8ed5535569ecb2e3ba5', '114.142.170.13', '2019-05-24 20:36:04');
INSERT INTO `app_token_login` VALUES (111, '7df0f1ce8db95c2b925f49abb2f24b21', '114.142.170.13', '2019-05-24 20:36:27');
INSERT INTO `app_token_login` VALUES (112, 'f4e95bfddddf8fc19fb7e9ee9525565a', '114.142.170.13', '2019-05-24 20:36:34');
INSERT INTO `app_token_login` VALUES (113, '952087d3ae7a8dd180690e70bf9aebfb', '114.142.170.13', '2019-05-24 20:37:05');
INSERT INTO `app_token_login` VALUES (114, '402d2e91451b3d148edb7bff7b0f9eb6', '114.142.170.13', '2019-05-24 20:37:22');
INSERT INTO `app_token_login` VALUES (115, '1d83fa305b861be08c41b3d54b0b7bff', '114.142.170.13', '2019-05-24 20:47:20');
INSERT INTO `app_token_login` VALUES (116, '3cf414cfb9c862822bd7bc83637932ff', '114.142.170.13', '2019-05-24 20:50:05');
INSERT INTO `app_token_login` VALUES (117, 'b9d313b7be3c21c3033c10e5ef101922', '114.142.170.13', '2019-05-24 20:50:19');
INSERT INTO `app_token_login` VALUES (118, '2e35e0eb3b76b20b5cb4f4ddbb3acd2d', '114.142.170.13', '2019-05-24 20:50:42');
INSERT INTO `app_token_login` VALUES (119, '3eded7fabd7e307a55e269a294c578ab', '114.142.170.13', '2019-05-24 20:51:31');
INSERT INTO `app_token_login` VALUES (120, 'b3cd8ac1d5f8b6832fee120227a7bfd6', '114.142.170.13', '2019-05-24 20:51:46');
INSERT INTO `app_token_login` VALUES (121, '68df24d63b3f12a675d5bdd0e58bee8a', '114.142.170.13', '2019-05-24 20:52:20');
INSERT INTO `app_token_login` VALUES (122, '9a510286ea6be3f7b4a0a08f806af44a', '114.142.170.13', '2019-05-24 20:57:01');
INSERT INTO `app_token_login` VALUES (123, 'e159162ac8a67d86f138ee16751c93e1', '114.142.170.13', '2019-05-24 20:57:10');
INSERT INTO `app_token_login` VALUES (124, 'af88f79ef220035c2e5c71d8f6d11df0', '114.142.170.13', '2019-05-24 20:58:07');
INSERT INTO `app_token_login` VALUES (125, '8b474d1c4d7be54608cbe64940a0e655', '114.142.170.13', '2019-05-24 20:58:19');
INSERT INTO `app_token_login` VALUES (126, '96fc2c765f3decf158ae31c93676ee57', '114.142.170.13', '2019-05-24 21:01:33');
INSERT INTO `app_token_login` VALUES (127, '678e243572f9922cf5925f2d9cf8e36e', '114.142.170.13', '2019-05-24 21:01:43');
INSERT INTO `app_token_login` VALUES (128, '34338c7deb5fec3f2df66cd4670110d2', '114.142.170.13', '2019-05-24 21:02:03');
INSERT INTO `app_token_login` VALUES (129, 'b0357645126fa4c1b2cc418ecfd83893', '114.142.170.13', '2019-05-24 21:02:40');
INSERT INTO `app_token_login` VALUES (130, '14e8e42d44b7169c2a0967ef92cb338f', '114.142.170.13', '2019-05-24 21:03:25');
INSERT INTO `app_token_login` VALUES (131, '8a16ea260b72c7d105e7b576adf12872', '114.142.170.13', '2019-05-24 21:04:03');
INSERT INTO `app_token_login` VALUES (132, '3aaa546f4d252af84a8e37215a93c632', '114.142.170.13', '2019-05-24 21:04:23');
INSERT INTO `app_token_login` VALUES (133, 'aa6f137b54374af8f13ddeeba209d3ca', '114.142.170.13', '2019-05-24 21:04:53');
INSERT INTO `app_token_login` VALUES (134, 'd5b2144a2c66170aea44dbe308cfb8c0', '114.142.170.13', '2019-05-24 21:05:29');
INSERT INTO `app_token_login` VALUES (135, '3e1cb18d6bfaeb8e65123788dd8a9e8f', '114.142.170.13', '2019-05-24 21:05:40');
INSERT INTO `app_token_login` VALUES (136, 'd49251dccf8345d9dd7fe31114982645', '114.142.170.13', '2019-05-24 21:06:00');
INSERT INTO `app_token_login` VALUES (137, '19ee331e021c97c724b9d8b9d918d14d', '114.142.170.13', '2019-05-24 21:06:38');
INSERT INTO `app_token_login` VALUES (138, '4806a150f8945eaa69620a291c726d87', '114.142.170.13', '2019-05-24 21:08:27');
INSERT INTO `app_token_login` VALUES (139, '56629b9004b3a80bcb5295c6ea07d142', '114.142.170.13', '2019-05-24 21:18:03');
INSERT INTO `app_token_login` VALUES (140, 'ec80f5408831c3e2e3986bf074071022', '114.142.170.13', '2019-05-26 16:14:31');
INSERT INTO `app_token_login` VALUES (141, '6a8bc1b5f925d64bed97852fe8c00009', '114.142.170.13', '2019-05-26 16:14:35');
INSERT INTO `app_token_login` VALUES (142, 'e46e84b55621a5edc13fe3adef236ce4', '114.142.170.13', '2019-05-27 11:23:44');
INSERT INTO `app_token_login` VALUES (146, 'a860d670128874ed1d9a3451b2687d01', '114.142.170.13', '2019-05-29 18:12:11');
INSERT INTO `app_token_login` VALUES (147, 'adbe856756195b077f641eda52b51c19', '114.142.170.13', '2019-05-29 20:32:16');
INSERT INTO `app_token_login` VALUES (148, '25cd394276362482150bd0fe512c6675', '114.142.170.13', '2019-05-30 06:11:23');
INSERT INTO `app_token_login` VALUES (149, '6c3cf5bb31a65f01eae7ee68c2acda55', '114.142.170.13', '2019-05-30 06:13:16');
INSERT INTO `app_token_login` VALUES (150, 'a595b2bd1df680a4ef5f464bcb0c80e9', '114.142.170.13', '2019-05-30 06:15:48');
INSERT INTO `app_token_login` VALUES (151, 'c586ca86917356e85d88e19730132e1e', '114.142.170.13', '2019-05-30 06:16:06');
INSERT INTO `app_token_login` VALUES (152, '5ecf924548c7797362b51ae74e894c80', '114.142.170.13', '2019-05-30 06:45:22');
INSERT INTO `app_token_login` VALUES (153, '4acd70389fbc245ff38bdbe3c8cdda80', '114.142.170.13', '2019-05-30 06:45:24');
INSERT INTO `app_token_login` VALUES (154, '3c4ae15864993edffd462ffa0d8a8902', '114.142.170.13', '2019-05-30 06:48:10');
INSERT INTO `app_token_login` VALUES (155, '9f962267146a15bb7d93f269dcb6e00e', '114.142.170.13', '2019-05-30 06:48:12');
INSERT INTO `app_token_login` VALUES (156, '97f4583535d69332151dc76b8c7a0c2c', '114.142.170.13', '2019-05-30 06:49:35');
INSERT INTO `app_token_login` VALUES (157, '6350e68f0b06557b921b5f4f4b392f2e', '114.142.170.13', '2019-05-30 06:49:46');
INSERT INTO `app_token_login` VALUES (158, 'aedd291075065e8f003d10c8d99d6388', '114.142.170.13', '2019-05-30 06:49:58');
INSERT INTO `app_token_login` VALUES (159, 'ce3f18b33a72861f538ae87b6dc63475', '114.142.170.13', '2019-05-30 06:50:00');
INSERT INTO `app_token_login` VALUES (160, '1b43dd215537a8c020319e1053b9cfdd', '114.142.170.13', '2019-05-30 06:50:02');
INSERT INTO `app_token_login` VALUES (161, 'e2a6353f023469dc71104c6f8b52657e', '114.142.170.13', '2019-05-30 06:50:08');
INSERT INTO `app_token_login` VALUES (162, 'b699a5fa4e740f9434db68bd3e556fdd', '114.142.170.13', '2019-05-30 06:52:29');
INSERT INTO `app_token_login` VALUES (163, '1f2a2ab7b65c3038ae01473daf836bd2', '114.142.170.13', '2019-05-30 06:52:30');
INSERT INTO `app_token_login` VALUES (164, '778cfa417f7909c7c0513f368f3dcdb7', '114.142.170.13', '2019-05-30 06:52:34');
INSERT INTO `app_token_login` VALUES (165, 'a3494118dd16222a9b22d65230f31abe', '114.142.170.13', '2019-05-30 06:52:46');
INSERT INTO `app_token_login` VALUES (166, 'd1090ac5bf34bac95c10dbcfb6620835', '114.142.170.13', '2019-05-30 06:53:03');
INSERT INTO `app_token_login` VALUES (167, '58a32e4d6df9eaae73eca5075639d9e5', '114.142.170.13', '2019-05-30 06:53:12');
INSERT INTO `app_token_login` VALUES (168, 'ad74f6d6c36f8b71137fed12c2dcf0f6', '114.142.170.13', '2019-05-30 06:53:24');
INSERT INTO `app_token_login` VALUES (169, '15cf5bca9dae6f19f9a1f93ef34c802d', '114.142.170.13', '2019-05-30 06:53:34');
INSERT INTO `app_token_login` VALUES (170, '31967c74aa43ea099021a3ae36ac3798', '114.142.170.13', '2019-05-30 06:53:52');
INSERT INTO `app_token_login` VALUES (171, 'db13f5177b37349ac674257fffdf5f3d', '114.142.170.13', '2019-05-30 06:54:00');
INSERT INTO `app_token_login` VALUES (172, 'b0084fbc79b5057bc792c5d24d55cc91', '114.142.170.13', '2019-05-30 06:54:30');
INSERT INTO `app_token_login` VALUES (173, 'a0f52ea3f2758b66adceacac4b630c03', '114.142.170.13', '2019-05-30 06:54:38');
INSERT INTO `app_token_login` VALUES (174, '8702360b609ef0a0426cb28b65eeaca9', '114.142.170.13', '2019-05-30 06:54:43');
INSERT INTO `app_token_login` VALUES (175, '5c9fd1fa6c7aa86f36f0d6977804170a', '114.142.170.13', '2019-05-30 06:54:44');
INSERT INTO `app_token_login` VALUES (176, '71199f664197c93c45216e8053dc4f52', '114.142.170.13', '2019-05-30 06:54:46');
INSERT INTO `app_token_login` VALUES (177, '97c70654668c980b53e227c28927e4ef', '114.142.170.13', '2019-05-30 06:55:16');
INSERT INTO `app_token_login` VALUES (178, '0b5d7cac877fc5c90cefcfda892297c7', '114.142.170.13', '2019-05-30 06:55:35');
INSERT INTO `app_token_login` VALUES (179, '39b7e7c03dd8ec5ddee2d9e773c55f03', '114.142.170.13', '2019-05-30 06:55:36');
INSERT INTO `app_token_login` VALUES (180, '94993829e766c7c5f661d4e8162e9446', '114.142.170.13', '2019-05-30 06:55:50');
INSERT INTO `app_token_login` VALUES (181, '49dcb0492e0eef9106bf7b78685f9873', '114.142.170.13', '2019-05-30 06:56:03');
INSERT INTO `app_token_login` VALUES (182, 'a248ae03a5e379a5ee39bbdac4238e28', '114.142.170.13', '2019-05-30 06:59:20');
INSERT INTO `app_token_login` VALUES (183, '33de1f84c80a330dc80cdb4972c1a016', '114.142.170.13', '2019-05-30 07:00:19');
INSERT INTO `app_token_login` VALUES (184, 'b6854bef0b52fc55c135e4f997df7c9e', '114.142.170.13', '2019-05-30 07:37:41');
INSERT INTO `app_token_login` VALUES (185, 'a6170b0b729a2a9a3ece08c58d5711ef', '114.142.170.13', '2019-05-30 07:41:43');
INSERT INTO `app_token_login` VALUES (186, '7cd76399b04fe83d7042413a308ab6be', '114.142.170.13', '2019-05-30 07:41:45');
INSERT INTO `app_token_login` VALUES (187, 'ef2074659afd81a02ab6930cb4d3513f', '114.142.170.13', '2019-05-30 07:41:55');
INSERT INTO `app_token_login` VALUES (188, '81cd4ef201d003a2692770fbab6d8b1a', '114.142.170.13', '2019-05-30 07:42:13');
INSERT INTO `app_token_login` VALUES (189, 'cf03e5f3b023142d4eed7267c403da9d', '114.142.170.13', '2019-05-30 07:43:41');
INSERT INTO `app_token_login` VALUES (190, '17fa68f6b3931660237798228fc75912', '114.142.170.13', '2019-05-30 07:43:57');
INSERT INTO `app_token_login` VALUES (191, 'f4283a29d438b4152b53f1cb199ce57a', '114.142.170.13', '2019-05-30 07:50:20');
INSERT INTO `app_token_login` VALUES (192, '560b8f55b99e073cd3a6a8a2234302e0', '114.142.170.13', '2019-05-30 07:57:19');
INSERT INTO `app_token_login` VALUES (193, 'becc292cf3428dcc06ee2118bae24709', '114.142.170.13', '2019-05-30 08:12:28');
INSERT INTO `app_token_login` VALUES (194, '7f1786603daf3c53858a9415b4ce9c03', '114.142.170.13', '2019-05-30 08:18:17');
INSERT INTO `app_token_login` VALUES (195, '69541858775c31080be2b26d43be1bc1', '114.142.170.13', '2019-05-30 08:19:18');
INSERT INTO `app_token_login` VALUES (196, 'e09838dd5f95deec97c6cddc2ea713cd', '114.142.170.13', '2019-05-30 08:19:34');
INSERT INTO `app_token_login` VALUES (197, '3e6ca7b8e7edbaee08b0d0ffe61658c2', '114.142.170.13', '2019-05-30 08:20:35');
INSERT INTO `app_token_login` VALUES (198, '93a9d39c4df20157f4dde6422a39a0d3', '114.142.170.13', '2019-05-31 08:33:29');
INSERT INTO `app_token_login` VALUES (199, '714b05e2664964c4749104dfd1b68b56', '114.142.170.13', '2019-05-31 08:33:42');
INSERT INTO `app_token_login` VALUES (200, 'eecfdd0371ec286f9dc96fc0587b3fba', '114.142.170.13', '2019-05-31 08:38:55');
INSERT INTO `app_token_login` VALUES (201, 'a59170b7d22e76ba3ddec45b48f6cab9', '114.142.170.13', '2019-05-31 08:38:59');
INSERT INTO `app_token_login` VALUES (202, '3dfdbe7fe3fd55e8fd4df99b52a912b2', '114.142.170.13', '2019-05-31 08:40:16');
INSERT INTO `app_token_login` VALUES (203, '3ba92276666a18a27dcff005b5cfc460', '114.142.170.13', '2019-05-31 08:40:48');
INSERT INTO `app_token_login` VALUES (204, '4dc7d8ba1d9ef18d0561264647cc4206', '114.142.170.13', '2019-05-31 08:42:57');
INSERT INTO `app_token_login` VALUES (205, '8cc76af58173c1cfc2c9b393897d6167', '114.142.170.13', '2019-05-31 08:43:26');
INSERT INTO `app_token_login` VALUES (206, '6d48a44b9360b9dc8b42309259a4c797', '114.142.170.13', '2019-05-31 08:54:42');
INSERT INTO `app_token_login` VALUES (207, 'ddb909e8e9b3318f83dee22fee20a254', '114.142.170.13', '2019-05-31 08:54:44');
INSERT INTO `app_token_login` VALUES (208, 'c2f188bcf842105f8fb61b74f836ae5e', '114.142.170.13', '2019-05-31 08:54:54');
INSERT INTO `app_token_login` VALUES (209, 'c8849526eb84e7b35cc592301741d5cf', '114.142.170.13', '2019-05-31 08:56:13');
INSERT INTO `app_token_login` VALUES (210, 'b509d5a50f01eddf8dfcc686a06c8abc', '114.142.170.13', '2019-05-31 09:00:52');
INSERT INTO `app_token_login` VALUES (211, '2a2b4a4a5b3edab5a1e2b567dadbfbe2', '114.142.170.13', '2019-05-31 09:01:01');
INSERT INTO `app_token_login` VALUES (212, '0872c86ba0b7f71c90aa0032a7be794e', '114.142.170.13', '2019-05-31 09:04:10');
INSERT INTO `app_token_login` VALUES (213, 'df11c5a9d0147c3d4727ae99c569e868', '114.142.170.13', '2019-05-31 12:17:10');
INSERT INTO `app_token_login` VALUES (214, 'b648d965868e5001b6c63c63a8893b73', '114.142.170.13', '2019-05-31 12:17:14');
INSERT INTO `app_token_login` VALUES (215, '89160eaef62b47203d6c69f66d030234', '114.142.170.13', '2019-05-31 13:02:01');
INSERT INTO `app_token_login` VALUES (216, '1147895a918067e8ba4d6deda18c182b', '114.142.170.13', '2019-05-31 18:39:15');
INSERT INTO `app_token_login` VALUES (217, 'f1caf05cabf2bfbf3a8580c240e14e60', '114.142.170.13', '2019-05-31 18:53:26');
INSERT INTO `app_token_login` VALUES (218, '9662f1937f460f6b2de1511bc8b6526a', '114.142.170.13', '2019-05-31 21:05:29');
INSERT INTO `app_token_login` VALUES (219, 'c171743e22f7d6d15d0612576b018579', '114.142.170.13', '2019-06-01 05:57:14');
INSERT INTO `app_token_login` VALUES (220, '2705fbff4a97b32f075d41bdd67b6d4f', '114.142.170.13', '2019-06-01 05:57:23');
INSERT INTO `app_token_login` VALUES (221, '44fa6dcc21108748c6d56536cb287d5a', '114.142.170.13', '2019-06-01 14:19:14');
INSERT INTO `app_token_login` VALUES (222, 'b8e0081f798e29deb56337a640337543', '114.142.170.13', '2019-06-01 14:19:58');
INSERT INTO `app_token_login` VALUES (223, '4f169343bbe51be4b9d28ff831212b0c', '114.142.170.13', '2019-06-01 23:55:42');
INSERT INTO `app_token_login` VALUES (224, 'aaab5953825e8c92456b803015276b86', '114.142.170.13', '2019-06-01 23:55:44');
INSERT INTO `app_token_login` VALUES (225, '8c7e0a4e72956a20b3a2fd823f65c75e', '114.142.170.13', '2019-06-01 23:56:26');
INSERT INTO `app_token_login` VALUES (226, '30ad334f8c7e5968fbade351a8705532', '114.142.170.13', '2019-06-01 23:58:03');
INSERT INTO `app_token_login` VALUES (227, '219b3ec787dd603e4cffd7aaedbc48c5', '114.142.170.13', '2019-06-02 00:00:29');
INSERT INTO `app_token_login` VALUES (228, '865118510e9749c5eb47a37d1d9e14f1', '114.142.170.13', '2019-06-02 00:15:02');
INSERT INTO `app_token_login` VALUES (229, 'e418558c7ff44e13a18d24c4fcb8e34b', '114.142.170.13', '2019-06-02 00:19:18');
INSERT INTO `app_token_login` VALUES (230, '182804a731264e09d2bdb331a9567c7b', '114.142.170.13', '2019-06-02 00:19:57');
INSERT INTO `app_token_login` VALUES (231, 'e7c973ab659fe76f5e1b856eb59c7fb7', '114.142.170.13', '2019-06-02 00:20:15');
INSERT INTO `app_token_login` VALUES (232, 'e267385b3c369c0c722d9b80c2c3c205', '114.142.170.13', '2019-06-02 00:20:51');
INSERT INTO `app_token_login` VALUES (233, '3a61594ed6df264d9a3ecb040e35b53e', '114.142.170.13', '2019-06-02 00:22:49');
INSERT INTO `app_token_login` VALUES (234, '84f1973868e22adc1d369bf12724c608', '114.142.170.13', '2019-06-02 00:22:51');
INSERT INTO `app_token_login` VALUES (235, 'c14d6e2e887019031984429ef91d2af8', '114.142.170.13', '2019-06-02 00:23:39');
INSERT INTO `app_token_login` VALUES (236, '2213f7c06785f7577fdbf9cf829a2fd5', '114.142.170.13', '2019-06-02 00:25:10');
INSERT INTO `app_token_login` VALUES (237, 'c452be10ad379b552bf51d6175626e4f', '114.142.170.13', '2019-06-02 01:23:04');
INSERT INTO `app_token_login` VALUES (238, '110dc95940d5833eebb8891ace9a50a0', '114.142.170.13', '2019-06-02 01:23:27');
INSERT INTO `app_token_login` VALUES (239, '4ef3d07c8dcd07be19e1b96463ca0846', '114.142.170.13', '2019-06-02 20:55:59');
INSERT INTO `app_token_login` VALUES (240, 'a8785979da41dcb7ed1837dd3d12f6f3', '114.142.170.13', '2019-06-02 20:58:17');
INSERT INTO `app_token_login` VALUES (241, 'acdae9f71913039fb38878ea80705078', '114.142.170.13', '2019-06-02 20:58:21');
INSERT INTO `app_token_login` VALUES (242, 'eec09e4de314ea8a32f7cfc096abde4d', '114.142.170.13', '2019-06-02 20:59:06');
INSERT INTO `app_token_login` VALUES (243, '9b7d273061bfdba5f76ebd91d00878fd', '114.142.170.13', '2019-06-02 21:01:10');
INSERT INTO `app_token_login` VALUES (244, 'a0bf674185a65ff994dd769057cc9bc8', '114.142.170.13', '2019-06-02 21:02:37');
INSERT INTO `app_token_login` VALUES (245, '9ae9dfc8fe1384ef6a56fa07e75a222b', '114.142.170.13', '2019-06-02 21:02:53');
INSERT INTO `app_token_login` VALUES (246, 'deca4eb74ce329a8f927dd7826b0846f', '114.142.170.13', '2019-06-02 21:03:05');
INSERT INTO `app_token_login` VALUES (247, '7e39b14bc5c776e8ce5be481318d47c4', '114.142.170.13', '2019-06-02 21:03:28');
INSERT INTO `app_token_login` VALUES (248, '7e71f46629236e7c38a9b3822a0f2cfa', '114.142.170.13', '2019-06-03 02:52:11');
INSERT INTO `app_token_login` VALUES (249, 'a49953d7d865e9f945e16deac0d43871', '114.142.170.13', '2019-06-03 02:54:25');
INSERT INTO `app_token_login` VALUES (250, '9407e95e5bf0c0348c97191d987aaab4', '114.142.170.13', '2019-06-03 08:30:45');
INSERT INTO `app_token_login` VALUES (251, 'e441f182467e35771929b5178ddba50e', '114.142.170.13', '2019-06-03 10:12:35');
INSERT INTO `app_token_login` VALUES (252, 'f56b2378fa210af2c302ee96c0be39cd', '114.142.170.13', '2019-06-03 19:57:35');
INSERT INTO `app_token_login` VALUES (253, 'b259cf9441cae7f962ec256acc6b228e', '114.142.170.13', '2019-06-03 19:57:48');
INSERT INTO `app_token_login` VALUES (254, 'ccb480b03324d9df81b6b030a4b84112', '114.142.170.13', '2019-06-03 20:02:55');
INSERT INTO `app_token_login` VALUES (255, '9849ff564a75abb344c9c9659483a3cf', '114.142.170.13', '2019-06-03 20:10:03');
INSERT INTO `app_token_login` VALUES (256, 'db4da04f2e0efe43b70eb98914a6d9ae', '114.142.170.13', '2019-06-03 20:11:04');
INSERT INTO `app_token_login` VALUES (257, '28eb80feb87de107844cc4b9fa4b0aa1', '114.142.170.13', '2019-06-03 20:11:25');
INSERT INTO `app_token_login` VALUES (258, 'af615030c899db2f1795c6efbf631e40', '114.142.170.13', '2019-06-03 20:12:53');
INSERT INTO `app_token_login` VALUES (259, 'bcf87259a4416454d23ac5bc325b29ad', '114.142.170.13', '2019-06-03 20:13:35');
INSERT INTO `app_token_login` VALUES (260, 'd9628c8b0c7766aa591a4105700754fc', '114.142.170.13', '2019-06-03 20:14:53');
INSERT INTO `app_token_login` VALUES (261, '82f2301e84d574bcd40fe5eae053330e', '114.142.170.13', '2019-06-03 20:15:15');
INSERT INTO `app_token_login` VALUES (262, 'd72f346e90038788c8c6f6dd66df7f73', '114.142.170.13', '2019-06-03 20:15:42');
INSERT INTO `app_token_login` VALUES (263, '720858220e780f0bf044e3cbf8060ef2', '114.142.170.13', '2019-06-03 20:58:04');
INSERT INTO `app_token_login` VALUES (264, 'aaca4fd306e9584bdb4a69a8d43c5261', '114.142.170.13', '2019-06-03 21:17:41');
INSERT INTO `app_token_login` VALUES (265, '0bed70b9e1b1f7124c527d639be1d9eb', '114.142.170.13', '2019-06-03 21:25:29');
INSERT INTO `app_token_login` VALUES (266, '247ffac7973ff7695bde4b6ee8b8096b', '114.142.170.13', '2019-06-03 21:27:02');
INSERT INTO `app_token_login` VALUES (267, '01642af4797d0278d74c7cb86a012454', '114.142.170.13', '2019-06-03 21:27:43');
INSERT INTO `app_token_login` VALUES (268, '80d9ad551853a561070bb11a44a4f451', '114.142.170.13', '2019-06-03 21:41:22');
INSERT INTO `app_token_login` VALUES (269, 'af649437e4f907c9c8d064fb6420f402', '114.142.170.13', '2019-06-03 22:07:26');
INSERT INTO `app_token_login` VALUES (270, 'af649437e4f907c9c8d064fb6420f402', '114.142.170.13', '2019-06-03 22:07:26');
INSERT INTO `app_token_login` VALUES (271, 'a2e2db2fe4abd4423ef0cd5c2d930252', '114.142.170.13', '2019-06-03 22:09:19');
INSERT INTO `app_token_login` VALUES (272, 'ab3b36a9378e1014acff880eded6828b', '114.142.170.13', '2019-06-03 22:09:39');
INSERT INTO `app_token_login` VALUES (273, '5ee30e44256f68185ec1fcefe465e37b', '114.142.170.13', '2019-06-04 04:58:33');
INSERT INTO `app_token_login` VALUES (274, 'cb72dcb9c3cad1795a7d6ed0b66a13c7', '114.142.170.13', '2019-06-04 05:05:03');
INSERT INTO `app_token_login` VALUES (275, '80edf326a4b9547f13a9ce314507c9ac', '114.142.170.13', '2019-06-04 05:10:37');
INSERT INTO `app_token_login` VALUES (276, '6fba73092d697d3672132e90ef8100ea', '114.142.170.13', '2019-06-04 05:11:17');
INSERT INTO `app_token_login` VALUES (277, '1ec4b6eff7e99ba686ce150b840525c6', '114.142.170.13', '2019-06-04 05:13:23');
INSERT INTO `app_token_login` VALUES (278, '91ee0bd146e51a9323fd8fa42b044413', '114.142.170.13', '2019-06-04 05:13:41');
INSERT INTO `app_token_login` VALUES (279, 'ff28c47cf95d186d6081ccf7c0430f8d', '114.142.170.13', '2019-06-04 05:18:59');
INSERT INTO `app_token_login` VALUES (280, '62717c1585f644f486c053ae1c353e09', '114.142.170.13', '2019-06-04 05:19:09');
INSERT INTO `app_token_login` VALUES (281, '8ab7f467099d8fab35a701dc2a5eb4d1', '114.142.170.13', '2019-06-04 08:26:00');
INSERT INTO `app_token_login` VALUES (282, 'a0b4eb282bde3bcd84dd9d7baa64fede', '114.142.170.13', '2019-06-04 11:11:56');
INSERT INTO `app_token_login` VALUES (283, '93fc7362fc36082548639d2a6b777a84', '114.142.170.13', '2019-06-04 13:36:23');
INSERT INTO `app_token_login` VALUES (284, '7ce5ca257e2cf8da431bd971c04b064e', '114.142.170.13', '2019-06-04 13:36:54');
INSERT INTO `app_token_login` VALUES (285, '0dc50c862479c835274fd6f1d2dcd708', '114.142.170.13', '2019-06-04 13:37:04');
INSERT INTO `app_token_login` VALUES (286, 'a18f09eb3a82cd25b125a00ec456dee4', '114.142.170.13', '2019-06-04 13:37:05');
INSERT INTO `app_token_login` VALUES (287, 'a18f09eb3a82cd25b125a00ec456dee4', '114.142.170.13', '2019-06-04 13:37:05');
INSERT INTO `app_token_login` VALUES (288, '113c4084ff5425621f7751dc3a20014c', '114.142.170.13', '2019-06-04 13:37:06');
INSERT INTO `app_token_login` VALUES (289, '113c4084ff5425621f7751dc3a20014c', '114.142.170.13', '2019-06-04 13:37:06');
INSERT INTO `app_token_login` VALUES (290, 'bf499333041722c6b4d92965e82d3f9a', '114.142.170.13', '2019-06-04 13:37:16');
INSERT INTO `app_token_login` VALUES (291, 'e8f12d8b1fdc6066e036c3e36d87a959', '114.142.170.13', '2019-06-04 13:37:17');
INSERT INTO `app_token_login` VALUES (292, 'e9973856fa62320dddd6bc05cb27d0cd', '114.142.170.13', '2019-06-04 13:37:19');
INSERT INTO `app_token_login` VALUES (293, 'a07c4b63806d93f71998403f6029da90', '114.142.170.13', '2019-06-04 13:38:05');
INSERT INTO `app_token_login` VALUES (294, '7d0bb8afcdb3c38c776f0a2f0d14a13d', '114.142.170.13', '2019-06-04 13:38:28');
INSERT INTO `app_token_login` VALUES (295, '5546f95b95a383381333cf97876c0c75', '114.142.170.13', '2019-06-04 13:38:40');
INSERT INTO `app_token_login` VALUES (296, '9f24b0b014eb1fbd3fb9a6cf363259f0', '114.142.170.13', '2019-06-04 13:39:07');
INSERT INTO `app_token_login` VALUES (297, '4ccdf95405f556882c491689743bfd3b', '114.142.170.13', '2019-06-04 13:40:46');
INSERT INTO `app_token_login` VALUES (298, '261b9742d89480abc4d998499f484838', '114.142.170.13', '2019-06-04 13:45:55');

-- ----------------------------
-- Table structure for app_user
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_password` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_realname` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_st` tinyint(1) NOT NULL COMMENT '1:active,0:not-active',
  `user_photo` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_group` int(2) NOT NULL COMMENT '1:administrator,2:publisher,3:creator',
  `user_description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_login` datetime(0) NULL DEFAULT NULL,
  `st_login` tinyint(1) NULL DEFAULT NULL COMMENT '1:active,0:not-active',
  `is_pegawai` tinyint(1) NOT NULL,
  `pasar_id` char(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES (5, 'admin', '77e2edcc9b40441200e31dc57dbb8829', 'Administrator', 1, '', 2, 'Administrator', '2019-06-04 13:54:39', 1, 0, NULL);
INSERT INTO `app_user` VALUES (6, 'petanahan', '06316e96fa229b361b44cc65e8482434', 'Operator Pasar Petanahan', 1, 'disperindag.kebumenkab.go.id.220319-petanahan.png', 2, '', '2019-05-24 20:37:14', 0, 0, '200006');
INSERT INTO `app_user` VALUES (7, 'prembun', 'bc6c7014349bad8797ba60e9d5310a10', 'Operator Pasar Prembun', 0, 'disperindag.kebumenkab.go.id.220319-prembun.png', 2, '', '2019-04-08 19:11:04', 0, 0, '100001');
INSERT INTO `app_user` VALUES (14, 'alfian', '2d30815bff30177260011392be6c5a9c', 'Alfian Muhammad Ardianto', 1, 'simbok.kebumenkab.go.id.290519-alfian.png', 2, '', '2019-05-30 08:19:23', 0, 0, NULL);
INSERT INTO `app_user` VALUES (15, 'developer', '5c1b29014cd1f34ec326aa6242d41cf1', 'Developer Web', 1, 'simbok.kebumenkab.go.id.300519-developer.png', 1, '', '2019-06-04 13:38:48', 1, 0, NULL);
INSERT INTO `app_user` VALUES (12, 'superadmin', '5994d324f32285147b2f738fb10141ab', 'Superadmin', 1, 'simbok.kebumenkab.go.id.280519-superadmin.png', 2, '', '2019-05-31 09:01:08', 0, 0, NULL);

-- ----------------------------
-- Table structure for app_usergroup
-- ----------------------------
DROP TABLE IF EXISTS `app_usergroup`;
CREATE TABLE `app_usergroup`  (
  `usergroup_id` int(2) NOT NULL AUTO_INCREMENT,
  `usergroup_nm` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`usergroup_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_usergroup
-- ----------------------------
INSERT INTO `app_usergroup` VALUES (1, 'Developer');
INSERT INTO `app_usergroup` VALUES (2, 'Administrator');
INSERT INTO `app_usergroup` VALUES (3, 'Operator');

-- ----------------------------
-- Table structure for app_usergroup_menu
-- ----------------------------
DROP TABLE IF EXISTS `app_usergroup_menu`;
CREATE TABLE `app_usergroup_menu`  (
  `usergroupmenu_id` int(10) NOT NULL AUTO_INCREMENT,
  `usergroup_id` int(2) NULL DEFAULT NULL,
  `menu_id` int(6) NULL DEFAULT NULL,
  `read_st` int(2) NULL DEFAULT NULL COMMENT '1 = Aktif, 0 = Tidak Aktif',
  `add_st` int(2) NULL DEFAULT NULL COMMENT '1 = Aktif, 0 = Tidak Aktif',
  `edit_st` int(2) NULL DEFAULT NULL COMMENT '1 = Aktif, 0 = Tidak Aktif',
  `delete_st` int(2) NULL DEFAULT NULL COMMENT '1 = Aktif, 0 = Tidak Aktif',
  PRIMARY KEY (`usergroupmenu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of app_usergroup_menu
-- ----------------------------
INSERT INTO `app_usergroup_menu` VALUES (17, 3, 1, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (18, 2, 1, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (19, 2, 15, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (20, 2, 16, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (21, 2, 17, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (22, 2, 18, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (23, 2, 19, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (24, 2, 20, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (42, 1, 1, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (43, 1, 15, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (44, 1, 16, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (45, 1, 17, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (46, 1, 18, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (47, 1, 19, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (48, 1, 20, 1, 1, 1, 1);
INSERT INTO `app_usergroup_menu` VALUES (49, 1, 29, 1, 1, 1, 1);

-- ----------------------------
-- Table structure for log_login
-- ----------------------------
DROP TABLE IF EXISTS `log_login`;
CREATE TABLE `log_login`  (
  `log_login_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `ip_address` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hostname` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kota` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wilayah` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `negara_kd` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `negara_nm` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `koordinat` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isp_as_number` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `browser_type` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `os_type` varchar(160) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `device_type` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `platform` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date_login` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`log_login_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of log_login
-- ----------------------------
INSERT INTO `log_login` VALUES (1, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 06:55:31');
INSERT INTO `log_login` VALUES (2, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 07:00:27');
INSERT INTO `log_login` VALUES (3, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 07:37:55');
INSERT INTO `log_login` VALUES (4, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 07:41:49');
INSERT INTO `log_login` VALUES (5, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 07:42:11');
INSERT INTO `log_login` VALUES (6, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 07:42:19');
INSERT INTO `log_login` VALUES (7, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 07:44:07');
INSERT INTO `log_login` VALUES (8, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 07:50:28');
INSERT INTO `log_login` VALUES (9, 14, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 07:57:25');
INSERT INTO `log_login` VALUES (10, 5, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, NULL, NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 08:12:32');
INSERT INTO `log_login` VALUES (11, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 08:18:09');
INSERT INTO `log_login` VALUES (12, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 08:18:22');
INSERT INTO `log_login` VALUES (13, 14, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 08:19:24');
INSERT INTO `log_login` VALUES (14, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 08:19:41');
INSERT INTO `log_login` VALUES (15, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-30 08:20:40');
INSERT INTO `log_login` VALUES (16, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 08:33:52');
INSERT INTO `log_login` VALUES (17, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 08:39:12');
INSERT INTO `log_login` VALUES (18, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 08:40:23');
INSERT INTO `log_login` VALUES (19, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 08:40:56');
INSERT INTO `log_login` VALUES (20, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 08:43:11');
INSERT INTO `log_login` VALUES (21, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 08:54:14');
INSERT INTO `log_login` VALUES (22, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 08:54:50');
INSERT INTO `log_login` VALUES (23, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 08:55:08');
INSERT INTO `log_login` VALUES (24, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 08:56:21');
INSERT INTO `log_login` VALUES (25, 12, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 09:01:08');
INSERT INTO `log_login` VALUES (26, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 09:04:16');
INSERT INTO `log_login` VALUES (27, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 12:17:22');
INSERT INTO `log_login` VALUES (28, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 13:02:09');
INSERT INTO `log_login` VALUES (29, 5, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, NULL, NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 18:39:19');
INSERT INTO `log_login` VALUES (30, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 18:53:31');
INSERT INTO `log_login` VALUES (31, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-05-31 21:06:35');
INSERT INTO `log_login` VALUES (32, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-01 05:57:19');
INSERT INTO `log_login` VALUES (33, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-01 05:57:34');
INSERT INTO `log_login` VALUES (34, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sanggungan', 'Jawa Tengah', NULL, 'ID', '-7.5825,110.8330', 'AS45727 Hutchison CP Telecommunications, PT', 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-01 14:20:07');
INSERT INTO `log_login` VALUES (35, 15, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, NULL, NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-01 23:56:05');
INSERT INTO `log_login` VALUES (36, 15, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, NULL, NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-01 23:56:38');
INSERT INTO `log_login` VALUES (37, 15, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, NULL, NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-01 23:58:16');
INSERT INTO `log_login` VALUES (38, 15, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, NULL, NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-02 00:00:37');
INSERT INTO `log_login` VALUES (39, 5, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, NULL, NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-02 00:24:08');
INSERT INTO `log_login` VALUES (40, 5, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, NULL, NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-02 00:25:15');
INSERT INTO `log_login` VALUES (41, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-02 20:59:06');
INSERT INTO `log_login` VALUES (42, 15, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, ',', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-02 21:01:39');
INSERT INTO `log_login` VALUES (43, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-02 21:02:48');
INSERT INTO `log_login` VALUES (44, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-02 21:03:01');
INSERT INTO `log_login` VALUES (45, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-02 21:03:39');
INSERT INTO `log_login` VALUES (46, 15, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, ',', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 02:52:18');
INSERT INTO `log_login` VALUES (47, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 02:54:43');
INSERT INTO `log_login` VALUES (48, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 08:30:57');
INSERT INTO `log_login` VALUES (49, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 10:12:43');
INSERT INTO `log_login` VALUES (50, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 19:57:45');
INSERT INTO `log_login` VALUES (51, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 19:58:05');
INSERT INTO `log_login` VALUES (52, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:03:13');
INSERT INTO `log_login` VALUES (53, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:11:17');
INSERT INTO `log_login` VALUES (54, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:11:49');
INSERT INTO `log_login` VALUES (55, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:13:10');
INSERT INTO `log_login` VALUES (56, 5, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, ',', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:14:06');
INSERT INTO `log_login` VALUES (57, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:14:26');
INSERT INTO `log_login` VALUES (58, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:15:08');
INSERT INTO `log_login` VALUES (59, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:15:26');
INSERT INTO `log_login` VALUES (60, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:15:55');
INSERT INTO `log_login` VALUES (61, 5, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, ',', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:58:30');
INSERT INTO `log_login` VALUES (62, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 20:58:50');
INSERT INTO `log_login` VALUES (63, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 21:18:11');
INSERT INTO `log_login` VALUES (64, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 21:25:36');
INSERT INTO `log_login` VALUES (65, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 21:27:17');
INSERT INTO `log_login` VALUES (66, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 21:27:54');
INSERT INTO `log_login` VALUES (67, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 21:41:40');
INSERT INTO `log_login` VALUES (68, 15, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, ',', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 22:07:34');
INSERT INTO `log_login` VALUES (69, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 22:08:18');
INSERT INTO `log_login` VALUES (70, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-03 22:09:46');
INSERT INTO `log_login` VALUES (71, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 05:05:21');
INSERT INTO `log_login` VALUES (72, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 05:10:44');
INSERT INTO `log_login` VALUES (73, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 05:11:25');
INSERT INTO `log_login` VALUES (74, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 05:13:31');
INSERT INTO `log_login` VALUES (75, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 05:13:48');
INSERT INTO `log_login` VALUES (76, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 05:19:04');
INSERT INTO `log_login` VALUES (77, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 05:19:22');
INSERT INTO `log_login` VALUES (78, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 08:26:31');
INSERT INTO `log_login` VALUES (79, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 11:13:09');
INSERT INTO `log_login` VALUES (80, 15, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, ',', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:37:12');
INSERT INTO `log_login` VALUES (81, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:37:45');
INSERT INTO `log_login` VALUES (82, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:37:51');
INSERT INTO `log_login` VALUES (83, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:37:59');
INSERT INTO `log_login` VALUES (84, 15, '114.142.170.13', 'DESKTOP-PDRKP51', NULL, NULL, NULL, NULL, ',', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:38:32');
INSERT INTO `log_login` VALUES (85, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:38:39');
INSERT INTO `log_login` VALUES (86, 15, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:38:49');
INSERT INTO `log_login` VALUES (87, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:39:24');
INSERT INTO `log_login` VALUES (88, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:40:51');
INSERT INTO `log_login` VALUES (89, 5, '114.142.170.13', 'DESKTOP-PDRKP51', 'Sragen', 'Jawa Tengah', 'ID', 'Indonesia', '-7.42639,111.022', NULL, 'Chrome 74.0.3729.169', 'Windows 10', 'Computer', 'Aplikasi Web', '2019-06-04 13:54:49');

-- ----------------------------
-- Table structure for mst_label_menu
-- ----------------------------
DROP TABLE IF EXISTS `mst_label_menu`;
CREATE TABLE `mst_label_menu`  (
  `label_menu_id` int(3) NOT NULL AUTO_INCREMENT,
  `label_menu_nm` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `label_menu_order` int(3) NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT NULL,
  `updated_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`label_menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mst_label_menu
-- ----------------------------
INSERT INTO `mst_label_menu` VALUES (1, '- None -', 0, '2019-04-27 21:18:24', NULL);
INSERT INTO `mst_label_menu` VALUES (2, 'PROSES DATA', 1, '2019-04-27 21:18:36', NULL);
INSERT INTO `mst_label_menu` VALUES (3, 'LAPORAN', 2, '2019-04-27 21:18:46', '2019-04-27 21:21:44');
INSERT INTO `mst_label_menu` VALUES (4, 'MASTER KONFIGURASI', 3, '2019-04-27 21:18:56', '2019-06-01 07:28:03');

-- ----------------------------
-- Table structure for web_file
-- ----------------------------
DROP TABLE IF EXISTS `web_file`;
CREATE TABLE `web_file`  (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `file_subdomain` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_path` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_date` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_no` int(3) NULL DEFAULT NULL,
  `file_name` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `file_size` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_tp` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`file_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of web_file
-- ----------------------------
INSERT INTO `web_file` VALUES (1, 12, 'disperindag.kebumenkab.go.id', 'assets/files/post/', '080219', 1, 'disperindag.kebumenkab.go.id.080219-1.html', 'Judul PDF', '60600', 'text/html');

-- ----------------------------
-- Table structure for web_image
-- ----------------------------
DROP TABLE IF EXISTS `web_image`;
CREATE TABLE `web_image`  (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `slideshow_id` int(11) NULL DEFAULT NULL,
  `gallery_id` int(11) NULL DEFAULT NULL,
  `image_subdomain` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image_path` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image_date` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image_no` int(3) NULL DEFAULT NULL,
  `image_name` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image_title` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `image_description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `image_description1` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `image_description2` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `image_size` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image_tp` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image_pos` tinyint(1) NOT NULL COMMENT '1:small,2:large',
  `is_thumbnail` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`image_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 44 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of web_image
-- ----------------------------
INSERT INTO `web_image` VALUES (15, NULL, 2, NULL, 'e-sppt.semarangkab', 'assets/images/slideshow/', '221118', 3, 'e-sppt.semarangkab.221118-3.jpg', 'id-Billing', NULL, 'Solusi Pembayaran Kolektif PBB-P2', 'Dengan id billing memudahkan petugas pemungut PBB-P2 dalam proses pembayaran dan pelaporannya', '194139', 'image/jpeg', 2, NULL);
INSERT INTO `web_image` VALUES (16, NULL, 2, NULL, 'e-sppt.semarangkab', 'assets/images/slideshow/', '221118', 4, 'e-sppt.semarangkab.221118-4.jpg', 'e-sppt', NULL, 'Cara Mudah Mendapatkan SPPT PBB-P2', 'Cara Mudah Mendapatkan SPPT PBB-P2', '128296', 'image/jpeg', 2, NULL);
INSERT INTO `web_image` VALUES (17, NULL, 2, NULL, 'e-sppt.semarangkab', 'assets/images/slideshow/', '221118', 5, 'e-sppt.semarangkab.221118-5.jpg', 'SIP PBB-P2', NULL, 'Sistem Informasi Pembayaran PBB-P2', 'Solusi Pembayaran PBB-P2 Kolektif dan Individu, memudahkan dalam pemantauan Transaksi dan Pelaporan', '72396', 'image/jpeg', 2, NULL);
INSERT INTO `web_image` VALUES (18, NULL, 2, NULL, 'e-sppt.semarangkab', 'assets/images/slideshow/', '221118', 6, 'e-sppt.semarangkab.221118-6.png', 'Bayar PBB-P2', NULL, 'Pembayaran PBB-P2 via kantor POS', 'Alhamdulillah mulai besok tgl 31/1/2018 pembayaran PBB-P2 se jateng bisa dibayar via kantor POS dan agen POS secara nasional', '121542', 'image/png', 2, NULL);
INSERT INTO `web_image` VALUES (37, 10, NULL, NULL, 'disperindag.kebumenkab.go.id', 'assets/images/post/', '270319', 2, 'disperindag.kebumenkab.go.id.270319-2.png', NULL, '', NULL, NULL, '214261', 'image/jpeg', 2, NULL);
INSERT INTO `web_image` VALUES (38, 14, NULL, NULL, 'disperindag.kebumenkab.go.id', 'assets/images/post/', '270319', 3, 'disperindag.kebumenkab.go.id.270319-3.png', NULL, '', NULL, NULL, '100269', 'image/jpeg', 2, NULL);
INSERT INTO `web_image` VALUES (39, 8, NULL, NULL, 'disperindag.kebumenkab.go.id', 'assets/images/post/', '270319', 4, 'disperindag.kebumenkab.go.id.270319-4.jpg', NULL, '', NULL, NULL, '179970', 'image/jpeg', 2, NULL);
INSERT INTO `web_image` VALUES (40, 12, NULL, NULL, 'disperindag.kebumenkab.go.id', 'assets/images/post/', '270319', 5, 'disperindag.kebumenkab.go.id.270319-5.jpg', NULL, '', NULL, NULL, '97712', 'image/jpeg', 2, NULL);
INSERT INTO `web_image` VALUES (41, 13, NULL, NULL, 'disperindag.kebumenkab.go.id', 'assets/images/post/', '270319', 6, 'disperindag.kebumenkab.go.id.270319-6.jpg', NULL, '', NULL, NULL, '128047', 'image/jpeg', 2, NULL);

-- ----------------------------
-- Table structure for web_menu
-- ----------------------------
DROP TABLE IF EXISTS `web_menu`;
CREATE TABLE `web_menu`  (
  `menu_id` int(3) NOT NULL AUTO_INCREMENT,
  `menu_parent` int(3) NOT NULL,
  `menu_title` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `menu_order` int(3) NOT NULL,
  `menu_icon` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_st` tinyint(1) NOT NULL COMMENT '1:active,0:not-active',
  `menu_url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `menu_webmin` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_category` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'I' COMMENT 'I:internal,E:external',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 45 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of web_menu
-- ----------------------------
INSERT INTO `web_menu` VALUES (1, 0, 'HOME', 1, 'fa fa-home', 1, 'web/location/', NULL, 'I');
INSERT INTO `web_menu` VALUES (8, 0, 'STATISTIK', 3, 'fa fa-line-chart', 1, 'statistik', NULL, 'I');
INSERT INTO `web_menu` VALUES (26, 0, 'BERITA', 5, 'fa fa-newspaper-o', 1, 'web/location/news', NULL, 'I');
INSERT INTO `web_menu` VALUES (27, 26, 'Berita Terbaru', 1, NULL, 1, '/berita-terbaru', NULL, 'I');
INSERT INTO `web_menu` VALUES (28, 26, 'Informasi', 2, NULL, 1, '/informasi', NULL, 'I');
INSERT INTO `web_menu` VALUES (29, 26, 'Publikasi', 3, NULL, 1, '/publikasi', NULL, 'I');
INSERT INTO `web_menu` VALUES (30, 26, 'Agenda Kegiatan', 4, NULL, 1, '/agenda-kegiatan', NULL, 'I');
INSERT INTO `web_menu` VALUES (31, 8, 'Statistik Per Pasar', 1, '', 1, 'web/location/statistik_pasar', NULL, 'I');
INSERT INTO `web_menu` VALUES (32, 8, 'Statistik Per Komoditas', 2, '', 1, 'web/location/statistik_komoditas', NULL, 'I');
INSERT INTO `web_menu` VALUES (35, 0, 'PERBANDINGAN HARGA', 4, 'fa fa-balance-scale', 1, 'web/location/perbandingan_harga', NULL, 'I');
INSERT INTO `web_menu` VALUES (38, 0, 'PASAR', 2, 'fa fa-map-marker', 1, 'pasar', NULL, 'I');
INSERT INTO `web_menu` VALUES (39, 38, 'PETANAHAN', 5, NULL, 1, 'web/location/pasar/petanahan-200006', '200006', 'I');
INSERT INTO `web_menu` VALUES (40, 38, 'PREMBUN', 6, NULL, 1, 'web/location/pasar/prembun-100001', '100001', 'I');
INSERT INTO `web_menu` VALUES (41, 38, 'TUMENGGUNGAN', 7, NULL, 1, 'web/location/pasar/tumenggungan-200001', '200001', 'I');
INSERT INTO `web_menu` VALUES (42, 38, 'WONOKRIYO', 8, NULL, 1, 'web/location/pasar/wonokriyo-400001', '400001', 'I');

-- ----------------------------
-- Table structure for web_post
-- ----------------------------
DROP TABLE IF EXISTS `web_post`;
CREATE TABLE `web_post`  (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(3) NOT NULL,
  `post_tp` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '1:post,2:page',
  `post_cat` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '1:news,2:agenda,3:info',
  `post_title` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `post_date` datetime(0) NOT NULL,
  `post_content` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `post_content_short` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `post_url` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'permalink',
  `post_hit` int(11) NOT NULL,
  `post_st` tinyint(1) NOT NULL COMMENT '1:publish,2:draft,3:not-active',
  `post_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `post_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of web_post
-- ----------------------------
INSERT INTO `web_post` VALUES (4, 7, '', '', 'Profil Bidang', '2018-03-31 10:51:48', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, 'profil-bidang', 71, 1, 'Admin', 1);
INSERT INTO `web_post` VALUES (5, 7, '', '', 'Visi & Misi', '2018-03-31 11:54:01', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n', NULL, 'visi---misi', 25, 1, 'Admin', 2);
INSERT INTO `web_post` VALUES (6, 7, '', '', 'Struktur Organisasi', '2018-03-31 11:54:22', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n', NULL, 'struktur-organisasi', 17, 1, 'Admin', 3);
INSERT INTO `web_post` VALUES (8, 7, '', '', 'Informasi Pendidik & Tenaga Kependidikan', '2018-06-26 10:57:42', 'Informasi Pendidik & Tenaga Kependidikan', NULL, 'informasi-pendidik---tenaga-kependidikan', 5, 2, 'Admin', 0);
INSERT INTO `web_post` VALUES (9, 2, '', '', 'Formulir dan Persyaratan', '2018-07-02 12:57:20', 'Formulir dan Persyaratan', 'Formulir dan Persyaratan', 'formulir-dan-persyaratan', 3, 1, 'Admin Disdik', NULL);
INSERT INTO `web_post` VALUES (10, 2, '', '', 'SOP Usulan Kenaikan Pangkat', '2018-07-02 12:58:16', 'SOP Usulan Kenaikan Pangkat', 'SOP Usulan Kenaikan Pangkat', 'sop-usulan-kenaikan-pangkat', 7, 1, 'Admin Disdik', NULL);
INSERT INTO `web_post` VALUES (11, 2, '', '', 'SOP Pensiun', '2018-07-02 12:58:46', 'SOP Pensiun', 'SOP Pensiun', 'sop-pensiun', 8, 1, 'Admin Disdik', NULL);
INSERT INTO `web_post` VALUES (12, 2, '', '', 'SOP Pemenuhan Jam Mengajar', '2018-07-02 12:59:00', 'SOP Pemenuhan Jam Mengajar', 'SOP Pemenuhan Jam Mengajar', 'sop-pemenuhan-jam-mengajar', 1, 1, 'Admin Disdik', NULL);
INSERT INTO `web_post` VALUES (13, 2, '', '', 'SOP Kenaikan Gaji Berkala', '2018-07-02 12:59:17', 'SOP Kenaikan Gaji Berkala', 'SOP Kenaikan Gaji Berkala', 'sop-kenaikan-gaji-berkala', 2, 1, 'Admin Disdik', NULL);
INSERT INTO `web_post` VALUES (14, 2, '', '', 'SOP Ibel Igel', '2018-07-02 12:59:43', 'SOP Ibel Igel', 'SOP Ibel Igel', 'sop-ibel-igel', 0, 1, 'Admin Disdik', NULL);
INSERT INTO `web_post` VALUES (15, 2, '', '', 'SOP Cuti Karena Alasan Penting', '2018-07-02 13:00:08', 'SOP Cuti Karena Alasan Penting', 'SOP Cuti Karena Alasan Penting', 'sop-cuti-karena-alasan-penting', 0, 1, 'Admin Disdik', NULL);
INSERT INTO `web_post` VALUES (16, 2, '', '', 'SOP Cuti Bersalin', '2018-07-02 05:33:21', 'SOP Cuti Bersalin', 'SOP Cuti Bersalin', 'sop-cuti-bersalin', 29, 1, 'Admin Disdik', NULL);

SET FOREIGN_KEY_CHECKS = 1;
