<?php
class Menu_dashboard_show_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_label_menu($menu_st = null) {
        $usergroup_id = $this->session->userdata('ses_usergroup');
        //
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql = "SELECT a.*, b.usergroup_id, c.label_menu_nm, c.label_menu_order  
                FROM app_menu_dashboard a
                LEFT JOIN app_usergroup_menu b ON a.menu_id=b.menu_id
                LEFT JOIN mst_label_menu c ON a.label_menu_id=c.label_menu_id
                WHERE 1 
                    AND b.usergroup_id = '$usergroup_id'
                    $sql_where
                GROUP BY a.label_menu_id
                ORDER BY c.label_menu_order ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        foreach($result as $key => $val) {
            $result[$key]['menu_parent'] = $this->get_all_menu_parent_show($val['label_menu_id'], $menu_st);
        }
        return $result;
    }

    function get_all_menu_parent_show($label_menu_id=null, $menu_st = null) {
        $usergroup_id = $this->session->userdata('ses_usergroup');
        //
        $sql_where = "";
        if($label_menu_id != "") $sql_where .= " AND a.label_menu_id = '$label_menu_id'";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql = "SELECT 
                    a.*, x.count_child   
                FROM app_menu_dashboard a
                LEFT JOIN 
                (
                    SELECT 
                        COUNT(*) as count_child, 
                        b.menu_parent 
                    FROM app_menu_dashboard b 
                    LEFT JOIN app_usergroup_menu c ON b.menu_id=c.menu_id
                    WHERE c.usergroup_id = '$usergroup_id'
                    GROUP BY b.menu_parent
                ) x ON a.menu_id = x.menu_parent 
                LEFT JOIN app_usergroup_menu b ON a.menu_id=b.menu_id
                WHERE a.menu_parent = '0' 
                    AND b.usergroup_id = '$usergroup_id'
                    $sql_where
                ORDER BY a.menu_order ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        foreach($result as $key => $val) {
            if($result[$key]['count_child'] > 0) {
                $result[$key]['menu_child'] = $this->get_all_menu_child_first($usergroup_id, $val['menu_id'],'1');
            }   
        }
        return $result;
    }

    function get_all_menu_child_first($usergroup_id = null, $menu_parent = null, $menu_st = null, $order_by = null, $is_count_tp = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql = "SELECT 
                    a.*, b.* 
                FROM app_menu_dashboard a 
                LEFT JOIN app_usergroup_menu b ON a.menu_id=b.menu_id
                WHERE a.menu_parent='$menu_parent'
                    AND b.usergroup_id='$usergroup_id'
                    AND a.sub_menu_level_1='0'
                    $sql_where
                ORDER BY a.menu_order ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        foreach($result as $key => $val) {
            $set_menu_url = explode('/', @$val['menu_url']);
            //
            $result[$key]['menu_level_1'] = $this->get_all_menu_level_1_first($usergroup_id, $menu_parent ,$val['menu_id']);
            $result[$key]['set_menu_url'] = @$set_menu_url[3];
        }
        //
        return $result;
    }

    function get_all_menu_level_1_first($usergroup_id = null, $menu_parent = null, $sub_menu_level_1 = null, $menu_st = null, $order_by = null, $is_count_tp = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql = "SELECT 
                    a.* 
                FROM app_menu_dashboard a 
                LEFT JOIN app_usergroup_menu b ON a.menu_id=b.menu_id
                WHERE 
                    a.menu_parent='$menu_parent'
                    AND a.sub_menu_level_1='$sub_menu_level_1'
                    AND b.usergroup_id='$usergroup_id'
                    $sql_where
                ORDER BY a.menu_order ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        foreach($result as $key => $val) {
            $result[$key]['get_usergroup_menu'] = $this->get_usergroup_menu($usergroup_id, $val['menu_id']);
        }
        //
        return $result;
    }

    function get_usergroup_menu($usergroup_id=null, $menu_id=null) {
        $sql = "SELECT 
                    a.* 
                FROM app_usergroup_menu a 
                WHERE 1 
                    AND a.usergroup_id = '$usergroup_id'
                    AND a.menu_id = '$menu_id'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        // 
        return $result;
    }

}
