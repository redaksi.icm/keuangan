<?php
class Config_model extends CI_Model {

    function __construct() {
        parent::__construct();
        //
        // session_start();
        $this->load->model('menu_model');
        $this->load->model('user_model');
        //
    }

    // validate api key
    function validate_api() {
        $json_auth   = 'kepokmas.kebumen';
        $json_apikey = md5(md5(date('Y-m-d H')));
        //
        $auth   = $this->input->post('auth');
        $apikey = $this->input->post('apikey');
        if($auth == $json_auth && $apikey == $json_apikey) {
            return true;
        } else {
            $resp_code = '89';              // kode
            $resp_desc = '89:Failed_Auth';  // desc
            //
            $this->render(array(
                'resp_code' => $resp_code,
                'resp_desc' => $resp_desc,
            ));            
        }
    }

    // render
    function render($data=array()) {
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    // var
    function getvar($init='apikey') {
        $result['apikey'] = md5(md5(date('Y-m-d H')));
        echo $result[$init];
    }

    function general() {
        $header['config']   = $this->get_config();
        $header['profile']  = $this->get_profile_user_login();
        $header['menu_dashboard'] = $this->menu_dashboard_show_model->get_all_label_menu('1');
        $header['get_all_user'] = $this->user_model->get_all_user();
        
        $header['csrf_token'] = $_SESSION['csrf_token'] = create_token(32);
        //
        return $header;
    }

    function footer() {
        $header['config']   = $this->get_config();
        $header['profile']  = $this->get_profile_user_login();
        //
        return $header;
    }

    function get_config() {
        $sql = "SELECT * FROM app_config";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        //
        $row['max_upload_size'] = '500000'; // 500Kb
        $row['max_upload_size_str'] = 'maksimal 500Kb'; 
        $row['time_interval'] = $row['time_interval'] / 1000;
        return $row;
    }

    function get_profile_user_login() {
        $this->load->model('user_model');
        //
        $user_id = $this->session->userdata('ses_userid');
        if($user_id != '') {
            $get_user = $this->user_model->get_user($user_id);
            return $get_user;
        } else {
            return array();
        }
    }

    function auth_login($u=null, $p=null, $t_captcha_hide=null, $t_captcha=null) {
        $sql = "SELECT * FROM app_user WHERE user_name=? AND user_password=?";
        $query = $this->db->query($sql, array($u, create_password($p)));
        if($query->num_rows() > 0) {
            $row = $query->row_array();
            //
            if ($t_captcha != $t_captcha_hide) {
                return '3';
            }else{
                if($row['user_st'] == '0') {    // not-active
                    return '2';
                } else {
                    $this->set_login($row['user_id'], 'online');
                    //
                    $ses_data = array(
                        'ses_login'        => 1,
                        'ses_userid'       => $row['user_id'],
                        'ses_username'     => $row['user_name'],
                        'ses_usergroup'    => $row['user_group'],
                        'ses_userrealname' => $row['user_realname'],
                        'ses_lastlogin'    => $row['last_login'],
                        'ses_userst'       => $row['user_st'],
                        'ses_pasar_id' => $row['pasar_id'],
                    );
                    $this->session->set_userdata($ses_data);
                    //
                    $this->_insert_log_login($row['user_id']);            
                    //
                    return '1';
                } 
            }           
        } else {
            return '0';
        }
    }

    function _insert_log_login($user_id=null) {
        $data = $_POST;
        //
        if (get_ip_address() == '::1') {
            $ip_address = '114.142.170.13';
        }else{
            $ip_address = get_ip_address();
        }
        $details = json_decode(file_get_contents("http://api.ipinfodb.com/v3/ip-city/?key=48e9a12e7c98df764fca3b69ec790aa799bcf34dc14e42aa2925ef870fd5d22b&ip=$ip_address&format=json"));
        $get_device = get_device();
        //
        $data['user_id'] = $user_id;
        $data['ip_address'] = $ip_address;
        $data['hostname'] = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $data['kota'] = $details->cityName;
        $data['wilayah'] = $details->regionName;
        $data['negara_kd'] = $details->countryCode;
        $data['negara_nm'] = $details->countryName;
        $data['koordinat'] = $details->latitude.','.$details->longitude;
        $data['browser_type'] = $get_device['browser_type'];
        $data['os_type'] = $get_device['os_type'];
        $data['device_type'] = $get_device['device_type'];
        $data['platform'] = 'Aplikasi Web';
        $data['date_login'] = date('Y-m-d H:i:s');
        //
        unset($data['t_username']);
        unset($data['t_password']);
        unset($data['t_captcha_hide']);
        unset($data['t_captcha']);
        unset($data['t_token_login']);
        unset($data['_token']);
        //
        $outp = $this->db->insert('log_login', $data);
        return $outp;
    }

    function get_token_login() {
        // ip address
        if (get_ip_address() == '::1') {
            $ip_address = '114.142.170.13';
        }else{
            $ip_address = get_ip_address();
        }
        //
        $sql = "SELECT * FROM app_token_login WHERE ip_address=? ORDER BY token_id DESC LIMIT 1";
        $query = $this->db->query($sql, $ip_address);
        $result = $query->row_array();
        return $result;
    }

    function insert_token_login() {
        $data = $_POST;
        //
        if (get_ip_address() == '::1') {
            $ip_address = '114.142.170.13';
        }else{
            $ip_address = get_ip_address();
        }
        // create token
        $token = md5(md5(md5(md5(md5(md5(date('Y-m-d H:i:s')))))));
        //
        $data['token_login'] = $token;
        $data['ip_address'] = $ip_address;
        $data['token_date'] = date('Y-m-d H:i:s');
        //
        $outp = $this->db->insert('app_token_login', $data);
        return $outp;
    }

    function validate_username($u=null) {
        $sql = "SELECT * FROM app_user WHERE user_name=?";
        $query = $this->db->query($sql, array($u));
        if($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['user_id'];
        } else {
            return false;
        }
    }

    function validate_menu($menu_url=null) {
        //
        $get_profile  = $this->get_profile_user_login();
        $usergroup_id = $get_profile['user_group'];
        //
        $sql = "SELECT a.*, b.menu_url  
                FROM app_usergroup_menu a
                INNER JOIN app_menu_dashboard b ON a.menu_id=b.menu_id
                WHERE a.usergroup_id='$usergroup_id' AND b.menu_url LIKE '%$menu_url'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        //
        $set_menu_url = explode('/', @$row['menu_url']);
        //
        if ($menu_url == @$set_menu_url[3]) {
            $result = 1;
        }else{
            $result = 0;
        }
        //
        return $result;
    }

    function validate_login($menu_url=null) {
        $get_validate_menu = $this->validate_menu($menu_url);
        $ses_login = $this->session->userdata('ses_login');        
        //
        if ($menu_url !='') {
            if($ses_login != 1) {
                redirect('manage/login');
            }else{
                if ($get_validate_menu == 0) {
                    redirect('not_found_404');
                }
            }
        }else{
            if($ses_login != 1) {
                redirect('manage/login');
            }
        }
    }

    function clear_login_session() {
        $ses_data = array(
            'ses_login'         => '',
            'ses_userid'        => '',
            'ses_username'      => '',
            'ses_usergroup'     => '',
            'ses_userrealname'  => '',
            'ses_lastlogin'     => '',
            'ses_userst'        => '',
            'ses_pasar_id' => '',
        );       
        $this->session->unset_userdata($ses_data);
        unset_session('ses_login,ses_userid,ses_username,ses_usergroup,ses_userrealname,ses_lastlogin,ses_userst');
    }

    function reset_password($user_id) {
        $new_password = new_password_reset();
        $new_password = create_password($new_password);
        $sql = "UPDATE app_user SET user_password=? WHERE user_id=?";
        return $this->db->query($sql, array($new_password, $user_id));
    }

    function set_login($user_id, $st=null) {
        //
        if ($st == 'online') {
            $st_login = '1';
        }else{
            $st_login = '0';
        }
        //
        $sql = "UPDATE app_user 
                SET st_login='$st_login',last_login=now() 
                WHERE user_id=?";
        return $this->db->query($sql, $user_id);
    }

    function set_logoff() {
        $user_id = $this->session->userdata('ses_userid');
        //
        $sql = "UPDATE app_user SET st_login='0' WHERE user_id=?";
        return $this->db->query($sql, $user_id);
    }

    function update_config($act = "") {
        $data = $_POST;
        //
        if($act == "") {
            if(@$data['is_slideshow'] == '') $data['is_slideshow'] = '';
            if(@$data['is_polling'] == '') $data['is_polling'] = '';
            if(@$data['is_statistic'] == '') $data['is_statistic'] = '';
            if(@$data['is_marquee'] == '') $data['is_marquee'] = '';
            if(@$data['is_gallery'] == '') $data['is_gallery'] = '';
            if(@$data['is_news_index'] == '') $data['is_news_index'] = '';
            if(@$data['is_news_popular'] == '') $data['is_news_popular'] = '';
            if(@$data['is_news_slide'] == '') $data['is_news_slide'] = '';
            if(@$data['is_sosmed'] == '') $data['is_sosmed'] = '';
            if(@$data['is_fb_fanspage'] == '') $data['is_fb_fanspage'] = '';
            if(@$data['is_link'] == '') $data['is_link'] = '';
            if(@$data['is_link_institusi'] == '') $data['is_link_institusi'] = '';
            if(@$data['is_profile'] == '') $data['is_profile'] = '';
            if(@$data['is_kepala'] == '') $data['is_kepala'] = '';
            //
            $data['time_interval'] = $data['time_interval'] * 1000;
            //

            $logo_img_mini = $this->upload_file_process($data);
            if($logo_img_mini != '') {
                $data['logo_img_mini'] = $logo_img_mini;    
            }

            $logo_img = $this->upload_photo_process($data);
            if($logo_img != '') {
                $data['logo_img'] = $logo_img;    
                $data['logo_img_mini'] = $logo_img_mini;    
            }
            //

        } elseif($act == 'facebook') {
            $data['fb_plugin_src'] = ($data['fb_plugin_tp'] == '1' ? $data['fb_fanspage'] : $data['fb_badge']);
            unset($data['fb_fanspage']);
            unset($data['fb_badge']);
        }
        //
        $outp = $this->db->update('app_config',$data);
        return outp_result($outp);
    }

    function upload_file_process($data=null) {
        $result   = '';
        if(@$_FILES['logo_img']['tmp_name'] != '') {
            $config     = $this->config_model->get_config();
            $subdomain  = $config['subdomain'];
            $path_dir   = "assets/images/logo/";
            $date       = date('dmy');
            $title      = 'logo-mini-kepokmas-kebumen';
            $tmp_name     = @$_FILES['logo_img']['tmp_name'];
            $type     = @$_FILES['logo_img']['type'];
            $fupload_name = @$_FILES['logo_img']['name'];
            //
            if ($type == 'image/jpeg' || $type == 'image/jpg' || $type == 'image/png') {
                $result = $this->compress_image($subdomain, $date, $title, $path_dir, $tmp_name, $fupload_name, $config['logo_img_mini']); 
            }else{
                $_SESSION['type_img'] = 1;
                $result = '';
            }          
        }        
        return $result;
    }

    function compress_image($subdomain=null, $date=null, $title=null, $path_dir=null, $tmp_name=null, $fupload_name=null, $old_file=null) {
        //
        $this->load->library('upload');
        $this->load->library('image_lib');
        //
        $name_img = no_upload_images($subdomain, $date, $title, $path_dir, $tmp_name, $fupload_name, $old_file);
        $src_file_name = 'logo_img';
        //
        $config['file_name'] = $name_img;
        $config['upload_path'] = $path_dir; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|gif'; //type yang dapat diakses bisa anda sesuaikan
 
        $this->upload->initialize($config);
        if(!empty($fupload_name)){
            if ($this->upload->do_upload($src_file_name)){
                $gbr = array('upload_data' => $this->upload->data()); 
                // cek resolusi gambar
                $nama_gambar = $path_dir.$gbr['upload_data']['file_name'];
                $data = getimagesize($nama_gambar);
                $width = $data[0];
                $height = $data[1];
                // pembagian
                $bagi_width = 23;
                $bagi_height = 31;
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image'] = $gbr['upload_data']['full_path'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '100%';
                $config['width']= $bagi_width;
                $config['height']= $bagi_height;
                $config['new_image']= $path_dir.$gbr['upload_data']['file_name'];
                $this->image_lib->initialize($config);
                // $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $this->image_lib->clear();
 
                $gambar_1=$gbr['upload_data']['file_name'];
            }
            //
            $compress_image = no_upload_images($subdomain, $date, $title, $path_dir, $tmp_name, @$gambar_1);
            return $name_img;          
        }   
    }

    function upload_photo_process($data=null) {
        $config  = $this->config_model->get_config();
        $result  = '';
        if(@$_FILES['logo_img']['tmp_name'] != '') {
            $subdomain  = $config['subdomain'];
            $path_dir   = "assets/images/logo/";
            $date       = date('dmy');
            $title      = 'logo-kepokmas-kebumen';
            $tmp_name     = @$_FILES['logo_img']['tmp_name'];
            $fupload_name = @$_FILES['logo_img']['name'];
            //
            $result = upload_user_image($subdomain, $date, $title, $path_dir, $tmp_name, $fupload_name, $config['logo_img']);
        }        
        return $result;
    }

    function update_change_dashboard() {
        $config  = $this->config_model->get_config();
        $result  = '';
        if(@$_FILES['dashboard_image']['tmp_name'] != '') {
            $subdomain  = $config['subdomain'];
            $path_dir   = "assets/images/user/";
            $date       = date('dmy');
            $title      = 'dashboard_image';
            $tmp_name     = @$_FILES['dashboard_image']['tmp_name'];
            $fupload_name = @$_FILES['dashboard_image']['name'];
            //
            $result = upload_user_image($subdomain, $date, $title, $path_dir, $tmp_name, $fupload_name, $config['dashboard_image']);
        }        
        //
        $data['dashboard_image'] = ($result != '' ? $result : @$_POST['dashboard_image_hidden']);
        $outp = $this->db->update('app_config',$data);
        return outp_result($outp);
    }

    function delete_photo() {
        $config = $this->get_config();
        $this->delete_photo_process($config['kadin_foto']);
        //
        $data['kadin_foto'] = '';
        $this->db->where('config_id', $config['config_id']);
        $result = $this->db->update('app_config', $data);
        return $result;
    }

    function delete_photo_process($kadin_foto=null) {
        $path_dir = "assets/images/profile/";
        $result = unlink($path_dir . $kadin_foto);
        return $result;
    }
}
