<?php
class Label_menu_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function list_label_menu() {
        $sql = "SELECT 
                    a.*
                FROM mst_label_menu a 
                WHERE 1 
                ORDER BY a.label_menu_id ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        $no=1;
        foreach($result as $key => $val) {
            $result[$key]['no'] = $no;
            $no++;
        }
        return $result;
    }

    function get_label_menu($label_menu_id=null) {
        $sql = "SELECT * FROM mst_label_menu WHERE label_menu_id=?";
        $query = $this->db->query($sql, $label_menu_id);
        $result = $query->row_array();
        return $result;
    }

    function get_all_label_menu() {
        $sql = "SELECT * 
                FROM mst_label_menu";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        $nomor=0;
        $no=1;
        foreach($result as $key => $val) {
            if ($val['label_menu_id'] !='1') {
                $result[$key]['no'] = $no;
                $no++;
            }
            //
            $result[$key]['nomor'] = $nomor;
            $nomor++;
        } 
        return $result;
    }

    function list_label_menu_by_where() {
        $sql_where = $this->where_label_menu();
        //
        $sql = "SELECT 
                    a.label_menu_id, a.label_menu_nm 
                FROM mst_label_menu a
                WHERE 1
                    $sql_where";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        $no=1;
        foreach($result as $key => $val) {
            $result[$key]['no'] = $no;
            $no++;
        } 
        //
        return $result;
    }

    function validate_label_menu_id($label_menu_id=null) {
        $sql = "SELECT a.label_menu_id 
                FROM mst_label_menu a 
                WHERE a.label_menu_id=?";
        $query = $this->db->query($sql, $label_menu_id);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function insert($data_label_menu) {
        $outp = $this->db->insert('mst_label_menu', $data_label_menu);
        return $outp;
    }

    function update($label_menu_id=null, $data_label_menu) {
        $this->db->where('label_menu_id', $label_menu_id);
        $outp = $this->db->update('mst_label_menu', $data_label_menu);
        return $outp;
    }

    function delete($label_menu_id=null) {
        //
        $this->update_app_menu_dashboard($label_menu_id);
        //
        $this->db->where('label_menu_id', $label_menu_id);
        $outp = $this->db->delete('mst_label_menu');
        //
        return $outp;
    }

    function update_app_menu_dashboard($label_menu_id=null) {
        //
        $data_app_menu_dashboard['label_menu_id'] = NULL;
        //
        $this->db->where('label_menu_id', $label_menu_id);
        $outp = $this->db->update('app_menu_dashboard', $data_app_menu_dashboard);
        return $outp;
    }

    function delete_all() {
        $data = $_POST;
        foreach($data['cb_item'] as $key => $id) {
            $this->db->where('label_menu_id', $id);
            $outp = $this->db->delete('mst_label_menu');
        }        
        return outp_result($outp,'delete');
    }

}
