<?php
class Validate_menu_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_data_app_menu_dashboard($menu_url=null, $menu_active=null) {
        $sql = "SELECT 
                    a.* 
                FROM app_menu_dashboard a 
                WHERE 1 
                    AND a.menu_url LIKE '%$menu_url'
                    AND a.menu_active = '$menu_active'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        // 
        return $result;
    }

    function validate_menu($menu_url=null, $menu_active=null, $usergroup_id=null, $url=null, $type=null) {
        $get_app_menu_dashboard = $this->get_data_app_menu_dashboard($menu_url, $menu_active);
        //
        $menu_id = $get_app_menu_dashboard['menu_id'];
        //
        $sql = "SELECT 
                    a.* 
                FROM app_usergroup_menu a 
                WHERE 1 
                    AND a.usergroup_id = '$usergroup_id'
                    AND a.menu_id = '$menu_id'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        //
        if ($type == 'redirect') {
            if ($url == 'add') {
                if ($row['add_st'] !='1') {
                    redirect('not_found_404');
                }
            }elseif ($url == 'edit') {
                if ($row['edit_st'] !='1') {
                    redirect('not_found_404');
                }
            }elseif ($url == 'delete') {
                if ($row['delete_st'] !='1') {
                    redirect('not_found_404');
                }
            }
        }else{
            return $row;
        }
    }

}
