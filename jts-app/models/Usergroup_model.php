<?php
class Usergroup_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function order_usergroup($o=null) {
        $sql_order = "ORDER BY a.usergroup_id ASC";
        if($o == '2') $sql_order = "ORDER BY a.usergroup_nm DESC";
        elseif($o == '3') $sql_order = "ORDER BY a.usergroup_nm ASC";
        return $sql_order;
    }

    function where_usergroup() {
        $ses_txt_search = @$_SESSION['ses_search']['ses_txt_search'];
        //
        $sql_where = "";
        if($ses_txt_search != '')  $sql_where .= " AND (a.usergroup_nm LIKE '%$ses_txt_search%')";
        return $sql_where;
    }

    function paging_usergroup($p = 1, $o = 0) {
        $config = $this->config_model->general();
        //
        $sql_where = $this->where_usergroup();
        //
        if (@$config['profile']['user_group'] != '1') {
            $sql_where.= " AND a.usergroup_id !='1'";
        }
        //
        $sql = "SELECT 
                    COUNT(a.usergroup_id) AS count_data 
                FROM app_usergroup a 
                WHERE 1
                    $sql_where";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $count_data = $row['count_data'];
        //
        $this->load->library('paging');
        $cfg['page'] = $p;
        $cfg['per_page'] = app('per_page');
        $cfg['num_rows'] = $count_data;
        $this->paging->init($cfg);        
        return $this->paging;
    }

    function list_usergroup($o = 0, $offset = 0, $limit = 100) {
        $config = $this->config_model->general();
        //
        $sql_where = $this->where_usergroup();
        $sql_order = $this->order_usergroup($o);
        $sql_paging = " LIMIT ".$offset.",".$limit;
        //
        if (@$config['profile']['user_group'] != '1') {
            $sql_where.= " AND a.usergroup_id !='1'";
        }
        //
        $sql = "SELECT 
                    a.*
                FROM app_usergroup a 
                WHERE 1 
                    $sql_where 
                    $sql_order 
                    $sql_paging";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        $no=1;
        foreach($result as $key => $val) {
            $result[$key]['no'] = $no+$offset;
            $result[$key]['md5_usergroup_id'] = md5(md5(md5($val['usergroup_id'])));
            $no++;
        }
        return $result;
    }

    function get_usergroup($usergroup_id=null) {
        $sql = "SELECT * FROM app_usergroup WHERE usergroup_id=?";
        $query = $this->db->query($sql, $usergroup_id);
        $result = $query->row_array();
        //
        return $result;
    }

    function get_usergroup_md5($usergroup_id=null) {
        $sql = "SELECT * FROM app_usergroup WHERE md5(md5(md5(usergroup_id)))=?";
        $query = $this->db->query($sql, $usergroup_id);
        $result = $query->row_array();
        //
        return $result;
    }

    function insert() {
        $data = $_POST;
        //  
        $outp = $this->db->insert('app_usergroup', $data);   
        //
        return outp_result($outp);
    }

    function update($id=null) {
        $data = $_POST;
        //       
        $this->db->where('usergroup_id', $id);
        $outp = $this->db->update('app_usergroup', $data);
        //
        return outp_result($outp);
    }

    function delete($id=null) {
        //
        $main = $this->get_usergroup_md5($id);
        //
        $this->db->where('usergroup_id', $main['usergroup_id']);
        $outp = $this->db->delete('app_usergroup');
        //
        $this->delete_app_usergroup_menu($id);
        //
        return outp_result($outp,'delete');
    }

    function delete_all() {
        $data = $_POST;
        foreach($data['cb_item'] as $key => $id) {
            if ($id == '1') {
                $outp = '';
            }else{
                $outp = $this->delete($id);
            }
        }        
        return outp_result($outp,'delete');
    }

    function delete_app_usergroup_menu($usergroup_id=null) {
        $this->db->where('usergroup_id', $usergroup_id);
        $outp = $this->db->delete('app_usergroup_menu');
        //
        return $outp;
    }

    function check_app_usergroup_menu($usergroup_id, $menu_id) {
        $sql = "SELECT 
                    * 
                FROM app_usergroup_menu 
                WHERE 1
                    AND usergroup_id='$usergroup_id' 
                    AND menu_id='$menu_id'";
        $query = $this->db->query($sql);
        $result = $query->num_rows();
        return $result;
    }

    function check_data($usergroup_id=null, $menu_id=null) {
        $sql = "SELECT 
                    * 
                FROM app_usergroup_menu 
                WHERE 1
                    AND usergroup_id='$usergroup_id' 
                    AND menu_id='$menu_id'";
        $query = $this->db->query($sql);
        $result = $query->num_rows();
        return $result;
    }

    function insert_config($usergroup_id=null) {
        $data = $_POST;
        // delete data
        $this->db->where('usergroup_id', $usergroup_id);
        $this->db->delete('app_usergroup_menu');
        //
        $data2 = array();
        //
        if(isset($data['read_st'])){
          foreach ($data['read_st'] as $row) {
            $data2[$row]['read_st'] = 1;
          }
        }

        if(isset($data['add_st'])){
          foreach ($data['add_st'] as $row) {
            $data2[$row]['add_st'] = 1;
          }
        }

        if(isset($data['edit_st'])){
          foreach ($data['edit_st'] as $row) {
            $data2[$row]['edit_st'] = 1;
          }
        }

        if(isset($data['delete_st'])){
          foreach ($data['delete_st'] as $row) {
            $data2[$row]['delete_st'] = 1;
          }
        }

        if(isset($data['all_st'])){
          foreach ($data['all_st'] as $row) {
            unset($data2[$row]['all_st']);
          }
        }

        foreach ($data2 as $key => $val) {
            $data_item['usergroup_id'] = $usergroup_id;
            $data_item['menu_id'] = $key;
            $data_item['read_st'] = @$val['read_st'];
            $data_item['add_st'] = @$val['add_st'];
            $data_item['edit_st'] = @$val['edit_st'];
            $data_item['delete_st'] = @$val['delete_st'];
            // insert data
            $outp = $this->db->insert('app_usergroup_menu', $data_item);
        }

        return outp_result(@$outp);
    }
}
