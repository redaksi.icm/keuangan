<?php
class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function order_user($o=null) {
        $sql_order = "ORDER BY a.user_group ASC, a.user_name ASC";
        if($o == '2') $sql_order = "ORDER BY a.user_name DESC";
        elseif($o == '3') $sql_order = "ORDER BY a.user_realname ASC";
        elseif($o == '4') $sql_order = "ORDER BY a.user_realname DESC";
        elseif($o == '5') $sql_order = "ORDER BY a.last_login ASC";
        elseif($o == '6') $sql_order = "ORDER BY a.last_login DESC";
        elseif($o == '7') $sql_order = "ORDER BY a.user_group ASC";
        elseif($o == '8') $sql_order = "ORDER BY a.user_group DESC";
        return $sql_order;
    }

    function where_user() {
        $ses_txt_search = @$_SESSION['ses_search']['ses_txt_search'];
        $ses_user_st = @$_SESSION['ses_search']['ses_user_st'];
        $ses_user_group = @$_SESSION['ses_search']['ses_user_group'];
        //
        if ($ses_user_st == 'aktif') {
            $res_ses_user_st = '1';
        }else{
            $res_ses_user_st = '0';
        }
        //
        $sql_where = "";
        if($ses_txt_search != '')  $sql_where .= " AND (a.user_name LIKE '%$ses_txt_search%') OR (a.user_realname LIKE '%$ses_txt_search%') OR (a.user_description LIKE '%$ses_txt_search%')";
        if($ses_user_st != '')  $sql_where .= " AND a.user_st = '$res_ses_user_st'";
        if($ses_user_group != '')  $sql_where .= " AND a.user_group = '$ses_user_group'";
        return $sql_where;
    }

    function paging_user($p = 1, $o = 0) {
        $config = $this->config_model->general();
        //
        $sql_where = $this->where_user();
        //
        if (@$config['profile']['user_group'] != '1') {
            $sql_where.= " AND a.user_group !='1'";
        }
        //
        $sql = "SELECT 
                    COUNT(a.user_id) AS count_data 
                FROM app_user a 
                WHERE 1
                    $sql_where";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $count_data = $row['count_data'];
        //
        $this->load->library('paging');
        $cfg['page'] = $p;
        $cfg['per_page'] = 8;
        $cfg['num_rows'] = $count_data;
        $this->paging->init($cfg);        
        return $this->paging;
    }

    function list_user($o = 0, $offset = 0, $limit = 100) {
        $config = $this->config_model->general();
        //
        $sql_where = $this->where_user();
        $sql_order = $this->order_user($o);
        $sql_paging = " LIMIT ".$offset.",".$limit;
        //
        if (@$config['profile']['user_group'] != '1') {
            $sql_where.= " AND a.user_group !='1'";
        }
        //
        $sql = "SELECT 
                    a.*, b.usergroup_nm 
                FROM app_user a 
                LEFT JOIN app_usergroup b ON a.user_group=b.usergroup_id
                WHERE 1 
                    $sql_where 
                    $sql_order 
                    $sql_paging";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        $no=1;
        foreach($result as $key => $val) {
            $result[$key]['no'] = $no+$offset;
            // $result[$key]['user_group_name'] = $this->get_user_group($result[$key]['user_group']);
            $no++;
        }
        return $result;
    }

    function list_usergroup() {
        $config = $this->config_model->general();
        //
        $sql_where = "";
        if (@$config['profile']['user_group'] != '1') {
            $sql_where = " AND usergroup_id !='1'";
        }
        //
        $sql = "SELECT * 
                FROM app_usergroup 
                WHERE 1
                    $sql_where
                ORDER BY usergroup_id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function list_jenisusulan_user($user_id=null) {
        $sql = "SELECT a.*, b.userjenisusulan_id 
                FROM mst_jenis_usulan a 
                LEFT JOIN app_user_jenisusulan b ON a.jenisusulan_id=b.jenisusulan_id 
                AND b.user_id=?";
        $query = $this->db->query($sql, $user_id);
        return $query->result_array();
    }

    function get_all_user() {
        $sql = "SELECT 
                    a.*
                FROM app_user a 
                WHERE 1 
                ORDER BY user_realname ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        $no=1;
        foreach($result as $key => $val) {
            $result[$key]['no'] = $no;
            $result[$key]['user_group_name'] = $this->get_user_group($val['user_group']);
            $no++;
        }
        return $result;
    }

    function get_user($user_id=null) {
        $sql = "SELECT * FROM app_user WHERE user_id=?";
        $query = $this->db->query($sql, $user_id);
        $result = $query->row_array();
        //
        $result['user_group_name'] = $this->get_user_group(@$result['user_group']);
        return $result;
    }

    function paging_log_login($p = 1, $o = 0, $user_id=null) {
        $sql = "SELECT 
                    COUNT(a.log_login_id) AS count_data 
                FROM log_login a 
                WHERE 1
                    AND a.user_id=?";
        $query = $this->db->query($sql, $user_id);
        $row = $query->row_array();
        $count_data = $row['count_data'];
        //
        $this->load->library('paging');
        $cfg['page'] = $p;
        $cfg['per_page'] = app('per_page');
        $cfg['num_rows'] = $count_data;
        $this->paging->init($cfg);        
        return $this->paging;
    }

    function list_log_login($o = 0, $offset = 0, $limit = 100, $user_id=null) {
        $sql_paging = " LIMIT ".$offset.",".$limit;
        //
        $sql = "SELECT 
                    a.* 
                FROM log_login a 
                WHERE 1 
                    AND a.user_id=?
                ORDER BY a.date_login DESC
                    $sql_paging";
        $query = $this->db->query($sql, $user_id);
        $result = $query->result_array();
        // 
        $no=1;
        foreach($result as $key => $val) {
            $result[$key]['no'] = $no+$offset;
            $no++;
        }
        return $result;
    }

    function get_user_group($id=null) {
        $sql = "SELECT usergroup_nm FROM app_usergroup WHERE usergroup_id=?";
        $query = $this->db->query($sql, $id);
        $row = $query->row_array();
        return @$row['usergroup_nm'];
    }

    function validate_user_name($user_name=null) {
        $sql = "SELECT a.user_id FROM app_user a WHERE a.user_name=?";
        $query = $this->db->query($sql, $user_name);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function insert() {
        $data = $_POST;
        //
        if(@$data['user_password'] != '') {
            $data['user_password'] = create_password($data['user_password']);
        }
        $data['user_photo'] = $this->upload_file_process($data);    
        $outp = $this->db->insert('app_user', $data);   
        //
        return outp_result($outp);
    }

    function update($id=null) {
        $data = $_POST;
        //
        unset($data['change_password']);
        unset($data['cb_password']);
        unset($data['jenisusulan_id']);
        unset($data['cb_item']);
        unset($data['dataTable_length']);
        //
        $this->db->where('user_id', $id);
        if(@$data['user_password'] != '') {
            $data['user_password'] = create_password($data['user_password']);
        }
        //
        $user_photo = $this->upload_file_process($data, $id);
        if($user_photo != '') {
            $data['user_photo'] = $user_photo;    
        }        
        $outp = $this->db->update('app_user', $data);
        //
        return outp_result($outp);
    }

    function update_change_profile($id=null) {
        $data = $_POST;
        //
        $data_change['user_name'] = $data['user_name'];
        $data_change['user_realname'] = $data['user_realname'];
        //
        if(@$data['user_password'] != '') {
            $data_change['user_password'] = create_password($data['user_password']);
        }
        //
        $user_photo = $this->upload_file_process($data, $id);
        if($user_photo != '') {
            $data_change['user_photo'] = $user_photo;    
        }        
        //
        $this->db->where('user_id', $id);
        $outp = $this->db->update('app_user', $data_change);
        return outp_result($outp);
    }

    function change_status($id=null, $status=null) {
        $data = $_POST;
        //
        $data['user_st'] = $status;
        //
        $this->db->where('user_id', $id);
        $outp = $this->db->update('app_user', $data);
        //
        return outp_result($outp);
    }

    function delete($id=null) {
        $user = $this->get_user($id);
        if (@$user['user_photo'] !='') {
            $this->delete_file_process(@$user['user_photo']);
        }
        //
        $this->db->where('user_id', $id);
        $outp = $this->db->delete('app_user');
        return outp_result($outp,'delete');
    }

    function delete_all() {
        $data = $_POST;
        foreach($data['cb_item'] as $key => $id) {
            $outp = $this->delete($id);
        }        
        return outp_result($outp,'delete');
    }

    function delete_photo($id=null) {
        $user = $this->get_user($id);
        $this->delete_file_process($user['user_photo']);
        //
        $data['user_photo'] = '';
        $this->db->where('user_id', $id);
        $result = $this->db->update('app_user', $data);
        return $result;
    }

    function delete_unitkerja_id($user_id=null, $unitkerja_id=null) {
        $where_array = array('user_id' => $user_id, 'unitkerja_id' => $unitkerja_id);
        $this->db->where($where_array);
        $outp = $this->db->delete('mst_mapping');
        return $outp;
    }

    function upload_file_process($data=null,$user_id=null) {
        $result   = '';
        if(@$_FILES['user_photo']['tmp_name'] != '') {
            $config     = $this->config_model->get_config();
            $subdomain  = $config['subdomain'];
            $path_dir   = "assets/images/user/";
            $date       = date('dmy');
            $title      = clean_url($data['user_name']);
            $tmp_name     = @$_FILES['user_photo']['tmp_name'];
            $type     = @$_FILES['user_photo']['type'];
            $fupload_name = @$_FILES['user_photo']['name'];
            //
            if ($type == 'image/jpeg' || $type == 'image/jpg' || $type == 'image/png') {
                if($user_id != '') {
                    $user = $this->get_user($user_id);
                    $result = upload_user_image($subdomain, $date, $title, $path_dir, $tmp_name, $fupload_name, $user['user_photo']);
                } else {
                    $result = upload_user_image($subdomain, $date, $title, $path_dir, $tmp_name, $fupload_name);
                }  
            }else{
                $_SESSION['type_img'] = 1;
                $result = '';
            }          
        }        
        return $result;
    }
    
    function delete_file_process($user_photo=null) {
        $path_dir = "assets/images/user/";
        $result = unlink($path_dir . $user_photo);
        return $result;
    }
}
