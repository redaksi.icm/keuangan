<?php
class Menu_dashboard_model extends CI_Model {

    function __construct() {
        parent::__construct();
        //
        $this->load->model('post_model');
    }

    function where_menu_dashboard() {
        $ses_txt_search = @$_SESSION['ses_search']['ses_txt_search'];
        //
        $sql_where = "";
        if($ses_txt_search != '')  $sql_where .= " AND (a.menu_title LIKE '%$ses_txt_search%' OR a.menu_url LIKE '%$ses_txt_search%')";
        return $sql_where;
    }

    function paging_menu_dashboard($p = 1, $o = 0) {
        $config  = $this->config_model->get_profile_user_login();
        //
        $sql_where = $this->where_menu_dashboard();
        //
        if ($config['user_group'] != '1') {
            $sql_where .= " AND a.menu_showing = '2'";
        }
        //
        $sql = "SELECT 
                    COUNT(menu_id) AS count_data 
                FROM app_menu_dashboard a 
                WHERE 1
                    $sql_where";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        $count_data = $row['count_data'];
        //
        $this->load->library('paging');
        $cfg['page'] = $p;
        $cfg['per_page'] = '100';
        $cfg['num_rows'] = $count_data;
        $this->paging->init($cfg);        
        return $this->paging;
    }

    function list_menu_dashboard($o = 0, $offset = 0, $limit = 100, $menu_parent=null) {
        $config  = $this->config_model->get_profile_user_login();
        //
        $sql_where = $this->where_menu_dashboard();
        if($menu_parent != '') $sql_where .= " AND a.menu_parent = '$menu_parent'";
        else $sql_where .= " AND a.menu_parent = '0'";
        $sql_paging = " LIMIT ".$offset.",".$limit;
        //
        if ($config['user_group'] != '1') {
            $sql_where .= " AND a.menu_showing = '2'";
        }
        //
        $sql = "SELECT 
                    a.*, b.label_menu_nm 
                FROM app_menu_dashboard a
                LEFT JOIN mst_label_menu b ON a.label_menu_id=b.label_menu_id 
                WHERE 1 
                    $sql_where 
                ORDER BY a.menu_order ASC
                    $sql_paging";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        $no=1;
        foreach($result as $key => $val) {
            $result[$key]['no'] = $no;
            $result[$key]['menu_child'] = $this->get_all_menu_child($result[$key]['menu_id']);
            $no++;
        }
        return $result;
    }

    function get_all_menu_dashboard($usergroup_id=null) {
        $config  = $this->config_model->get_profile_user_login();
        //
        if ($config['user_group'] != '1') {
            $sql_where = " AND a.menu_showing = '2'";
        }else{
            $sql_where = "";
        }
        //
        $sql = "SELECT 
                    a.*, b.* 
                FROM app_menu_dashboard a 
                LEFT JOIN mst_label_menu b ON a.label_menu_id=b.label_menu_id
                WHERE 1 
                    $sql_where
                    AND a.menu_parent = '0'
                ORDER BY a.menu_order ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        $no=1;
        foreach($result as $key => $val) {
            $result[$key]['no'] = $no;
            $result[$key]['menu_child'] = $this->get_all_menu_child_config($usergroup_id, $result[$key]['menu_id']);
            $result[$key]['get_usergroup_menu'] = $this->get_usergroup_menu($usergroup_id, $result[$key]['menu_id']);
            $no++;
        }
        return $result;
    }

    function get_all_menu_child_config($usergroup_id = null, $menu_parent = null, $menu_st = null, $order_by = null, $is_count_tp = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql_order_by = "ASC";
        if($order_by != "") $sql_order_by = $order_by;
        //
        $sql = "SELECT 
                    a.*, b.* 
                FROM app_menu_dashboard a 
                LEFT JOIN mst_label_menu b ON a.label_menu_id=b.label_menu_id
                WHERE a.menu_parent='$menu_parent'
                    AND a.sub_menu_level_1='0'
                    $sql_where 
                ORDER BY a.menu_order 
                    $sql_order_by";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        foreach($result as $key => $val) {
            $result[$key]['menu_level_1'] = $this->get_all_menu_level_1_config($usergroup_id, $menu_parent ,$result[$key]['menu_id']);
            $result[$key]['get_usergroup_menu'] = $this->get_usergroup_menu($usergroup_id, $result[$key]['menu_id']);
        }
        //
        return $result;
    }

    function get_all_menu_level_1_config($usergroup_id = null, $menu_parent = null, $sub_menu_level_1 = null, $menu_st = null, $order_by = null, $is_count_tp = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql_order_by = "ASC";
        if($order_by != "") $sql_order_by = $order_by;
        //
        $sql = "SELECT 
                    a.*, b.* 
                FROM app_menu_dashboard a 
                LEFT JOIN mst_label_menu b ON a.label_menu_id=b.label_menu_id
                WHERE 
                    a.menu_parent='$menu_parent'
                    AND a.sub_menu_level_1='$sub_menu_level_1'
                    $sql_where
                ORDER BY a.menu_order 
                    $sql_order_by";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        foreach($result as $key => $val) {
            $result[$key]['get_usergroup_menu'] = $this->get_usergroup_menu($usergroup_id, $result[$key]['menu_id']);
        }
        //
        return $result;
    }

    function get_usergroup_menu($usergroup_id=null, $menu_id=null) {
        $sql = "SELECT 
                    a.* 
                FROM app_usergroup_menu a 
                WHERE 1 
                    AND a.usergroup_id = '$usergroup_id'
                    AND a.menu_id = '$menu_id'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        // 
        return $result;
    }

    function get_menu_dashboard($menu_id = null) {
        $sql = "SELECT * FROM app_menu_dashboard a WHERE a.menu_id=?";
        $query = $this->db->query($sql, $menu_id);
        return $query->row_array();
    }

    function get_menu_by_url($menu_url = null) {
        if ($menu_url == 'recent') {
            $sql_menu_url = " a.menu_parent='26'";
        }else{
            $sql_menu_url = " REPLACE(a.menu_url,'/','') = '$menu_url'";
        }
        //
        $sql = "SELECT * FROM app_menu_dashboard a WHERE $sql_menu_url";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function get_all_menu_parent_show($menu_st = null) {
        $config  = $this->config_model->get_profile_user_login();
        $usergroup_id = @$config['user_group'];
        //
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql = "SELECT 
                    a.*, x.count_child   
                FROM app_menu_dashboard a
                LEFT JOIN 
                (
                    SELECT 
                        COUNT(*) as count_child, 
                        b.menu_parent 
                    FROM app_menu_dashboard b 
                    LEFT JOIN app_usergroup_menu c ON b.menu_id=c.menu_id
                    WHERE c.usergroup_id = '$usergroup_id'
                    GROUP BY b.menu_parent
                ) x ON a.menu_id = x.menu_parent 
                LEFT JOIN app_usergroup_menu b ON a.menu_id=b.menu_id
                WHERE a.menu_parent = '0' 
                    AND b.usergroup_id = '$usergroup_id'
                    $sql_where 
                ORDER BY a.menu_order ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        foreach($result as $key => $val) {
            if($result[$key]['count_child'] > 0) {
                $result[$key]['menu_child'] = $this->get_all_menu_child_first($usergroup_id, $result[$key]['menu_id'],'1');
            }   
        }
        return $result;
    }

    function get_all_menu_parent($menu_st = null) {
        $config  = $this->config_model->get_profile_user_login();
        $usergroup_id = @$config['user_group'];
        //
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql = "SELECT 
                    a.*, x.count_child   
                FROM app_menu_dashboard a
                LEFT JOIN 
                (
                    SELECT 
                        COUNT(*) as count_child, 
                        b.menu_parent 
                    FROM app_menu_dashboard b 
                    LEFT JOIN app_usergroup_menu c ON b.menu_id=c.menu_id
                    WHERE c.usergroup_id = '$usergroup_id'
                    GROUP BY b.menu_parent
                ) x ON a.menu_id = x.menu_parent 
                WHERE a.menu_parent = '0' 
                    $sql_where 
                ORDER BY a.menu_order ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        foreach($result as $key => $val) {
            if($result[$key]['count_child'] > 0) {
                $result[$key]['menu_child'] = $this->get_all_menu_child_first($usergroup_id, $result[$key]['menu_id'],'1');
            }                    
        }
        return $result;
    }

    function get_all_menu_child_first($usergroup_id = null, $menu_parent = null, $menu_st = null, $order_by = null, $is_count_tp = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql_order_by = "ASC";
        if($order_by != "") $sql_order_by = $order_by;
        //
        $sql = "SELECT 
                    a.*, b.* 
                FROM app_menu_dashboard a 
                LEFT JOIN app_usergroup_menu b ON a.menu_id=b.menu_id
                WHERE a.menu_parent='$menu_parent'
                    AND b.usergroup_id='$usergroup_id'
                    AND a.sub_menu_level_1='0'
                    $sql_where 
                ORDER BY a.menu_order 
                    $sql_order_by";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        foreach($result as $key => $val) {
            $result[$key]['menu_level_1'] = $this->get_all_menu_level_1_first($usergroup_id, $menu_parent ,$result[$key]['menu_id']);
        }
        //
        return $result;
    }

    function get_all_menu_level_1_first($usergroup_id = null, $menu_parent = null, $sub_menu_level_1 = null, $menu_st = null, $order_by = null, $is_count_tp = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql_order_by = "ASC";
        if($order_by != "") $sql_order_by = $order_by;
        //
        $sql = "SELECT 
                    a.* 
                FROM app_menu_dashboard a 
                LEFT JOIN app_usergroup_menu b ON a.menu_id=b.menu_id
                WHERE 
                    a.menu_parent='$menu_parent'
                    AND a.sub_menu_level_1='$sub_menu_level_1'
                    AND b.usergroup_id='$usergroup_id'
                    $sql_where
                ORDER BY a.menu_order 
                    $sql_order_by";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        foreach($result as $key => $val) {
            $result[$key]['get_usergroup_menu'] = $this->get_usergroup_menu($usergroup_id, $result[$key]['menu_id']);
        }
        //
        return $result;
    }

    function get_sub_menu_level_1($menu_st = null, $menu_id = null) {
        $sql_where = "";
        $sql_without = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql = "SELECT 
                    a.*, x.count_child
                FROM app_menu_dashboard a
                LEFT JOIN 
                (
                    SELECT COUNT(*) as count_child, b.menu_parent FROM app_menu_dashboard b GROUP BY b.menu_parent
                ) x ON a.menu_id = x.menu_parent 
                WHERE 
                    a.menu_parent = '$menu_id'
                    AND a.sub_menu_level_1 = '0'
                    $sql_where 
                ORDER BY a.menu_order ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        // 
        foreach($result as $key => $val) {
            if($result[$key]['count_child'] > 0) {
                $result[$key]['menu_child'] = $this->get_all_menu_child($result[$key]['menu_id'],'1','','all_post');
            }                    
        }
        return $result;
    }

    function get_all_menu_child($menu_parent = null, $menu_st = null, $order_by = null, $is_count_tp = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql_order_by = "ASC";
        if($order_by != "") $sql_order_by = $order_by;
        //
        $sql = "SELECT 
                    a.*, b.label_menu_nm 
                FROM app_menu_dashboard a 
                LEFT JOIN mst_label_menu b ON a.label_menu_id=b.label_menu_id
                WHERE a.menu_parent='$menu_parent'
                    AND a.sub_menu_level_1='0'
                    $sql_where 
                ORDER BY a.menu_order 
                    $sql_order_by";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        foreach($result as $key => $val) {
            $result[$key]['menu_level_1'] = $this->get_all_menu_level_1_all($menu_parent ,$result[$key]['menu_id']);
            $result[$key]['get_usergroup_menu'] = $this->get_usergroup_menu_all($result[$key]['menu_id']);
        }
        //
        return $result;
    }

    function get_all_menu_level_1_all($menu_parent = null, $sub_menu_level_1 = null, $menu_st = null, $order_by = null, $is_count_tp = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql_order_by = "ASC";
        if($order_by != "") $sql_order_by = $order_by;
        //
        $sql = "SELECT 
                    a.*, b.label_menu_nm  
                FROM app_menu_dashboard a 
                LEFT JOIN mst_label_menu b ON a.label_menu_id=b.label_menu_id
                WHERE 
                    a.menu_parent='$menu_parent'
                    AND a.sub_menu_level_1='$sub_menu_level_1'
                    $sql_where
                ORDER BY a.menu_order 
                    $sql_order_by";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        return $result;
    }

    function get_usergroup_menu_all($menu_id=null) {
        $sql = "SELECT 
                    a.* 
                FROM app_usergroup_menu a 
                WHERE 1 
                    AND a.menu_id = '$menu_id'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        // 
        return $result;
    }

    function get_all_menu_level_1($usergroup_id = null, $menu_parent = null, $sub_menu_level_1 = null, $menu_st = null, $order_by = null, $is_count_tp = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql_order_by = "ASC";
        if($order_by != "") $sql_order_by = $order_by;
        //
        $sql = "SELECT 
                    a.* 
                FROM app_menu_dashboard a 
                WHERE 
                    a.menu_parent='$menu_parent'
                    AND a.sub_menu_level_1='$sub_menu_level_1'
                    $sql_where
                ORDER BY a.menu_order 
                    $sql_order_by";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //
        foreach($result as $key => $val) {
            $result[$key]['get_usergroup_menu'] = $this->get_usergroup_menu($usergroup_id, $result[$key]['menu_id']);
        }
        //
        return $result;
    }

    function get_last_menu_order($menu_parent=null) {
        $sql_where = "";
        if($menu_parent != "") $sql_where .= " AND a.menu_parent = '$menu_parent'";
        $sql = "SELECT 
                    MAX(a.menu_order) as menu_order 
                FROM app_menu_dashboard a 
                WHERE 1 
                    $sql_where";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return ($row['menu_order'] + 1);
    }

    function insert() {
        $data = $_POST;        
        //
        if ($data['label_menu_id'] == '1') {
            $data['label_menu_id'] = NULL;
        }
        if ($data['menu_showing'] !='') {
            $data['menu_showing'] = $data['menu_showing'];
        }else{
            $data['menu_showing'] = 2;
        }
        //
        $outp = $this->db->insert('app_menu_dashboard', $data);
        return outp_result($outp);
    }

    function insert_partial($menu_parent=null) {
        $data = $_POST;
        $data_partial['menu_parent']    = $menu_parent;
        $data_partial['menu_title']     = $data['menu_title'];
        $data_partial['menu_order']     = $this->get_last_menu_order($menu_parent);
        $data_partial['menu_st']        = '1';
        $data_partial['menu_category']  = 'I';  // internal
        $data_partial['menu_url']       = '/'.clean_url($data['menu_title']);
        //
        if($this->validate_partial($data_partial['menu_parent'], $data_partial['menu_title']) == false) {
            $this->db->insert('app_menu_dashboard', $data_partial);
            //
            $menu_id = $this->db->insert_id();
            //
            $return  = array(
                'menu_id'     => $menu_id,
                'menu_title'  => $data['menu_title'],
            );
        } else {
            $return = false;   
        }        
        return @$return;
    }

    function validate_partial($menu_parent=null, $menu_title=null) {
        $sql = "SELECT 
                    a.menu_id 
                FROM app_menu_dashboard a 
                WHERE a.menu_parent=? AND LOWER(a.menu_title)=?";
        $query = $this->db->query($sql, array($menu_parent, strtolower($menu_title)));
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function validate_menu_url($menu_url=null) {
        $sql = "SELECT 
                    a.menu_id 
                FROM app_menu_dashboard a 
                WHERE REPLACE(a.menu_url,'/','')=?";
        $query = $this->db->query($sql, $menu_url);
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function update($id=null) {
        $data = $_POST;
        //
        if ($data['label_menu_id'] == '1') {
            $data['label_menu_id'] = NULL;
        }
        if ($data['menu_showing'] !='') {
            $data['menu_showing'] = $data['menu_showing'];
        }else{
            $data['menu_showing'] = 2;
        }
        //
        $this->db->where('menu_id', $id);
        $outp = $this->db->update('app_menu_dashboard', $data);
        return outp_result($outp);
    }

    function delete($id=null) {
        $this->db->where('menu_id', $id);
        $outp = $this->db->delete('app_menu_dashboard');
        // delete to app_usergroup_menu
        $this->delete_app_usergroup_menu($id);
        //
        return outp_result($outp,'delete');
    }

    function delete_app_usergroup_menu($menu_id=null) {
        $this->db->where('menu_id', $menu_id);
        $outp = $this->db->delete('app_usergroup_menu');
        return $outp;
    }

    function delete_all() {
        $data = $_POST;
        foreach($data['cb_item'] as $key => $id) {
            $outp = $this->delete($id);
        }        
        return outp_result($outp,'delete');
    }

    /** API **/
    function api_get_all_menu_child($menu_parent = null, $menu_st = null) {
        $sql_where = "";
        if($menu_st != "") $sql_where .= " AND a.menu_st = '$menu_st'";
        //
        $sql_order_by = "ASC";
        //
        $sql = "SELECT 
                    a.menu_id as category_id, 
                    a.* 
                FROM app_menu_dashboard a 
                WHERE a.menu_parent=? 
                    $sql_where 
                ORDER BY a.menu_order 
                    $sql_order_by";
        $query = $this->db->query($sql, $menu_parent);
        $result = $query->result_array();
        return $result;
    }
}
