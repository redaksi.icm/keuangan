<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Error 404 | Nifty - Admin Template</title>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/nifty.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/demo/nifty-demo-icons.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/plugins/pace/pace.min.css" rel="stylesheet">
    <script src="<?=base_url()?>assets/plugins/pace/pace.min.js"></script>

</head>
<body>
    <div id="container" class="cls-container">
        <!-- HEADER -->
        <!-- <div class="cls-header">
            <div class="cls-brand">
                <a class="box-inline" href="index.html">
                    <img alt="Nifty Admin" src="<?=base_url()?>assets/images/logo.png" class="brand-icon">
                    <span class="brand-title">Nifty<span class="text-thin">Admin</span></span>
                </a>
            </div>
        </div> -->