<div class="cls-content">
    <h1 class="error-code text-info">404</h1>
    <p class="h4 text-uppercase text-bold">Page Not Found!</p>
    <div class="pad-btm">
        Maaf, Halaman yang Anda cari tidak ada. Silahkan klik tombol di bawah untuk kembali ke Halaman Utama
    </div>
    <hr class="new-section-sm bord-no">
    <div class="pad-top"><a class="btn btn-primary" href="<?=site_url('manage/index/location/index')?>">Return Home</a></div>
</div>