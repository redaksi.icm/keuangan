<div class="boxed">
<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">    
		<div class="pad-all text-center">
		    <h3>Welcome back to the Dashboard.</h3>
		    <p1>Scroll down to see quick links and overviews of your Server, To do list, Order status or get some Help using Nifty.</p1>
		</div>
    </div>
    <!--Page content-->
    <div id="page-content">
		    <div class="row">
		        <div class="col-lg-7">
		            
		        </div>
		        <div class="col-lg-5">
		            <div class="row">
		                <div class="col-sm-6 col-lg-6">
		                    <!--Sparkline Area Chart-->
		                    <div class="panel panel-success panel-colorful">
		                        <div class="pad-all">
		                            <p class="text-lg text-semibold"><i class="demo-pli-data-storage icon-fw"></i> HDD Usage</p>
		                            <p class="mar-no">
		                                <span class="pull-right text-bold">132Gb</span> Free Space
		                            </p>
		                            <p class="mar-no">
		                                <span class="pull-right text-bold">1,45Gb</span> Used space
		                            </p>
		                        </div>
		                        <div class="pad-top text-center">
		                            <!--Placeholder-->
		                            <div id="demo-sparkline-area" class="sparklines-full-content"></div>
		                        </div>
		                    </div>
		                </div>
		                <div class="col-sm-6 col-lg-6">
		                    <!--Sparkline Line Chart-->
		                    <div class="panel panel-info panel-colorful">
		                        <div class="pad-all">
		                            <p class="text-lg text-semibold">Earning</p>
		                            <p class="mar-no">
		                                <span class="pull-right text-bold">$764</span> Today
		                            </p>
		                            <p class="mar-no">
		                                <span class="pull-right text-bold">$1,332</span> Last 7 Day
		                            </p>
		                        </div>
		                        <div class="pad-top text-center">
		                            <!--Placeholder-->
		                            <div id="demo-sparkline-line" class="sparklines-full-content"></div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-sm-6 col-lg-6">
		                    <!--Sparkline bar chart -->
		                    <div class="panel panel-purple panel-colorful">
		                        <div class="pad-all">
		                            <p class="text-lg text-semibold"><i class="demo-pli-basket-coins icon-fw"></i> Sales</p>
		                            <p class="mar-no">
		                                <span class="pull-right text-bold">$764</span> Today
		                            </p>
		                            <p class="mar-no">
		                                <span class="pull-right text-bold">$1,332</span> Last 7 Day
		                            </p>
		                        </div>
		                        <div class="text-center">
		                            <!--Placeholder-->
		                            <div id="demo-sparkline-bar" class="box-inline"></div>
		                        </div>
		                    </div>
		                </div>
		                <div class="col-sm-6 col-lg-6">
		                    <!--Sparkline pie chart -->
		                    <div class="panel panel-warning panel-colorful">
		                        <div class="pad-all">
		                            <p class="text-lg text-semibold">Task Progress</p>
		                            <p class="mar-no">
		                                <span class="pull-right text-bold">34</span> Completed
		                            </p>
		                            <p class="mar-no">
		                                <span class="pull-right text-bold">79</span> Total
		                            </p>
		                        </div>
		                        <div class="pad-all">
		                            <div class="pad-btm">
		                                <div class="progress progress-sm">
		                                    <div style="width: 45%;" class="progress-bar progress-bar-light">
		                                        <span class="sr-only">45%</span>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="pad-btm">
		                                <div class="progress progress-sm">
		                                    <div style="width: 89%;" class="progress-bar progress-bar-light">
		                                        <span class="sr-only">89%</span>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <!--Extra Small Weather Widget-->
		            <div class="panel">
		                <div class="panel-body text-center clearfix">
		                    <div class="col-sm-4 pad-top">
		                        <div class="text-lg">
		                            <p class="text-5x text-thin text-main">95</p>
		                        </div>
		                        <p class="text-sm text-bold text-uppercase">New Friends</p>
		                    </div>
		                    <div class="col-sm-8">
		                        <button class="btn btn-pink mar-ver">View Details</button>
		                        <p class="text-xs">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
		                        <ul class="list-unstyled text-center bord-top pad-top mar-no row">
		                            <li class="col-xs-4">
		                                <span class="text-lg text-semibold text-main">1,345</span>
		                                <p class="text-sm text-muted mar-no">Following</p>
		                            </li>
		                            <li class="col-xs-4">
		                                <span class="text-lg text-semibold text-main">23K</span>
		                                <p class="text-sm text-muted mar-no">Followers</p>
		                            </li>
		                            <li class="col-xs-4">
		                                <span class="text-lg text-semibold text-main">278</span>
		                                <p class="text-sm text-muted mar-no">Post</p>
		                            </li>
		                        </ul>
		                    </div>
		                </div>
		            </div>
		            <!--End Extra Small Weather Widget-->
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-md-3">
		            <div class="panel panel-warning panel-colorful media middle pad-all">
		                <div class="media-left">
		                    <div class="pad-hor">
		                        <i class="demo-pli-file-word icon-3x"></i>
		                    </div>
		                </div>
		                <div class="media-body">
		                    <p class="text-2x mar-no text-semibold">241</p>
		                    <p class="mar-no">Documents</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-md-3">
		            <div class="panel panel-info panel-colorful media middle pad-all">
		                <div class="media-left">
		                    <div class="pad-hor">
		                        <i class="demo-pli-file-zip icon-3x"></i>
		                    </div>
		                </div>
		                <div class="media-body">
		                    <p class="text-2x mar-no text-semibold">241</p>
		                    <p class="mar-no">Zip Files</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-md-3">
		            <div class="panel panel-mint panel-colorful media middle pad-all">
		                <div class="media-left">
		                    <div class="pad-hor">
		                        <i class="demo-pli-camera-2 icon-3x"></i>
		                    </div>
		                </div>
		                <div class="media-body">
		                    <p class="text-2x mar-no text-semibold">241</p>
		                    <p class="mar-no">Photos</p>
		                </div>
		            </div>
		        </div>
		        <div class="col-md-3">
		            <div class="panel panel-danger panel-colorful media middle pad-all">
		                <div class="media-left">
		                    <div class="pad-hor">
		                        <i class="demo-pli-video icon-3x"></i>
		                    </div>
		                </div>
		                <div class="media-body">
		                    <p class="text-2x mar-no text-semibold">241</p>
		                    <p class="mar-no">Videos</p>
		                </div>
		            </div>
		        </div>
		    </div>
    </div>
    <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->
            
            

        

        
