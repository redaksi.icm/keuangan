<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">
        <!--Page Title-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Detail Data User</h1>
        </div>
        <!--End page title-->
        <!--Breadcrumb-->
        <ol class="breadcrumb">
		<li><a href="<?=site_url('manage/index/location/index')?>"><i class="demo-pli-home icon-bold"></i></a></li>
		<li><a href="<?=site_url('manage/index/location/master_user')?>">Data User</a></li>
		<li class="active">Detail</li>
        </ol>
        <!--End breadcrumb-->
    </div>
    <!--Page content-->
	<div id="page-content">
        <div class="panel">
	        <div class="panel-body">
	            <div class="fixed-fluid">
	                <div class="fixed-md-200 pull-sm-left fixed-right-border">
	
	                    <!-- Simple profile -->
	                    <div class="text-center">
	                        <div class="pad-ver">
                        	<?php if(@$main['user_photo'] != ''):?>
                        		<img src="<?=base_url()?>assets/images/user/<?=@$main['user_photo']?>" class="img-lg img-circle" alt="Profile Picture">
                        	<?php else: ?>
                        		<img src="<?=base_url()?>assets/images/user/no-image.png" class="img-lg img-circle" alt="Profile Picture">
                        	<?php endif; ?>
	                        </div>
	                        <h4 class="text-lg text-overflow mar-no"><?=@$main['user_realname']?></h4>
	                        <p class="text-sm text-muted"><?=@$main['user_group_name']?></p>
	                    </div>
	                    <hr>
	
	                    <!-- Profile Details -->
	                    <p class="pad-ver text-main text-sm text-uppercase text-bold">Detail Data User</p>
	                    <p><i class="ti-user icon-lg icon-fw icon-bold"></i> <b>Username</b> : <?=@$main['user_name']?></p>
	                    <p><i class="ti-comment-alt icon-lg icon-fw icon-bold"></i> <b>Nama</b> : <?=@$main['user_realname']?></p>
	                    <?php if (@$main['user_st'] !=0): ?>
	                    	<p><i class="ti-unlock icon-lg icon-fw icon-bold"></i> <b>Status</b> : <span class="label label-success">Aktif</span></p>
	                    <?php else: ?>
	                    	<p><i class="ti-lock icon-lg icon-fw icon-bold"></i> <b>Status</b> : <span class="label label-danger">Non Aktif</span></p>
	                    <?php endif; ?>
	                    <p><i class="ti-id-badge icon-lg icon-fw icon-bold"></i> <b>Group User</b> : <?=@$main['user_group_name']?></p>
	                    <p><i class="ti-calendar icon-lg icon-fw icon-bold"></i> <b>Last Login</b> : <?=(@$main['last_login'] !='') ? convert_date_indo(@$main['last_login']) : '-' ?></p>
	                    <p class="text-sm text-center"><?=@$main['user_description']?></p>
	                    <hr>
	                </div>
	                <div class="fluid">
	                	<div class="pull-left hidden-xs">
	                		<a href="<?=site_url('manage/index/location/master_user/'.$view_p.'/'.$view_o)?>" class="btn btn-sm btn-warning"><i class="ti-arrow-circle-left"></i> Kembali</a>
	                	</div>
	                    <div class="text-right">
	                        <a href="<?=site_url("manage/master_user/form/$view_p/$view_o/$main[user_id]")?>" class="btn btn-sm btn-primary"><i class="ti-pencil icon-lg icon-fw"></i> Edit User</a>
	                        <a href="<?=site_url("manage/master_user/delete/$view_p/$view_o/$main[user_id]")?>" class="btn btn-sm btn-danger delete-item"><i class="ti-trash icon-lg icon-fw"></i> Hapus User</a>
	                        <?php if (@$main['user_st'] !=0): ?>
	                        <a href="<?=site_url("manage/master_user/status/$view_p/$view_o/$main[user_id]/0")?>" class="btn btn-sm btn-purple"><i class="icon-lg icon-fw ti-lock"></i> Non Aktifkan User</a>
	                        <?php else: ?>
	                        <a href="<?=site_url("manage/master_user/status/$view_p/$view_o/$main[user_id]/1")?>" class="btn btn-sm btn-success"><i class="icon-lg icon-fw ti-unlock"></i> Aktifkan User</a>
	                        <?php endif; ?>
	                    </div>	
	                    <hr>
	                    <p class="text-main text-sm text-uppercase text-bold" style="margin-top: -10px; margin-bottom: -10px;"><i class="ion-folder icon-lg"></i> Data History Login</p>
	                    <hr>

	                    <!-- Newsfeed Content -->
	                    <?php foreach ($list_log_login as $row): ?>
	                    <div class="comments media-block">
	                        <div class="media-left pos-relative">
	                        	<img class="img-circle img-sm" alt="Profile Picture" src="<?=base_url()?>assets/images/icon/<?=img_perangkat($row['device_type'])?>">
	                        	<i class="<?=icon_perangkat($row['device_type'])?> centered icon-white icon-fw icon-2x"></i>
	                        </div>
	                        <div class="media-body">
	                            <div class="comment-header">
	                                <div class="media-heading box-inline text-main text-semibold"><?=$row['ip_address']?></div> | Sistem Operasi <div href="#" class="media-heading box-inline text-main text-semibold"><?=$row['os_type']?></div>
	                                <p class="text-muted text-sm"><i class="<?=icon_perangkat($row['device_type'])?> icon-lg"></i> - Dari <?=$row['device_type']?> - <?=time_elapsed($row['date_login'])?></p>
	                            </div>
	                            <div class="col-lg-6">
	                            	<ul style="margin-left: -12px;">
	                            		<li><span class="text-semibold">Kota</span> : <?=$row['kota']?></li>
	                            		<li><span class="text-semibold">Wilayah</span> : <?=$row['wilayah']?></li>
	                            		<li><span class="text-semibold">Negara</span> : <?=$row['negara_nm']?> <i class="flag-icon flag-icon-<?=strtolower($row['negara_kd'])?>" style="border: 1px solid #eee"></i></li>
	                            		<li><span class="text-semibold">Koordinat</span> : <?=$row['koordinat']?> <a href="https://www.google.com/maps/search/?api=1&amp;query=<?=$row['koordinat']?>" class="btn btn-xs btn-mint" target="_blank"><i class="ion-location"></i> Buka di Google Maps</a></li>
	                            	</ul>
	                            </div>
	                            <div class="col-lg-6">
	                            	<ul style="margin-left: -12px;">
	                            		<li><span class="text-semibold">Hostname</span> : <?=$row['hostname']?></li>
	                            		<li><span class="text-semibold">Browser</span> : <?=$row['browser_type']?></li>
	                            		<li><span class="text-semibold">Waktu Login</span> : <?=convert_date_indo($row['date_login'])?></li>
	                            		<li><span class="text-semibold">Login Lewat</span> : <?=$row['platform']?></li>
	                            	</ul>
	                            </div>
	                            <!-- <a class="btn btn-sm btn-info"><i class="ti-eye icon-lg icon-fw"></i> Selengkapnya </a> -->
	                        </div>
	                    </div>
	                	<?php endforeach; ?>
	                    <!-- End Newsfeed Content -->
	                    <!-- Pagination -->
						<?php $this->load->view('manage/template/pagination-with-id', array('url'=>'manage/master_user/detail','main'=>$list_log_login,'paging'=>$paging,'view_p'=>$view_p,'view_o'=>$view_o,'id'=>$id))?>
						<!-- End Pagination -->
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->