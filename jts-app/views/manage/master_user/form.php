<!-- js -->
<?php $this->load->view('manage/master_user/_js')?>
<!-- / -->
<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">
        <!--Page Title-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Form Data User</h1>
        </div>
        <!--End page title-->
        <!--Breadcrumb-->
        <ol class="breadcrumb">
		<li><a href="<?=site_url('manage/index/location/index')?>"><i class="demo-pli-home icon-bold"></i></a></li>
		<li><a href="<?=site_url('manage/index/location/master_user')?>">Data User</a></li>
		<li class="active">Form</li>
        </ol>
        <!--End breadcrumb-->
    </div>
    <!--Page content-->
    <div id="page-content">
        
		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel">
		            <div class="panel-heading">
		                <h3 class="panel-title"><i class="ti-user"></i> Form Data User</h3>
		            </div>
		            <form data-toggle="validator" role="form" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="form-horizontal">
		                <div class="panel-body">
		                    <p class="bord-btm pad-ver text-main text-bold">Form Isian Data User</p>
		                    <fieldset>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Username</label>
		                            <div class="col-lg-4">
		                                <input type="text" class="form-control input-sm" name="user_name" value="<?=@$main['user_name']?>" required="">
		                                <div class="help-block with-errors"></div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Password</label>
		                            <div class="col-lg-4">
		                                <input type="text" class="form-control input-sm" name="user_password" id="user_password" value="<?=@$main['user_password']?>" <?php if(@$main['user_password'] != '') echo 'disabled'?>>
		                            </div>
		                            <div class="col-lg-5">
		                            	<div class="checkbox">
							                <label for="cb_password" style="font-weight: bold;">Centang untuk merubah password </label>
							                <input name="cb_password" id="cb_password" class="magic-checkbox" type="checkbox">
							                <label for="cb_password"></label>
							            </div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Nama Lengkap</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control input-sm" name="user_realname" value="<?=@$main['user_realname']?>" required="">
		                                <div class="help-block with-errors"></div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Description</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control input-sm" name="user_description" value="<?=@$main['user_description']?>">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">User Group</label>
		                            <div class="col-lg-4">
		                                <select name="user_group" id="demo-select2" class="demo_select2 form-control" style="width: 100%;" required="">
		                                	<option value="">- Pilih User Group -</option>
			                                <?php foreach($list_usergroup as $usergroup):?>
			                                <option value="<?=$usergroup['usergroup_id']?>" <?php if($usergroup['usergroup_id'] == @$main['user_group']) echo 'selected'?>><?=$usergroup['usergroup_nm']?></option>
			                                <?php endforeach;?>
			                            </select>
			                            <div class="help-block with-errors"></div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Photo</label>
		                            <div class="col-lg-3">
		                                <div class="panel panel-default panel-colorful">
						                    <div class="pad-all text-center">
						                    	<div id="box_file">
						                    	<?php if(@$main['user_photo'] != ''):?>
						                        	<img class="img-lg img-circle mar-btm" src="<?=base_url()?>assets/images/user/<?=@$main['user_photo']?>">
						                    	<?php endif; ?>
						                    	</div>
						                        <div class="btn-group btn-group-justified">
						                        	<?php if(@$main['user_photo'] != ''):?>
						                        		<div class="row">
								                        	<div class="col-lg-6">
									                            <span class="btn btn-primary btn-labeled btn-file">
																    <i class="btn-label ti-upload"></i> Upload <input type="file" name="user_photo" value="<?=@$main['user_photo']?>">
																</span>
															</div>
															<div id="box_btn_delete" class="col-lg-6">
																<a href="javascript:void(0)" class="btn btn-danger btn-labeled add-tooltip delete_photo" data-id="<?=$main['user_id']?>" data-file="<?=$main['user_photo']?>" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Hapus Gambar"><i class="btn-label ti-trash"></i> Hapus</a>
															</div>
														</div>
						                        	<?php else: ?>
						                        		<center>
							                        		<span class="btn btn-primary btn-labeled btn-file">
															    <i class="btn-label ti-upload"></i> Upload Gambar <input type="file" name="user_photo">
															</span>
														</center>
						                        	<?php endif; ?>
													</center>
						                        </div>
						                    </div>
						                </div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Status</label>
		                            <div class="col-lg-2">
		                                <select name="user_st" class="selectpicker">
			                                <option value="1" <?php if(@$main['user_st'] == '1') echo 'selected'?>>Aktif</option>
                                			<option value="0" <?php if(@$main['user_st'] == '0') echo 'selected'?>>Tidak Aktif</option>
			                            </select>
		                            </div>
		                        </div>
		                    </fieldset>		
		                </div>
		                <div class="panel-footer">
		                    <div class="row">
		                        <div class="col-sm-7 col-sm-offset-3">
		                            <button class="btn btn-primary" type="submit"><i class="ion-paper-airplane"></i> Simpan</button>
		                            <button type="reset" class="btn btn-danger" onclick="location.href='<?=site_url("manage/index/location/master_user")?>'"><i class="ti-reload"></i> Batal</button>
		                        </div>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
    </div>
    <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->