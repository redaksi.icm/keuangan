        <script type="text/javascript">
        $(function() {
            $('#cb_password').bind('click',function() {
                var c = $(this).is(':checked');
                if(c == true) {
                    $('#user_password').removeAttr('disabled').focus().val('');
                } else {
                    location.reload(true);    
                }                
            });
            //
            <?php if(@$main['user_id'] != ''):?>
            _get_operator_pasar('<?=$main["user_group"]?>');
            <?php endif;?>
            $('#user_group').bind('change',function() {
                var i = $(this).val();
                _get_operator_pasar(i);
            });
            function _get_operator_pasar(i) {
                if(i == '2') {  // Operator
                    $('#box_operator_pasar').removeClass('hide');
                } else {
                    $('#box_operator_pasar').addClass('hide');
                }
            }
            //
            $('.delete_photo').bind('click',function(e) {
                e.preventDefault()
                swal({
                    title: "",
                    text: "Apakah anda yakin akan menghapus foto ini ?",
                    icon: "warning",
                    buttons: {
                      cancel: "❌ Batal",
                      danger: {
                        text: "Hapus",
                      },
                    },
                })
                .then((willDelete) => {
                    if (willDelete) {
                        var i = $(this).attr('data-id');
                        var f = $(this).attr('data-file');
                        $.get('<?=site_url("manage/master_user/ajax/delete_photo")?>?user_id='+i+'&user_photo='+f,null,function(data) {
                            if(data.callback == 'true') {
                                $('#box_file').hide();
                                $('#box_btn_delete').hide();
                                $('#user_photo').attr('data-file','');
                                swal({
                                    text: "Foto berhasil di hapus !",
                                    icon: "success",
                                    button: "OK",
                                });
                            }                    
                        },'json');
                    } else {
                      
                    }
                });
            });

        });
        </script>