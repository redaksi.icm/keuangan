<?=outp_notification('top')?>
<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">
        <div id="page-title">
            <h1 class="page-header text-overflow">Data User</h1>
        </div>
    </div>
    <!--Page content-->
    <div id="page-content">

    		<form id="form-index" action="<?=site_url('manage/master_user/search')?>" method="POST">
		    <div class="row pad-btm">
		        <div class="col-sm-2 toolbar-left">
		            <a class="btn btn-default add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Tambah Data" href="<?=site_url('manage/master_user/form')?>"><i class="ion-plus-circled"></i> Tambah User</a>
		        </div>
		        <div class="col-sm-3 text-center">
		        </div>
		        <div class="col-sm-2 text-center">
		        	<select name="ses_user_st" id="ses_user_st" class="chosen-select form-control" onchange="$('#form-index').submit()">
	                	<option value="">- Semua Status -</option>
	                	<option value="aktif" <?php if(@$ses_search['ses_user_st'] == 'aktif') echo 'selected'?>>Aktif</option>
                        <option value="tidak" <?php if(@$ses_search['ses_user_st'] == 'tidak') echo 'selected'?>>Tidak Aktif</option>
	                </select>
		        </div>
		        <div class="col-sm-2 text-center">
		        	<select name="ses_user_group" id="ses_user_group" class="chosen-select form-control" onchange="$('#form-index').submit()">
	                	<option value="">- Semua User Group -</option>
	                	<?php foreach($list_usergroup as $usergroup):?>
                        <option value="<?=$usergroup['usergroup_id']?>" <?php if($usergroup['usergroup_id'] == @$ses_search['ses_user_group']) echo 'selected'?>><?=$usergroup['usergroup_nm']?></option>
                        <?php endforeach;?>
	                </select>
		        </div>
		        <div class="col-sm-3 toolbar-right text-right">
		            <div class="input-group mar-btm">
	                    <input type="text" name="ses_txt_search" placeholder="Pencarian..." value="<?=@$ses_search['ses_txt_search']?>" class="form-control">
	                    <span class="input-group-btn">
	                        <button class="btn btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cari" type="submit"><i class="ion-search"></i></button>
	                        <a class="btn btn-danger add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Clear" href="<?=site_url('manage/index/location/master_user')?>"><i class="ion-loop"></i></a>
	                    </span>
	                </div>
		        </div>
		    </div>
			</form>
		    
		    <div class="row">
		    	<?php foreach($main as $row):?>
		        <div class="col-sm-4 col-md-3">
		            <div class="panel pos-rel">
		                <div class="pad-all text-center">
		                	<div class="text-left">
		                		<?php if ($row['user_st'] !=0): ?>
		                			<i class="ti-unlock icon-lg icon-bold text-success"></i>
	                    			<span class="label label-success">Aktif</span>
		                		<?php else: ?>
		                			<i class="ti-lock icon-lg icon-bold text-danger"></i>
	                    			<span class="label label-danger">Non Aktif</span>
		                		<?php endif; ?>
		                		<span class="label label-success">Online</span>
	                    	</div>
		                    <div class="widget-control">
		                        <div class="btn-group dropdown">
		                            <a href="#" class="dropdown-toggle btn btn-trans" data-toggle="dropdown" aria-expanded="false"><i class="demo-psi-dot-vertical icon-lg"></i></a>
		                            <ul class="dropdown-menu dropdown-menu-right" style="">
		                                <li><a href="<?=site_url("manage/master_user/form/$p/$o/$row[user_id]")?>"><i class="icon-lg icon-fw text-success ti-pencil"></i> Edit</a></li>
		                                <li><a href="<?=site_url("manage/master_user/detail/$p/$o/$row[user_id]")?>"><i class="icon-lg icon-fw text-info ti-eye"></i> Detail</a></li>
		                                <li><a href="<?=site_url("manage/master_user/delete/$p/$o/$row[user_id]")?>" class="delete-item"><i class="icon-lg icon-fw text-danger ti-trash"></i> Hapus</a></li>
		                                <li class="divider"></li>
		                                <?php if ($row['user_st'] !=0): ?>
		                                	<li><a href="<?=site_url("manage/master_user/status/$p/$o/$row[user_id]/0")?>"><i class="icon-lg icon-fw text-danger ti-lock"></i> Non Aktifkan</a></li>
		                                <?php else: ?>
		                                	<li><a href="<?=site_url("manage/master_user/status/$p/$o/$row[user_id]/1")?>"><i class="icon-lg icon-fw text-success ti-unlock"></i> Aktifkan</a></li>
		                                <?php endif; ?>
		                            </ul>
		                        </div>
		                    </div>
	                    	<?php if($row['user_photo'] != ''):?>
	                    		<img class="img-lg img-circle img-thumbnail mar-ver" src="<?=base_url()?>assets/images/user/<?=$row['user_photo']?>">
	                    	<?php else: ?>
	                    		<img class="img-lg img-circle img-thumbnail mar-ver" src="<?=base_url()?>assets/images/user/no-image.png">
	                    	<?php endif; ?>
	                        <p class="text-lg text-semibold mar-no text-main"><?=$row['user_realname']?></p>
	                        <p class="text-sm">Username : <?=$row['user_name']?></p>
	                        <p class="text-md text-semibold mar-no text-main"><?=$row['usergroup_nm']?></p>
	                        <p class="text-sm">Last Login : <?=($row['last_login'] !='') ? convert_date_indo($row['last_login']) : '-' ?></p>
		                    <div class="text-center pad-to">
		                        <div class="btn-group">
		                            <a href="<?=site_url("manage/master_user/form/$p/$o/$row[user_id]")?>" class="btn btn-sm btn-success add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Tooltip on top"><i class="ti-pencil icon-lg icon-fw"></i> Edit</a>
		                            <a href="<?=site_url("manage/master_user/detail/$p/$o/$row[user_id]")?>" class="btn btn-sm btn-primary add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Tooltip on top"><i class="ti-eye icon-lg icon-fw"></i> Detail</a>
		                            <a href="<?=site_url("manage/master_user/delete/$p/$o/$row[user_id]")?>" class="btn btn-sm btn-danger add-tooltip delete-item" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Tooltip on top"><i class="ti-trash icon-lg icon-fw"></i> Hapus</a>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    	<?php endforeach; ?>
		    </div>
	        <div class="well well-lg" style="background: #fff; padding-bottom: 38px;">
	        	<!-- Pagination -->
				<?php $this->load->view('manage/template/pagination', array('url'=>'manage/master_user/index','main'=>$main,'paging'=>$paging))?>
				<!-- End Pagination -->
	        </div>
    </div>
    <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->
            
            

        

        
