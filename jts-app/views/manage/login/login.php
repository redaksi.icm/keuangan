  <script type="text/javascript">
	$(function() {
		$('input[name="t_username"]').focus();
		//
		$('#btn-login').bind('click',function(e) {
			e.preventDefault();
			var u = $('input[name="t_username"]');
			var p = $('input[name="t_password"]');
			var captcha = $('input[name="t_captcha"]');
			$('#img-load-login').removeClass('hide');
			if(u.val() == '') {
				u.focus();
				$('#body-message').removeClass('hide').fadeOut().fadeIn('slow');
				$('#txt-message').html('<i class="fa fa-warning"></i> Maaf, Username harap diisi !');
				$('#img-load-login').addClass('hide');
			} else if(p.val() == '') {
				p.focus();
				$('#body-message').removeClass('hide').fadeOut().fadeIn('slow');
				$('#txt-message').html('<i class="fa fa-warning"></i> Maaf, Password harap diisi !');
				$('#img-load-login').addClass('hide');
			} else if(captcha.val() == '') {
				captcha.focus();
				$('#body-message').removeClass('hide').fadeOut().fadeIn('slow');
				$('#txt-message').html('<i class="fa fa-warning"></i> Maaf, Captcha harap diisi !');
				$('#img-load-login').addClass('hide');
			} else {
				$.post('<?=site_url("manage/login/ajax/auth_login")?>',$('#form-login').serialize(),function(data) {
					if(data.result == 'false') {
						u.focus();
						$('#body-message').removeClass('hide').fadeOut().fadeIn('slow');
						$('#txt-message').html(data.message);
						$('#img-load-login').addClass('hide');
					} else {
						location.href = data.redirect;
					}
				},'json');
				//
				setTimeout(function(){ 
					$('#body-message').removeClass('hide').fadeOut().fadeIn('slow');
					$('#txt-message').html('<i class="fa fa-warning"></i> Login Gagal, Silahkan cek koneksi internet Anda !');
					$('#img-load-login').addClass('hide');
				}, 35000);
			}
		});
		//
		function refresh_captcha(base_url) {
		  $.post('<?=site_url("manage/login/refresh_captcha")?>',$('form1').serialize(),function(data) {
		      $('#captcha_img').val(data.captcha_img);
		      $('#captcha').focus();
		  },'json');
		}
		//
		$('#refresh_captcha,#captcha_img').click(function() {
	    	refresh_captcha('<?=site_url()?>');
		});
	});
  </script>

  <body style="background-image: url('<?=base_url()?>assets/login/background/background-blue.png');">
  <!-- <body> -->
	<div class="login-wrapper">
		<div class="text-center" style="margin-bottom: -10px">
			<!-- <img src="<?=base_url()?>assets/images/simbok-logo-with-outline.png" class="fadeInUp animation-delay2" style="width: 420px;"> -->
		</div>
		<div class="text-center">
			<h2 class="fadeInUp animation-delay2" style="font-weight:bold">
				<div style="color:#ada3a3; text-shadow:0 1px #fff; font-size: 23px"><?=@$config['dinas_name']?></div>
				<div style="color:#ada3a3; text-shadow:0 1px #fff; font-size: 23px"><?=@$config['kabupaten']?></div>
			</h2>
		</div>
		<div class="login-widget animation-delay1">	
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<div class="pull-left">
						<i class="fa fa-lock fa-lg"></i> Login Aplikasi
					</div>
				</div>
				<div class="panel-body">
					<form method="post" id="form-login">
						<input type="hidden" name="t_token_login" value="<?=$token_val['token_login']?>">
						<div class="form-group">
							<label>Username</label>
							<input name="t_username" type="text" placeholder="Masukan username ..." class="form-control input-sm bounceIn animation-delay2" autocomplete="off">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input name="t_password" type="password" placeholder="Masukan password ..." class="form-control input-sm bounceIn animation-delay3" autocomplete="off">
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-2">
											<label style="margin-top: 5px;">Captcha</label>
										</div>
										<div class="col-md-5">
											<input name="t_captcha_hide" type="text" id="captcha_img" value="<?=captcha()?>" class="form-control input-sm bounceIn animation-delay3" autocomplete="off" readonly="" style="background: #2e74b0; color: #fff; text-align: center; font-size: 20px;">
										</div>
										<div class="col-md-2">
											<a href="javascript:void(0)" id="refresh_captcha"><img src="<?=base_url()?>assets/login/icon/refresh.png" style="margin-top: 7px; margin-left: -22px;"></a>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-5">
											<input style="margin-left: 66px; margin-top: 7px;" name="t_captcha" id="captcha" type="text" placeholder="Ketik Captcha Disini ..." class="form-control text-center input-sm bounceIn animation-delay3" autocomplete="off">
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="_token" value="<?=$_token?>">
						<hr/>	
						<div class="button-login">						
							<button class="btn btn-success btn-sm bounceIn animation-delay7 login-link pull-left" type="submit" id="btn-login"><i class="fa fa-sign-in"></i> Sign in</button>
							<img src="<?=base_url()?>assets/login/icon/loading.gif" id="img-load-login" class="img-load-login hide">
						</div>
						<div class="form-group" style="margin-top: 30px">
							<a class="pull-right" href="<?=site_url('')?>"><i class="fa fa-back"></i> &laquo; Kembali ke Web Portal</a>
						</div>
					</form>
				</div>
			</div><!-- /panel -->
			<div class="alert alert-danger hide" id="body-message">
				<div id="txt-message"></div>
			</div>
		</div><!-- /login-widget -->
	</div><!-- /login-wrapper -->
</body>