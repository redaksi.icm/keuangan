<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">
        <!--Page Title-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Form Data Group User</h1>
        </div>
        <!--End page title-->
        <!--Breadcrumb-->
        <ol class="breadcrumb">
		<li><a href="<?=site_url('manage/index/location/index')?>"><i class="demo-pli-home icon-bold"></i></a></li>
		<li><a href="<?=site_url('manage/index/location/master_usergroup')?>">Data Group User</a></li>
		<li class="active">Form</li>
        </ol>
        <!--End breadcrumb-->
    </div>
    <!--Page content-->
    <div id="page-content">
        
		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel">
		            <div class="panel-heading">
		                <h3 class="panel-title"><i class="ti-user"></i> Form Data Group User</h3>
		            </div>
		            <form data-toggle="validator" role="form" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="form-horizontal">
		                <div class="panel-body">
		                    <p class="bord-btm pad-ver text-main text-bold">Form Isian Data Group User</p>
		                    <fieldset>
		                    	<?php if ($id !=''): ?>
		                    	<div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">ID Group User</label>
		                            <div class="col-lg-1">
		                                <input type="text" class="form-control input-sm" value="<?=@$main['usergroup_id']?>" disabled>
		                            </div>
		                        </div>
		                    	<?php endif; ?>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Nama User Group</label>
		                            <div class="col-lg-4">
		                                <input type="text" class="form-control input-sm" name="usergroup_nm" value="<?=@$main['usergroup_nm']?>" required="">
		                                <div class="help-block with-errors"></div>
		                            </div>
		                        </div>
		                    </fieldset>		
		                </div>
		                <div class="panel-footer">
		                    <div class="row">
		                        <div class="col-sm-7 col-sm-offset-3">
		                            <button class="btn btn-primary" type="submit"><i class="ion-paper-airplane"></i> Simpan</button>
		                            <button type="reset" class="btn btn-danger" onclick="location.href='<?=site_url("manage/index/location/master_usergroup")?>'"><i class="ti-reload"></i> Batal</button>
		                        </div>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
    </div>
    <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->