<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">
        <!--Page Title-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Data Group User</h1>
        </div>
        <!--End page title-->
        <!--Breadcrumb-->
        <ol class="breadcrumb">
		<li><a href="<?=site_url('manage/index/location/index')?>"><i class="demo-pli-home icon-bold"></i></a></li>
		<li><a href="<?=site_url('manage/index/location/master_usergroup')?>">Data Group User</a></li>
		<li class="active">Index</li>
        </ol>
        <!--End breadcrumb-->
    </div>
    <!--Page content-->
    <div id="page-content">

		<div class="panel">
		    <div class="panel-heading">
		        <h3 class="panel-title">List Data Group User</h3>
		    </div>
		
		    <!--Data Table-->
		    <div class="panel-body">
		    	<form id="form-index" action="<?=site_url('manage/master_usergroup/search')?>" method="POST">
		        <div class="form-inline">
		            <div class="row">
		                <div class="col-sm-6 table-toolbar-left">
		                    <a href="<?=site_url('manage/master_usergroup/form')?>" class="btn btn-sm btn-primary add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Tambah Data"><i class="fa fa-plus-circle"></i> Tambah</a>
		                    <a class="btn btn-sm btn-danger add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Delete Multiple" id="delete-all" data-status="not_delete" data-action="<?=site_url('manage/master_usergroup/delete_all')?>"><i class="fa fa-trash-o"></i> Delete</a>
		                </div>
		                <div class="col-sm-6 table-toolbar-right">
		                    <div class="form-group">
		                        <input type="text" name="ses_txt_search" placeholder="Pencarian..." value="<?=@$ses_search['ses_txt_search']?>" class="form-control input-sm" autocomplete="off">
		                    </div>
		                    <div class="btn-group">
		                        <button type="submit" class="btn btn-sm btn-primary add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cari"><i class="fa fa-search"></i></button>
		                        <a href="<?=site_url('manage/index/location/master_usergroup')?>" class="btn btn-sm btn-danger add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Clear"><i class="fa fa-refresh"></i></a>
		                    </div>
		                </div>
		            </div>
		        </div>
		    	<?=outp_notification()?>
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover table-condensed">
		                <thead>
                            <tr>
                                <th class="text-center text-bold" width="3%">No</th>
                                <th class="text-center text-bold" width="3%">
                                    <div class="checkbox mar-no">
			                            <input id="demo-form-checkbox" class="magic-checkbox cb-all" type="checkbox">
			                            <label for="demo-form-checkbox"></label>
			                        </div>
                                </th>
                                <th class="text-center text-bold" width="8%">Aksi</th>
                                <th class="text-left text-bold"><?=a_sort(array('title'=>'Nama Group User','url'=>site_url('manage/master_usergroup/index/'.$p),'order'=>$o,'idxa'=>'1', 'idxb'=>'2'))?></th>
                            </tr>
                        </thead>
		                <tbody>
                            <?php foreach($main as $row):?>
                            <tr>
                                <td class="text-center"><?=$row['no']?></td>
                                <td class="text-center">
                                    <div class="checkbox mar-no">
			                            <input id="checkbox-<?=$row['md5_usergroup_id']?>" class="magic-checkbox cb-item" type="checkbox" name="cb_item[]" value="<?=$row['md5_usergroup_id']?>">
			                            <label for="checkbox-<?=$row['md5_usergroup_id']?>"></label>
			                        </div>
                                </td>
                                <td class="text-center">
                                    <a href="<?=site_url("manage/master_usergroup/config/$p/$o/$row[md5_usergroup_id]")?>" data-toggle="tooltip" data-placement="top" data-original-title="Konfigurasi Hak Akses Menu"><i class="fa fa-gear fa-lg text-info"></i></a>
                                    <a href="<?=site_url("manage/master_usergroup/form/$p/$o/$row[md5_usergroup_id]")?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit Data"><i class="fa fa-edit fa-lg text-success"></i></a>
                                    <?php if ($row['usergroup_id'] !='1' && $row['usergroup_id'] !='2'): ?>
                                      <a href="<?=site_url("manage/master_usergroup/delete/$p/$o/$row[md5_usergroup_id]")?>" class="delete-item"><i class="fa fa-trash-o fa-lg text-danger" data-toggle="tooltip" data-placement="top" data-original-title="Delete Data"></i></a>
                                    <?php endif; ?>
                                </td>
                                <td class="text-left"><?=$row['usergroup_nm']?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
		            </table>
		        </div>
		        </form>
		        <!-- Pagination -->
				<?php $this->load->view('manage/template/pagination', array('url'=>'manage/master_usergroup/index','main'=>$main,'paging'=>$paging))?>
				<!-- End Pagination -->
		    </div>
		    <!--End Data Table-->
		</div>
    </div>
    <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->
            
            

        

        
