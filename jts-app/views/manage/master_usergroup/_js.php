<script type="text/javascript">
$(function() {
  //
  $('.check-all').click(function (e) {
    var menu_id = $(this).data('menu_id');
    $('.check-read-'+menu_id).prop('checked', this.checked);
    $('.check-add-'+menu_id).prop('checked', this.checked);
    $('.check-edit-'+menu_id).prop('checked', this.checked);
    $('.check-delete-'+menu_id).prop('checked', this.checked);
  });
  //
  $('.check-add').click(function (e) {
    var menu_id = $(this).data('menu_id');
    var read_st = $('.check-read-'+menu_id).data('read_st');
    if (read_st != '1') {
      $('.check-read-'+menu_id).prop('checked', this.checked);
    }else{
      if($('.check-read-'+menu_id).prop('checked') == false){
        $('.check-read-'+menu_id).prop('checked', this.checked);
      }
    }
  });
  //
  $('.check-edit').click(function (e) {
    var menu_id = $(this).data('menu_id');
    var read_st = $('.check-read-'+menu_id).data('read_st');
    if (read_st != '1') {
      $('.check-read-'+menu_id).prop('checked', this.checked);
    }else{
      if($('.check-read-'+menu_id).prop('checked') == false){
        $('.check-read-'+menu_id).prop('checked', this.checked);
      }
    }
  });
  //
  $('.check-delete').click(function (e) {
    var menu_id = $(this).data('menu_id');
    var read_st = $('.check-read-'+menu_id).data('read_st');
    if (read_st != '1') {
      $('.check-read-'+menu_id).prop('checked', this.checked);
    }else{
      if($('.check-read-'+menu_id).prop('checked') == false){
        $('.check-read-'+menu_id).prop('checked', this.checked);
      }
    }
  });
  //
});
</script>