<!-- JS -->
<?php $this->load->view('manage/master_usergroup/_js')?>
<!-- End JS -->
<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">
        <!--Page Title-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Form Konfigurasi Hak Akses Menu User</h1>
        </div>
        <!--End page title-->
        <!--Breadcrumb-->
        <ol class="breadcrumb">
		<li><a href="<?=site_url('manage/index/location/index')?>"><i class="demo-pli-home icon-bold"></i></a></li>
		<li><a href="<?=site_url('manage/index/location/master_usergroup')?>">Data Group User</a></li>
		<li class="active">Konfigurasi</li>
        </ol>
        <!--End breadcrumb-->
    </div>
    <!--Page content-->
    <div id="page-content">
        
		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel">
		            <div class="panel-heading">
		                <h3 class="panel-title"><i class="ti-user"></i> Form Konfigurasi Hak Akses Menu User</h3>
		            </div>
		            <form data-toggle="validator" role="form" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="form-horizontal">
		                <div class="panel-body">
		                    <p class="bord-btm pad-ver text-main text-bold">Form Isian Konfigurasi Hak Akses Menu User</p>
		                    <fieldset id="checkArray">
		                    	<div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">ID Group User</label>
		                            <div class="col-lg-1">
		                                <input type="text" class="form-control input-sm" value="<?=@$main['usergroup_id']?>" disabled>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Nama User Group</label>
		                            <div class="col-lg-4">
		                                <input type="text" class="form-control input-sm" value="<?=@$main['usergroup_nm']?>" disabled>
		                            </div>
		                        </div>
                            <style type="text/css">
                              
                            </style>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Data Menu Dashboard</label>
		                            <div class="col-lg-10">
		                            	<div class="table-responsive">
		                                <table class="table table-striped table-bordered table-hover table-condensed table-option">
                                      <thead>
                                        <tr>
                                          <th class="text-center" width="3%">No</th>
                                          <th class="text-center">Judul Menu</th>
                                          <th class="text-center" width="21%">Label Menu</th>
                                          <th class="text-center" width="10%">Baca</th>
                                          <th class="text-center" width="10%">Tambah</th>
                                          <th class="text-center" width="10%">Edit</th>
                                          <th class="text-center" width="10%">Hapus</th>
                                          <th class="text-center" width="11%">Semua Aktif</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php foreach($list_menu_dashboard as $row):?>
                                        <tr style="font-weight: bold;">
                                          <td class="text-center">
                                            <?=$row['no']?>
                                          </td>
                                          <td><i class="<?=$row['menu_icon']?>"></i> <?=$row['menu_title']?></td>
                                          <td align="center"><?=$row['label_menu_nm']?></td>
                                          <td class="text-center">
                                            <div class="checkbox mar-no">
			                                        <input id="checkbox-read-<?=$row['menu_id']?>" class="magic-checkbox check-read check-read-<?=$row['menu_id']?>" data-menu_id="<?=$row['menu_id']?>" data-read_st="<?=$row['get_usergroup_menu']['read_st']?>" type="checkbox" name="read_st[]" value="<?=$row['menu_id']?>" <?php if(@$row['get_usergroup_menu']['read_st'] == '1') echo 'checked'?>>
			                                        <label for="checkbox-read-<?=$row['menu_id']?>"></label>
				                                    </div>
                                          </td> 
                                          <td class="text-center">
                                            <div class="checkbox mar-no">
                                              <input id="checkbox-add-<?=$row['menu_id']?>" class="magic-checkbox check-add check-add-<?=$row['menu_id']?>" data-menu_id="<?=$row['menu_id']?>" data-add_st="<?=$row['get_usergroup_menu']['add_st']?>" type="checkbox" name="add_st[]" value="<?=$row['menu_id']?>" <?php if(@$row['get_usergroup_menu']['add_st'] == '1') echo 'checked'?>>
                                              <label for="checkbox-add-<?=$row['menu_id']?>"></label>
                                            </div>
                                          </td>   
                                          <td class="text-center">
                                            <div class="checkbox mar-no">
                                              <input id="checkbox-edit-<?=$row['menu_id']?>" class="magic-checkbox check-edit check-edit-<?=$row['menu_id']?>" data-menu_id="<?=$row['menu_id']?>" type="checkbox" name="edit_st[]" value="<?=$row['menu_id']?>" <?php if(@$row['get_usergroup_menu']['edit_st'] == '1') echo 'checked'?>>
                                              <label for="checkbox-edit-<?=$row['menu_id']?>"></label>
                                            </div>
                                          </td>   
                                          <td class="text-center">
                                            <div class="checkbox mar-no">
                                              <input id="checkbox-delete-<?=$row['menu_id']?>" class="magic-checkbox check-delete check-delete-<?=$row['menu_id']?>" data-menu_id="<?=$row['menu_id']?>" type="checkbox" name="delete_st[]" value="<?=$row['menu_id']?>" <?php if(@$row['get_usergroup_menu']['delete_st'] == '1') echo 'checked'?>>
                                              <label for="checkbox-delete-<?=$row['menu_id']?>"></label>
                                            </div>
                                          </td>   
                                          <td class="text-center">
                                            <div class="checkbox mar-no">
                                              <input id="checkbox-all-<?=$row['menu_id']?>" class="magic-checkbox check-all" data-menu_id="<?=$row['menu_id']?>" type="checkbox" name="all_st[]" value="<?=$row['menu_id']?>" <?php if(@$row['get_usergroup_menu']['read_st'] == '1' && @$row['get_usergroup_menu']['edit_st'] == '1' && @$row['get_usergroup_menu']['delete_st'] == '1') echo 'checked'?>>
                                              <label for="checkbox-all-<?=$row['menu_id']?>"></label>
                                            </div>
                                          </td>                                        
                                          </tr>
                                          <?php foreach($row['menu_child'] as $child):?>
                                            <tr>
                                                <td class="text-center">
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-level-up fa-rotate-90"></i> <?=$child['menu_title']?></td>     
                                                <td align="center"><?=$child['label_menu_nm']?></td>
                                                <td class="text-center">
                                                    <div class="checkbox mar-no">
  						                                        <input id="checkbox-read-<?=$child['menu_id']?>" class="magic-checkbox check-read check-read-<?=$child['menu_id']?>" data-menu_id="<?=$child['menu_id']?>" data-read_st="<?=$child['get_usergroup_menu']['read_st']?>" type="checkbox" name="read_st[]" value="<?=$child['menu_id']?>" <?php if(@$child['get_usergroup_menu']['read_st'] == '1') echo 'checked'?>>
  						                                        <label for="checkbox-read-<?=$child['menu_id']?>"></label>
      						                                  </div>
                                                </td> 
                                                <td class="text-center">
                                                    <div class="checkbox mar-no">
                                                      <input id="checkbox-add-<?=$child['menu_id']?>" class="magic-checkbox check-add check-add-<?=$child['menu_id']?>" data-menu_id="<?=$child['menu_id']?>" data-add_st="<?=$child['get_usergroup_menu']['add_st']?>" type="checkbox" name="add_st[]" value="<?=$child['menu_id']?>" <?php if(@$child['get_usergroup_menu']['add_st'] == '1') echo 'checked'?>>
                                                      <label for="checkbox-add-<?=$child['menu_id']?>"></label>
                                                    </div>
                                                </td>  
                                                <td class="text-center">
                                                    <div class="checkbox mar-no">
                                                      <input id="checkbox-edit-<?=$child['menu_id']?>" class="magic-checkbox check-edit check-edit-<?=$child['menu_id']?>" data-menu_id="<?=$child['menu_id']?>" type="checkbox" name="edit_st[]" value="<?=$child['menu_id']?>" <?php if(@$child['get_usergroup_menu']['edit_st'] == '1') echo 'checked'?>>
                                                      <label for="checkbox-edit-<?=$child['menu_id']?>"></label>
                                                    </div>
                                                </td>  
                                                <td class="text-center">
                                                    <div class="checkbox mar-no">
                                                      <input id="checkbox-delete-<?=$child['menu_id']?>" class="magic-checkbox check-delete check-delete-<?=$child['menu_id']?>" data-menu_id="<?=$child['menu_id']?>" type="checkbox" name="delete_st[]" value="<?=$child['menu_id']?>" <?php if(@$child['get_usergroup_menu']['delete_st'] == '1') echo 'checked'?>>
                                                      <label for="checkbox-delete-<?=$child['menu_id']?>"></label>
                                                    </div>
                                                </td>  
                                                <td class="text-center">
                                                    <div class="checkbox mar-no">
                                                      <input id="checkbox-all-<?=$child['menu_id']?>" class="magic-checkbox check-all" data-menu_id="<?=$child['menu_id']?>" type="checkbox" name="all_st[]" value="<?=$child['menu_id']?>" <?php if(@$child['get_usergroup_menu']['read_st'] == '1' && @$child['get_usergroup_menu']['edit_st'] == '1' && @$child['get_usergroup_menu']['delete_st'] == '1') echo 'checked'?>>
                                                      <label for="checkbox-all-<?=$child['menu_id']?>"></label>
                                                    </div>
                                                </td>                                            
                                            </tr>
                                              <?php foreach($child['menu_level_1'] as $menu_level_1):?>
                                              <tr>
                                                  <td class="text-center">
                                                  </td>
                                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-level-up fa-rotate-90"></i> <?=$menu_level_1['menu_title']?></td>
                                                  <td align="center"><?=$menu_level_1['label_menu_nm']?></td>
                                                  <td class="text-center">
                                                      <div class="checkbox mar-no">
      							                                    <input id="checkbox-read-<?=$menu_level_1['menu_id']?>" class="magic-checkbox check-read check-read-<?=$menu_level_1['menu_id']?>" data-menu_id="<?=$menu_level_1['menu_id']?>" data-read_st="<?=$menu_level_1['get_usergroup_menu']['read_st']?>" type="checkbox" name="read_st[]" value="<?=$menu_level_1['menu_id']?>" <?php if(@$menu_level_1['get_usergroup_menu']['menu_id'] == $menu_level_1['menu_id']) echo 'checked'?>>
      							                                    <label for="checkbox-read-<?=$menu_level_1['menu_id']?>"></label>
        							                                </div>
                                                  </td>  
                                                  <td class="text-center">
                                                      <div class="checkbox mar-no">
                                                        <input id="checkbox-add-<?=$menu_level_1['menu_id']?>" class="magic-checkbox check-add check-add-<?=$menu_level_1['menu_id']?>" data-menu_id="<?=$menu_level_1['menu_id']?>" data-add_st="<?=$menu_level_1['get_usergroup_menu']['add_st']?>" type="checkbox" name="add_st[]" value="<?=$menu_level_1['menu_id']?>" <?php if(@$menu_level_1['get_usergroup_menu']['menu_id'] == $menu_level_1['menu_id']) echo 'checked'?>>
                                                        <label for="checkbox-add-<?=$menu_level_1['menu_id']?>"></label>
                                                      </div>
                                                  </td>  
                                                  <td class="text-center">
                                                      <div class="checkbox mar-no">
                                                        <input id="checkbox-edit-<?=$menu_level_1['menu_id']?>" class="magic-checkbox check-edit check-edit-<?=$menu_level_1['menu_id']?>" data-menu_id="<?=$menu_level_1['menu_id']?>" type="checkbox" name="edit_st[]" value="<?=$menu_level_1['menu_id']?>" <?php if(@$menu_level_1['get_usergroup_menu']['menu_id'] == $menu_level_1['menu_id']) echo 'checked'?>>
                                                        <label for="checkbox-edit-<?=$menu_level_1['menu_id']?>"></label>
                                                      </div>
                                                  </td>  
                                                  <td class="text-center">
                                                      <div class="checkbox mar-no">
                                                        <input id="checkbox-delete-<?=$menu_level_1['menu_id']?>" class="magic-checkbox check-delete check-delete-<?=$menu_level_1['menu_id']?>" data-menu_id="<?=$menu_level_1['menu_id']?>" type="checkbox" name="delete_st[]" value="<?=$menu_level_1['menu_id']?>" <?php if(@$menu_level_1['get_usergroup_menu']['menu_id'] == $menu_level_1['menu_id']) echo 'checked'?>>
                                                        <label for="checkbox-delete-<?=$menu_level_1['menu_id']?>"></label>
                                                      </div>
                                                  </td>  
                                                  <td class="text-center">
                                                      <div class="checkbox mar-no">
                                                        <input id="checkbox-all-<?=$menu_level_1['menu_id']?>" class="magic-checkbox check-all" data-menu_id="<?=$menu_level_1['menu_id']?>" type="checkbox" name="all_st[]" value="<?=$menu_level_1['menu_id']?>" <?php if(@$menu_level_1['get_usergroup_menu']['menu_id'] == $menu_level_1['menu_id']) echo 'checked'?>>
                                                        <label for="checkbox-all-<?=$menu_level_1['menu_id']?>"></label>
                                                      </div>
                                                  </td>                                            
                                              </tr>
                                              <?php endforeach;?>
                                            <?php endforeach;?>
                                            <?php endforeach; ?>
                                          </tbody>
                                      </table>
                                  	</div>
		                            </div>
		                        </div>
		                    </fieldset>		
		                </div>
		                <div class="panel-footer">
		                    <div class="row">
		                        <div class="col-sm-7 col-sm-offset-3">
		                            <button class="btn btn-primary" type="submit"><i class="ion-paper-airplane"></i> Simpan</button>
		                            <button type="reset" class="btn btn-danger" onclick="location.href='<?=site_url("manage/index/location/master_usergroup")?>'"><i class="ti-reload"></i> Batal</button>
		                        </div>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
    </div>
    <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->