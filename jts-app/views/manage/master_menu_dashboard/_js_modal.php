<script type="text/javascript">
    // List Label Menu
    $('#btn_label_menu').click(function () {
        get_list_label_menu();
        $('#modal_list_label_menu').modal('show');
    });

    // Form Label Menu
    $('#btn_room').click(function () {
        $('#label_menu_nm').val('');
        $('#label_menu_order').val('');
        $('#modal_form_label_menu').modal('show');
        $('#modal_list_label_menu').modal('hide');
    });

    // Validate Proses Form Label Menu
    $('#btn_add_label_menu').click(function () {
      var label_menu_nm = $('#label_menu_nm').val();
      var label_menu_order = $('#label_menu_order').val();
      //
      if (label_menu_nm == '') {
        swal({
          text: "Nama Label belum diisi !",
          icon: "warning",
          button: "OK",
        });
      }else if (label_menu_order == '') {
        swal({
          text: "No.Urut belum diisi !",
          icon: "warning",
          button: "OK",
        });
      }else {
        add_label_menu();
      }
    });

    // Proses Form Label Menu
    function add_label_menu() {
        var label_menu_nm = $('#label_menu_nm').val();
        var label_menu_order = $('#label_menu_order').val();
        //
        $.ajax({
          type : 'post',
          url : '<?=base_url()?>manage/master_menu_dashboard/insert_label_menu',
          data : 'label_menu_nm='+label_menu_nm+'&label_menu_order='+label_menu_order,
          success : function (data) {
            $('#modal_list_label_menu').modal('show');
            $('#modal_form_label_menu').modal('hide');
            get_list_label_menu();
          }
        })
    }

    // Validate Proses Update Label Menu
    $('#btn_update_label_menu').click(function () {
      var update_label_menu_nm = $('#update_label_menu_nm').val();
      var update_label_menu_order = $('#update_label_menu_order').val();
      //
      if (update_label_menu_nm == '') {
        swal({
          text: "Nama Label belum diisi !",
          icon: "warning",
          button: "OK",
        });
      }else if (update_label_menu_order == '') {
        swal({
          text: "No.Urut belum diisi !",
          icon: "warning",
          button: "OK",
        });
      }else {
        update_label_menu();
      }
    });

    // Proses Form Update Label Menu
    function update_label_menu() {
        var update_label_menu_id = $('#update_label_menu_id').val();
        var update_label_menu_nm = $('#update_label_menu_nm').val();
        var update_label_menu_order = $('#update_label_menu_order').val();

        $.ajax({
          type : 'post',
          url : '<?=base_url()?>manage/master_menu_dashboard/update_label_menu',
          data : 'label_menu_id='+update_label_menu_id+'&label_menu_nm='+update_label_menu_nm+'&label_menu_order='+update_label_menu_order,
          success : function (data) {
            $('#modal_list_label_menu').modal('show');
            $('#modal_update_label_menu').modal('hide');
            get_list_label_menu();
          }
        })
    }

    // Validate Proses Hapus Label Menu
    $('#btn_update_label_menu').click(function () {
      var update_label_menu_nm = $('#update_label_menu_nm').val();
      var update_label_menu_order = $('#update_label_menu_order').val();
      //
      if (update_label_menu_nm == '') {
        swal({
          text: "Nama Label belum diisi !",
          icon: "warning",
          button: "OK",
        });
      }else if (update_label_menu_order == '') {
        swal({
          text: "No.Urut belum diisi !",
          icon: "warning",
          button: "OK",
        });
      }else {
        update_label_menu();
      }
    });

    // Proses Hapus Label Menu
    function delete_label_menu(label_menu_id) {
        swal({
            title: "",
            text: "Apakah Anda yakin akan menghapus data ini ?",
            icon: "warning",
            buttons: {
                    cancel: "Batal",
                    sucess: {
                      text: "Hapus",
                    },
                  },
        })
          .then((willProcess) => {
            if (willProcess) {
                $.ajax({
                    type : 'post',
                    url : '<?=base_url()?>manage/master_menu_dashboard/delete_label_menu',
                    data : 'label_menu_id='+label_menu_id,
                    success : function () {
                    get_list_label_menu();
                    }
                })
            }
        });
    }

    // Back to list
    $('.btn_back_label_menu').click(function () {
        $('#modal_form_label_menu').modal('hide');
        $('#modal_update_label_menu').modal('hide');
        $('#modal_list_label_menu').modal('show');
    });

    function get_list_label_menu() {
        $.ajax({
          type : 'post',
          url : '<?=base_url()?>manage/master_menu_dashboard/get_list_label_menu',
          data : '',
          dataType : 'json',
          success : function (data) {
            $("#row_list_label_menu").html('');
            if (data.list_label_menu == null || data.list_label_menu == '') {
                var row = '<tr>'+
                    '<td class="text-center" colspan="4">Data tidak ada!</td>'+
                    '</tr>';
                $("#row_list_label_menu").append(row);
            } else { 
                var $select = $('#label_menu_id').empty();
                var $testing = $('#testing').empty();
                var row_label_menu_id = "<?=@$main['label_menu_id']?>";
                $.each(data.list_label_menu, function(i, item) {
                    if (item.label_menu_id != '1') {
                        var row = '<tr>'+
                        '<td align="center">'+item.no+'</td>'+
                        '<td class="text-center">'+
                            '<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="Edit Data" onclick="update_label_menu_show('+item.label_menu_id+')"><i class="fa fa-edit fa-lg text-success"></i></a>'+
                            '&nbsp;'+
                            '<a href="javascript:void(0)" class="delete-item" data-toggle="tooltip" data-placement="top" data-original-title="Delete Data" onclick="delete_label_menu('+item.label_menu_id+')"><i class="fa fa-trash-o fa-lg text-danger"></i></a>'+
                        '</td>'+
                        '<td>'+item.label_menu_nm+'</td>'+
                        '<td align="center">'+item.label_menu_order+'</td>'+
                        '</tr>';
                        $("#row_list_label_menu").append(row);
                    }
                    //
                    var n = '<li data-original-index="'+item.nomor+'">'+
                    '<a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false">'+
                      '<span class="text">'+item.label_menu_nm+'</span>'+
                      '<span class="glyphicon glyphicon-ok check-mark"></span>'+
                    '</a>'+
                    '</li>';
                    ($testing).append(n);
                    //
                    var o = $('<option/>', { value: item.label_menu_id })
                        .text(item.label_menu_nm)
                        .prop('selected', item.label_menu_id == row_label_menu_id);
                    o.appendTo($select);
                })
            }
          }
        })
    }

    function update_label_menu_show(label_menu_id) {
        $.ajax({
          type : 'post',
          url : '<?=base_url()?>manage/master_menu_dashboard/update_label_menu_show',
          dataType : 'json',
          data : 'label_menu_id='+label_menu_id,
          success : function (data) {
            $("#update_label_menu_id").val(data.label_menu_id);
            $("#update_label_menu_nm").val(data.label_menu_nm);
            $("#update_label_menu_order").val(data.label_menu_order);
            //
            $("#modal_list_label_menu").modal('hide');
            $("#modal_update_label_menu").modal('show');

          }
        })
    }
    </script>