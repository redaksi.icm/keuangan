<!-- List Label Menu -->
<div id="modal_list_label_menu" class="modal fade"  role="dialog" aria-labelledby="modal_list_label_menu">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="title_room_list">Manajemen Label Menu</h4>
      </div>
      <div class="modal-body" style="margin-bottom: -20px;">
        <button class="btn btn-info btn-sm" id="btn_room"><i class="fa fa-plus"></i> Tambah Data</button>
        <br><br>
        <table id="tbl_room_list" class="table table-bordered table-condensed table-striped table-hover">
          <thead>
            <tr>
              <th class="text-center" width="8%">No</th>
              <th class="text-center" width="15%">Aksi</th>
              <th class="text-center">Nama Label</th>
              <th class="text-center" width="20%">No Urut</th>
            </tr>
          </thead>
          <tbody id="row_list_label_menu">
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Form Label Menu -->
<div id="modal_form_label_menu" class="modal fade"  role="dialog" aria-labelledby="modal_form_label_menu">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="title_room_list">Form Input Label Menu</h4>
      </div>
      <div class="modal-body" style="margin-bottom: -20px;">
        <div class="form-horizontal">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="" class="col-lg-3 control-label align-left">Nama Label</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control input-sm" id="label_menu_nm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-lg-3 control-label align-left">No.Urut</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control input-sm" id="label_menu_order">
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        <button type="button" class="btn btn-info btn-sm btn_back_label_menu"><i class="fa fa-arrow-left"></i> Kembali</button>
        <button type="button" class="btn btn-success btn-sm" id="btn_add_label_menu"><i class="fa fa-sign-in"></i> Simpan</button>
      </div>
    </div>
  </div>
</div>

<!-- Form Update Label Menu -->
<div id="modal_update_label_menu" class="modal fade"  role="dialog" aria-labelledby="modal_room">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="title_room_list">Form Edit Label Menu</h4>
      </div>
      <div class="modal-body" style="margin-bottom: -20px;">
        <input id="update_label_menu_id" type="hidden" value="" readonly>

        <div class="form-horizontal">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="" class="col-lg-3 control-label align-left">Nama Label</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control input-sm" id="update_label_menu_nm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-lg-3 control-label align-left">No.Urut</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control input-sm" id="update_label_menu_order">
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
        <button type="button" class="btn btn-info btn-sm btn_back_label_menu"><i class="fa fa-arrow-left"></i> Kembali</button>
        <button type="button" class="btn btn-success btn-sm" id="btn_update_label_menu"><i class="fa fa-sign-in"></i> Simpan</button>
      </div>
    </div>
  </div>
</div>