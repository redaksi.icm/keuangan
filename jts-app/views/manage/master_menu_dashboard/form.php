<!-- JS -->
<?php $this->load->view('manage/master_menu_dashboard/_js', @$main)?>
<!-- End JS -->
<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">
        <!--Page Title-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Form Data Menu Dashboard</h1>
        </div>
        <!--End page title-->
        <!--Breadcrumb-->
        <ol class="breadcrumb">
		<li><a href="<?=site_url('manage/index/location/index')?>"><i class="demo-pli-home icon-bold"></i></a></li>
		<li><a href="<?=site_url('manage/index/location/master_menu_dashboard')?>">Data Menu Dashboard</a></li>
		<li class="active">Form</li>
        </ol>
        <!--End breadcrumb-->
    </div>
    <!--Page content-->
    <div id="page-content">
        
		<div class="row">
		    <div class="col-lg-8">
		        <div class="panel">
		            <div class="panel-heading">
		                <h3 class="panel-title"><i class="ti-user"></i> Form Data Menu Dashboard</h3>
		            </div>
		            <form data-toggle="validator" role="form" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="form-horizontal">
		                <div class="panel-body">
		                    <p class="bord-btm pad-ver text-main text-bold">Form Isian Data Menu Dashboard</p>
		                    <fieldset>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Label Menu</label>
		                            <div class="col-lg-4">
		                                <select name="label_menu_id" id="label_menu_id" class="selectpicker">
			                                <?php foreach($list_label_menu as $lm):?>
                                            <option value="<?=$lm['label_menu_id']?>" <?php if($lm['label_menu_id'] == @$main['label_menu_id']) echo 'selected'?>><?=$lm['label_menu_nm']?></option>
                                            <?php endforeach;?>
			                            </select>
		                            </div>
		                            <div class="col-lg-5">
		                            	<button class="btn btn-info btn-sm" id="btn_label_menu" type="button">
                                            <i class="fa fa-gear"></i> Manajemen Label Menu
                                        </button>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Induk Menu</label>
		                            <div class="col-lg-5">
		                                <select name="menu_parent" id="menu_parent" class="chosen-select form-control" style="width: 100%;">
		                                	<option value="">- None -</option>
                                            <?php foreach($list_menu_parent as $mp):?>
                                            <option value="<?=$mp['menu_id']?>" <?php if($mp['menu_id'] == @$main['menu_parent']) echo 'selected'?>><?=$mp['menu_title']?></option>
                                            <?php endforeach;?>
			                            </select>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Judul Menu</label>
		                            <div class="col-lg-8">
		                                <input type="text" class="form-control input-sm" name="menu_title" id="menu_title" value="<?=@$main['menu_title']?>" required="">
		                                <div class="help-block with-errors"></div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">URL Menu</label>
		                            <div class="col-lg-8">
		                                <input type="text" class="form-control input-sm" name="menu_url" value="<?=@$main['menu_url']?>" required="">
		                                <div class="help-block with-errors"></div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Urut</label>
		                            <div class="col-lg-2">
		                                <input type="text" class="form-control input-sm" name="menu_order" value="<?=@$main['menu_order']?>" required="">
		                                <div class="help-block with-errors"></div>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Icon</label>
		                            <div class="col-lg-4">
		                                <input type="text" class="form-control input-sm" id="input-val" name="menu_icon" value="<?=@$main['menu_icon']?>">
		                            </div>
		                            <div class="col-lg-2">
		                            	<button type="button" data-target="#demo-default-modal" data-toggle="modal" class="btn btn-primary btn-sm"><i class="fa fa-folder-open"></i> Pilih Icon</button>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Active Menu</label>
		                            <div class="col-lg-4">
		                                <input type="text" class="form-control input-sm" name="menu_active" value="<?=@$main['menu_active']?>">
		                            </div>
		                        </div>
		                        <?php if ($profile['user_group'] == '1'): ?>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Tampilkan Di</label>
		                            <div class="col-lg-4">
		                                <select name="menu_showing" class="form-control input-sm chosen-select">
		                                	<option value="">- Pilih -</option>
                                            <option value="1" <?php if(@$main['menu_showing'] == '1') echo 'selected'?>>Developer</option>
                                            <option value="2" <?php if(@$main['menu_showing'] == '2') echo 'selected'?>>Semua</option>
                                        </select>
		                            </div>
		                        </div>
		                    	<?php endif; ?>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Kategori Link</label>
		                            <div class="col-lg-4">
		                                <select name="menu_category" class="form-control input-sm chosen-select">
                                            <option value="I" <?php if(@$main['menu_category'] == 'I') echo 'selected'?>>Internal</option>
                                            <option value="E" <?php if(@$main['menu_category'] == 'E') echo 'selected'?>>External</option>
                                        </select>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label text-bold">Aktif</label>
		                            <div class="col-lg-4">
		                                <select name="menu_st" class="form-control input-sm chosen-select">
                                            <option value="1" <?php if(@$main['menu_st'] == '1') echo 'selected'?>>Aktif</option>
                                            <option value="0" <?php if(@$main['menu_st'] == '0') echo 'selected'?>>Tidak Aktif</option>
                                        </select>
		                            </div>
		                        </div>
		                    </fieldset>		
		                </div>
		                <div class="panel-footer">
		                    <div class="row">
		                        <div class="col-sm-7 col-sm-offset-3">
		                            <button class="btn btn-primary" type="submit"><i class="ion-paper-airplane"></i> Simpan</button>
		                            <button type="reset" class="btn btn-danger" onclick="location.href='<?=site_url("manage/index/location/master_menu_dashboard")?>'"><i class="ti-reload"></i> Batal</button>
		                        </div>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
    </div>
    <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->

<?php $this->load->view('manage/master_menu_dashboard/_modal-icon')?>

<?php $this->load->view('manage/master_menu_dashboard/_modal-manajemen-menu-label', @$main)?>

<?php $this->load->view('manage/master_menu_dashboard/_js_modal', @$main)?>