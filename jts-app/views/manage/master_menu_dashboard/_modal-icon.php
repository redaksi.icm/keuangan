<!--Default Bootstrap Modal-->
<div class="modal fade" id="demo-default-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" style="padding-top: 25px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!--Modal header-->
            <div class="modal-header" style="margin-bottom: -30px;">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title">Daftar Font Icon</h4>
            </div>

            <!--Modal body-->
            <div class="modal-body">
            	
		        <?php $this->load->view('manage/master_menu_dashboard/font-icon/font-awesome')?>

            </div>

            <script type="text/javascript">
            $(".set-icon").click(function(){
		        var text = $(this).text();
		        $('#input-val').val(text);
		        $('#demo-default-modal').modal('toggle');
		    }); 
            </script>

            <!--Modal footer-->
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!--End Default Bootstrap Modal-->