<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">
        <!--Page Title-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Data Group User</h1>
        </div>
        <!--End page title-->
        <!--Breadcrumb-->
        <ol class="breadcrumb">
		<li><a href="<?=site_url('manage/index/location/index')?>"><i class="demo-pli-home icon-bold"></i></a></li>
		<li><a href="<?=site_url('manage/index/location/master_menu_dashboard')?>">Data Group User</a></li>
		<li class="active">Index</li>
        </ol>
        <!--End breadcrumb-->
    </div>
    <!--Page content-->
    <div id="page-content">

		<div class="panel">
		    <div class="panel-heading">
		        <h3 class="panel-title">List Data Group User</h3>
		    </div>
		
		    <!--Data Table-->
		    <div class="panel-body">
		    	<form id="form-index" action="<?=site_url('manage/master_menu_dashboard/search')?>" method="POST">
		        <div class="form-inline">
		            <div class="row">
		                <div class="col-sm-6 table-toolbar-left">
                            <!-- Add Data -->
                            <?php if (@$validate_menu['add_st'] == '1'): ?>
		                    <a href="<?=site_url('manage/master_menu_dashboard/form')?>" class="btn btn-sm btn-primary add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Tambah Data"><i class="fa fa-plus-circle"></i> Tambah</a>
                            <?php endif; ?>
                            <!-- End Add Data -->
                            <!-- Delete Multiple -->
                            <?php if (@$validate_menu['delete_st'] == '1'): ?>
		                    <a class="btn btn-sm btn-danger add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Delete Multiple" id="delete-all" data-status="not_delete" data-action="<?=site_url('manage/master_menu_dashboard/delete_all')?>"><i class="fa fa-trash-o"></i> Delete</a>
                            <?php endif; ?>
                            <!-- End Delete Multiple -->
		                </div>
		                <div class="col-sm-6 table-toolbar-right">
		                    <div class="form-group">
		                        <input type="text" name="ses_txt_search" placeholder="Pencarian..." value="<?=@$ses_search['ses_txt_search']?>" class="form-control input-sm" autocomplete="off">
		                    </div>
		                    <div class="btn-group">
		                        <button type="submit" class="btn btn-sm btn-primary add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Cari"><i class="fa fa-search"></i></button>
		                        <a href="<?=site_url('manage/index/location/master_menu_dashboard')?>" class="btn btn-sm btn-danger add-tooltip" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="Clear"><i class="fa fa-refresh"></i></a>
		                    </div>
		                </div>
		            </div>
		        </div>
		    	<?=outp_notification()?>
		        <div class="table-responsive">
		            <table class="table table-striped table-bordered table-hover table-condensed">
		                <thead>
                            <tr>
                                <th class="text-center" width="3%">No</th>
                                <!-- Delete Multiple -->
                                <?php if (@$validate_menu['delete_st'] == '1'): ?>
                                <th class="text-center text-bold" width="3%">
                                    <div class="checkbox mar-no">
                                        <input id="demo-form-checkbox" class="magic-checkbox cb-all" type="checkbox">
                                        <label for="demo-form-checkbox"></label>
                                    </div>
                                </th>
                                <?php endif; ?>
                                <!-- End Delete Multiple -->
                                <!-- Aksi -->
                                <?php if (@$validate_menu['edit_st'] == '1' || @$validate_menu['delete_st'] == '1'): ?>
                                <th class="text-center" width="6%">Aksi</th>
                                <?php endif; ?>
                                <!-- End Aksi -->
                                <th class="text-center" width="28%">Judul</th>
                                <th class="text-center" width="15%">Label Menu</th>
                                <th class="text-center" width="20%">URL</th>
                                <th class="text-center" width="7%">Urut</th>
                                <th class="text-center" width="5%">Aktif</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($main as $row):?>
                            <tr>
                                <td class="text-center"><?=$row['no']?></td>
                                <!-- Delete Multiple -->
                                <?php if (@$validate_menu['delete_st'] == '1'): ?>
                                <td class="text-center">
                                    <div class="checkbox mar-no">
                                        <input id="checkbox-<?=$row['menu_id']?>" class="magic-checkbox cb-item" type="checkbox" name="cb_item[]" value="<?=$row['menu_id']?>">
                                        <label for="checkbox-<?=$row['menu_id']?>"></label>
                                    </div>
                                </td>
                                <?php endif; ?>
                                <!-- End Delete Multiple -->
                                <!-- ============================================ -->
                                <?php if (@$validate_menu['edit_st'] == '1' || @$validate_menu['delete_st'] == '1'): ?>
                                <td class="text-center">
                                <!-- Edit Data -->
                                <?php if (@$validate_menu['edit_st'] == '1'): ?>
                                    <a href="<?=site_url("manage/master_menu_dashboard/form/$p/$o/$row[menu_id]")?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit Data"><i class="fa fa-edit fa-lg text-success"></i></a>
                                <?php endif; ?>
                                <!-- End Edit Data -->
                                <!-- Delete Data -->
                                <?php if (@$validate_menu['delete_st'] == '1'): ?>
                                    <a href="<?=site_url("manage/master_menu_dashboard/delete/$p/$o/$row[menu_id]")?>" class="delete-item"><i class="fa fa-trash-o fa-lg text-danger" data-toggle="tooltip" data-placement="top" data-original-title="Delete Data"></i></a>
                                <?php endif; ?>
                                <!-- End Delete Data -->
                                </td>
                                <?php endif; ?>
                                <!-- ============================================ -->
                                <td><i class="<?=$row['menu_icon']?>"></i> <?=$row['menu_title']?></td>
                                <td class="text-center"><?=$row['label_menu_nm']?></td>
                                <td><?=$row['menu_url']?></td>                                        
                                <td class="text-center"><?=$row['menu_order']?></td>                                        
                                <td class="text-center"><?=font_icon_st($row['menu_st'])?></td>
                            </tr>
                              <?php foreach($row['menu_child'] as $child):?>
                              <tr>
                                  <td class="text-center"></td>
                                  <!-- Multiple Delete -->
                                  <?php if (@$validate_menu['delete_st'] == '1'): ?>
                                  <td class="text-center">
                                      <div class="checkbox mar-no">
                                        <input id="checkbox-<?=$child['menu_id']?>" class="magic-checkbox cb-item" type="checkbox" name="cb_item[]" value="<?=$child['menu_id']?>">
                                        <label for="checkbox-<?=$child['menu_id']?>"></label>
                                      </div>
                                  </td>
                                  <?php endif; ?>
                                  <!-- End Multiple Delete -->
                                  <!-- ============================================ -->
                                  <?php if (@$validate_menu['edit_st'] == '1' || @$validate_menu['delete_st'] == '1'): ?>
                                  <td class="text-center">
                                      <!-- Edit Data -->
                                      <?php if (@$validate_menu['edit_st'] == '1'): ?>
                                      <a href="<?=site_url("manage/master_menu_dashboard/form/$p/$o/$child[menu_id]")?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit Data"><i class="fa fa-edit fa-lg text-success"></i></a>
                                      <?php endif; ?>
                                      <!-- End Edit Data -->
                                      <!-- Delete Data -->
                                      <?php if (@$validate_menu['delete_st'] == '1'): ?>
                                      <a href="<?=site_url("manage/master_menu_dashboard/delete/$p/$o/$child[menu_id]")?>" class="delete-item"><i class="fa fa-trash-o fa-lg text-danger" data-toggle="tooltip" data-placement="top" data-original-title="Delete Data"></i></a>
                                      <!-- End Delete Data -->
                                      <?php endif; ?>
                                  </td>
                                  <?php endif; ?>
                                  <!-- ============================================ -->
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-level-up fa-rotate-90"></i> <?=$child['menu_title']?></td>    
                                  <td class="text-center"><?=$child['label_menu_nm']?></td>                                    
                                  <td><?=$child['menu_url']?></td>                                        
                                  <td class="text-center"><?=$child['menu_order']?></td>                                        
                                  <td class="text-center"><?=font_icon_st($child['menu_st'])?></td>                                        
                              </tr>
                                <?php foreach($child['menu_level_1'] as $menu_level_1):?>
                                <tr>
                                    <td class="text-center"></td>
                                    <!-- Multiple Delete -->
                                    <?php if (@$validate_menu['delete_st'] == '1'): ?>
                                    <td class="text-center">
                                        <div class="checkbox mar-no">
                                            <input id="checkbox-<?=$menu_level_1['menu_id']?>" class="magic-checkbox cb-item" type="checkbox" name="cb_item[]" value="<?=$menu_level_1['menu_id']?>">
                                            <label for="checkbox-<?=$menu_level_1['menu_id']?>"></label>
                                        </div>
                                    </td>
                                    <?php endif; ?>
                                    <!-- End Multiple Delete -->
                                    <!-- ============================================ -->
                                    <?php if (@$validate_menu['edit_st'] == '1' || @$validate_menu['delete_st'] == '1'): ?>
                                    <td class="text-center">
                                        <!-- Edit Data -->
                                        <?php if (@$validate_menu['edit_st'] == '1'): ?>
                                        <a href="<?=site_url("manage/master_menu_dashboard/form/$p/$o/$menu_level_1[menu_id]")?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit Data"><i class="fa fa-edit fa-lg text-success"></i></a>
                                        <?php endif; ?>
                                        <!-- End Edit Data -->
                                        <!-- Delete Data -->
                                        <?php if (@$validate_menu['delete_st'] == '1'): ?>
                                        <a href="<?=site_url("manage/master_menu_dashboard/delete/$p/$o/$menu_level_1[menu_id]")?>" class="delete-item"><i class="fa fa-trash-o fa-lg text-danger" data-toggle="tooltip" data-placement="top" data-original-title="Delete Data"></i></a>
                                        <?php endif; ?>
                                        <!-- End Delete Data -->
                                    </td>
                                    <?php endif; ?>
                                    <!-- ============================================ -->
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-level-up fa-rotate-90"></i> <?=$menu_level_1['menu_title']?></td>
                                    <td class="text-center"><?=$menu_level_1['label_menu_nm']?></td>
                                    <td><?=$menu_level_1['menu_url']?></td>                                        
                                    <td class="text-center"><?=$menu_level_1['menu_order']?></td>                                        
                                    <td class="text-center"><?=font_icon_st($menu_level_1['menu_st'])?></td>              
                                </tr>
                                <?php endforeach;?>
                              <?php endforeach;?>
                            <?php endforeach;?>
                        </tbody>
		            </table>
		        </div>
		        </form>
		        <!-- Pagination -->
				<?php $this->load->view('manage/template/pagination', array('url'=>'manage/master_menu_dashboard/index','main'=>$main,'paging'=>$paging))?>
				<!-- End Pagination -->
		    </div>
		    <!--End Data Table-->
		</div>
    </div>
    <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->
            
            

        

        
