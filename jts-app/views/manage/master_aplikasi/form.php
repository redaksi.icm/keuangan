<!--CONTENT CONTAINER-->
<div id="content-container">
    <div id="page-head">
        <!--Page Title-->
        <div id="page-title">
            <h1 class="page-header text-overflow">Form Validation</h1>
        </div>
        <!--End page title-->
        <!--Breadcrumb-->
        <ol class="breadcrumb">
		<li><a href="#"><i class="demo-pli-home"></i></a></li>
		<li><a href="#">Forms</a></li>
		<li class="active">Form Validation</li>
        </ol>
        <!--End breadcrumb-->
    </div>

    
    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        
		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel">
		            <div class="panel-heading">
		                <h3 class="panel-title">Form Validation</h3>
		            </div>
		
		            <form action="http://www.themeon.net/nifty/v2.9.1/forms-validation.html" class="form-horizontal">
		                <div class="panel-body">
		                    <p class="bord-btm pad-ver text-main text-bold">Pengaturan Logo</p>
		                    <fieldset>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Gambar Logo</label>
		                            <div class="col-lg-4">
			                            <input type="file" name="logo_img" class="form-control input-sm">
			                            <?php if(@$main['logo_img'] != ''):?>
			                            <img id="view-image" src="<?=base_url()?>assets/images/logo/<?=@$main['logo_img']?>" style="width: 150px; margin-top: 8px;" class="img-thumbnail"><br>
			                            <?php endif;?>
			                        </div>
		                        </div>
		                    </fieldset>

		                    <br>
		                    <p class="bord-btm pad-ver text-main text-bold">Pengaturan Aplikasi</p>
		                    <fieldset>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Judul Aplikasi (Lengkap)</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Judul Aplikasi (Pendek)</label>
		                            <div class="col-lg-3">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Alamat</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Telpon</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Fax</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Email</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Nama Website</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Nama Dinas</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Copyright</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                    </fieldset>

		                    <br>
		                    <p class="bord-btm pad-ver text-main text-bold">Pengaturan SEO</p>
		                    <fieldset>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Meta Description</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Meta Keywoards</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-lg-2 control-label">Meta Content</label>
		                            <div class="col-lg-5">
		                                <input type="text" class="form-control" name="password">
		                            </div>
		                        </div>
		                    </fieldset>
		
		                </div>
		                <div class="panel-footer">
		                    <div class="row">
		                        <div class="col-sm-7 col-sm-offset-3">
		                            <button class="btn btn-mint" type="submit">Submit</button>
		                        </div>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		
		
		
    </div>
    <!--===================================================-->
    <!--End page content-->

</div>
<!--END CONTENT CONTAINER-->
            
            

        

        
