                            <?php if(count($main) > 0):?>
                            <?php 
                            $paging_url_end = '';
                            if(@$paging->url_end != '') $paging_url_end = '/'.$paging->url_end;
                            ?>
                            <div style="margin-top: -27px;">
                                <ul class="pagination pagination-xs m-top-none pull-left" style="margin-top: 23px;">
                                    <li><i class="fa fa-bookmark fa-sm"></i>  Terdapat <?=($paging->num_rows)?> data</li>
                                </ul>
                                <ul class="pagination pagination-xs m-top-none pull-right">
                                    <?php if($paging->start_link): ?>
                                        <!-- <li><a href="<?=site_url("$url/$paging->c_start_link/$o". $paging_url_end) ?>">First</a></li> -->
                                        <li><a class="btn btn-sm" href="<?=site_url("$url/$paging->start_link/$o". $paging_url_end) ?>">First</a></li>
                                    <?php endif; ?>
                                    <?php if($paging->prev): ?>
                                        <li><a class="btn btn-sm" href="<?=site_url("$url/$paging->prev/$o". $paging_url_end) ?>">Prev</a></li>
                                    <?php endif; ?>

                                    <?php for($i = $paging->c_start_link; $i <= $paging->c_end_link; $i++): ?>
                                        <li <?php jecho($p, $i, "class='active'") ?>><a class="btn btn-sm" href="<?=site_url("$url/$i/$o". $paging_url_end) ?>"><?=$i ?></a></li>
                                    <?php endfor; ?>

                                    <?php if($paging->next): ?>
                                        <li><a class="btn btn-sm" href="<?=site_url("$url/$paging->next/$o". $paging_url_end) ?>">Next</a></li>
                                    <?php endif; ?>
                                    <?php if($paging->end_link): ?>
                                        <li><a class="btn btn-sm" href="<?=site_url("$url/$paging->end_link/$o". $paging_url_end) ?>">Last</a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <?php else:?>
                                <?php if (@$news != 'news'): ?>
                                    <div class="panel-footer clearfix panel-colorful panel-default" style="margin-top: -20px;">
                                        <ul class="pagination pagination-xs pull-left" style="margin-bottom: 0px; margin-top: 0px;">
                                            <li>Data tidak ditemukan.</li>
                                        </ul>                                
                                    </div>
                                <?php endif; ?>
                            <?php endif;?>