    <!-- Sidebar -->
    <?php $this->load->view('manage/template/sidebar')?>
    <!-- End Sidebar -->

        <footer id="footer">
            <div class="show-fixed pad-rgt pull-right">
                Page rendered in <a href="#" class="text-main"><span class="badge badge-danger"><strong>{elapsed_time}</strong></span> second.</a>
            </div>
            <div class="hide-fixed pull-right pad-rgt">
                14GB of <strong>512GB</strong> Free.
            </div>
            <p class="pad-lft">&#0169; 2018 Your Company</p>
        </footer>
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
    </div>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/validator.js"></script>
    <script src="<?=base_url()?>assets/js/sweetalert.min.js"></script>
    <script src="<?=base_url()?>assets/js/autonumeric.js"></script>
    <script src="<?=base_url()?>assets/js/jts.js"></script>
    <script src="<?=base_url()?>assets/js/nifty.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/chosen/chosen.jquery.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?=base_url()?>assets/js/demo/form-component.js"></script>
</body>
</html>