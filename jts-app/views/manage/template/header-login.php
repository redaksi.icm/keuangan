<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Login Aplikasi SIMBOK - <?=$config['app_title_header2']?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url()?>assets/images/logo/<?=$config['logo_img_mini']?>" type="image/x-icon">
    <!-- CSS -->
    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="<?=base_url()?>assets/login/css/style-login.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url()?>assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Perfect -->
    <link href="<?=base_url()?>assets/login/css/app.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/login/css/app-skin.css" rel="stylesheet">
    <!-- JAVASCRIPT -->
    <!-- Jquery -->
    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>    
  </head>