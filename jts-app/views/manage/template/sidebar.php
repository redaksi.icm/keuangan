<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
    <div id="mainnav">
        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <!--Profile Widget-->
                    <!--================================-->
                    <div id="mainnav-profile" class="mainnav-profile">
                        <div class="profile-wrap text-center">
                            <div class="pad-btm">
                            <?php if (@$profile['user_photo'] !=''): ?>
                                <img class="img-circle img-md" src="<?=base_url()?>assets/images/user/<?=@$profile['user_photo']?>">
                            <?php else: ?>
                                <img class="img-circle img-md" src="<?=base_url()?>assets/images/user/no-image.png">
                            <?php endif; ?>
                            </div>
                            <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                <span class="pull-right dropdown-toggle">
                                    <i class="dropdown-caret"></i>
                                </span>
                                <p class="mnp-name"><?=$profile['user_realname']?></p>
                                <span class="mnp-desc"><?=$profile['user_group_name']?></span>
                            </a>
                        </div>
                        <div id="profile-nav" class="collapse list-group bg-trans">
                            <a href="#" class="list-group-item">
                                <i class="demo-pli-male icon-lg icon-fw"></i> View Profile
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="demo-pli-gear icon-lg icon-fw"></i> Settings
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="demo-pli-information icon-lg icon-fw"></i> Help
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
                            </a>
                        </div>
                    </div>
                    <!--Shortcut buttons-->
                    <!--================================-->
                    <div id="mainnav-shortcut" class="hidden">
                        <ul class="list-unstyled shortcut-wrap">
                            <li class="col-xs-3" data-content="My Profile">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                    <i class="demo-pli-male"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Messages">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                    <i class="demo-pli-speech-bubble-3"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Activity">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                    <i class="demo-pli-thunder"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Lock Screen">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                    <i class="demo-pli-lock-2"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--================================-->
                    <!--End shortcut buttons-->
                    <ul id="mainnav-menu" class="list-group">
                        <!--Category name-->
                        <li class="list-header">Navigation</li>
                        <!--Menu list item-->
                        <?php foreach($menu_dashboard as $menu_label):?>
                            <?php if ($menu_label['label_menu_id'] ==''): ?>
                                <?php if($menu_label['menu_category'] == 'I'):?>
                                    <?php if ($menu_label['menu_url'] == '#'): ?>
                                        <li class="<?php if(@$this->active_menu == $menu_label['menu_active']) echo 'active-link'?>">
                                            <a href="<?=$menu_label['menu_url']?>">
                                                <i class="<?=$menu_label['menu_icon']?>"></i>
                                                <span class="menu-title"><?=$menu_label['menu_title']?></span>
                                            </a>
                                        </li>
                                    <?php else: ?>
                                        <li class="<?php if(@$this->active_menu == $menu_label['menu_active']) echo 'active-link'?>">
                                            <a href="<?=site_url($menu_label['menu_url'])?>">
                                                <i class="<?=$menu_label['menu_icon']?>"></i>
                                                <span class="menu-title"><?=$menu_label['menu_title']?></span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php if ($menu_label['menu_url'] == '#'): ?>
                                        <li class="<?php if(@$this->active_menu == $menu_label['menu_active']) echo 'active-link'?>">
                                            <a href="<?=$menu_label['menu_url']?>">
                                                <i class="<?=$menu_label['menu_icon']?>"></i>
                                                <span class="menu-title"><?=$menu_label['menu_title']?></span>
                                            </a>
                                        </li>
                                    <?php else: ?>
                                        <li class="<?php if(@$this->active_menu == $menu_label['menu_active']) echo 'active-link'?>">
                                            <a href="<?=url_target_blank($menu_label['menu_url'])?>" target="_blank">
                                                <i class="<?=$menu_label['menu_icon']?>"></i>
                                                <span class="menu-title"><?=$menu_label['menu_title']?></span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <!-- Label -->
                                <li class="list-header"><?=$menu_label['label_menu_nm']?></li>
                                <!-- End Label -->
                            <?php endif; ?>
                            <?php foreach ($menu_label['menu_parent'] as $menu_parent): ?>
                                <?php if ($menu_label['label_menu_id'] !=''): ?>
                                    <?php if (count(@$menu_parent['menu_child']) == 0): ?>
                                        <?php if($menu_child['menu_category'] == 'I'):?>
                                            <?php if ($menu_child['menu_url'] == '#'): ?>
                                                <li class="<?php if(@$this->active_menu == $menu_parent['menu_active']) echo 'active-sub'?>">
                                                    <a href="<?=$menu_parent['menu_url']?>">
                                                        <i class="<?=$menu_parent['menu_icon']?>"></i>
                                                        <span class="menu-title"><?=$menu_parent['menu_title']?></span>
                                                    </a>
                                                </li>
                                            <?php else: ?>
                                                <li class="<?php if(@$this->active_menu == $menu_parent['menu_active']) echo 'active-sub'?>">
                                                    <a href="<?=site_url($menu_parent['menu_url'])?>">
                                                        <i class="<?=$menu_parent['menu_icon']?>"></i>
                                                        <span class="menu-title"><?=$menu_parent['menu_title']?></span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?php if ($menu_child['menu_url'] == '#'): ?>
                                                <li class="<?php if(@$this->active_menu == $menu_parent['menu_active']) echo 'active-sub'?>">
                                                    <a href="<?=$menu_parent['menu_url']?>" target="_blank">
                                                        <i class="<?=$menu_parent['menu_icon']?>"></i>
                                                        <span class="menu-title"><?=$menu_parent['menu_title']?></span>
                                                    </a>
                                                </li>
                                            <?php else: ?>
                                                <li class="<?php if(@$this->active_menu == $menu_parent['menu_active']) echo 'active-sub'?>">
                                                    <a href="<?=site_url($menu_parent['menu_url'])?>" target="_blank">
                                                        <i class="<?=$menu_parent['menu_icon']?>"></i>
                                                        <span class="menu-title"><?=$menu_parent['menu_title']?></span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <li class="<?php if(@$this->active_menu == $menu_parent['menu_active']) echo 'active-sub'?>">
                                            <a href="#">
                                                <i class="<?=$menu_parent['menu_icon']?>"></i>
                                                <span class="menu-title"><?=$menu_parent['menu_title']?></span>
                                                <i class="arrow"></i>
                                            </a>
                                            <!--Submenu-->
                                            <ul class="collapse <?php if(@$this->active_menu == $menu_parent['menu_active']) echo 'in'?>">
                                                <?php foreach ($menu_parent['menu_child'] as $menu_child): ?>
                                                    <?php if($menu_child['menu_category'] == 'I'):?>
                                                        <?php if ($menu_child['menu_url'] == '#'): ?>
                                                            <li class="<?php if(@$this->menu_url == $menu_child['set_menu_url']) echo 'active-link'?>"><a href="<?=$menu_child['menu_url']?>"><?=$menu_child['menu_title']?></a></li>
                                                        <?php else: ?>
                                                            <li class="<?php if(@$this->menu_url == $menu_child['set_menu_url']) echo 'active-link'?>"><a href="<?=site_url($menu_child['menu_url'])?>"><?=$menu_child['menu_title']?></a></li>
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        <?php if ($menu_child['menu_url'] == '#'): ?>
                                                            <li class="<?php if(@$this->menu_url == $menu_child['set_menu_url']) echo 'active-link'?>"><a href="<?=$menu_child['menu_url']?>" target="_blank"><?=$menu_child['menu_title']?></a></li>
                                                        <?php else: ?>
                                                            <li class="<?php if(@$this->menu_url == $menu_child['set_menu_url']) echo 'active-link'?>"><a href="<?=site_url($menu_child['menu_url'])?>" target="_blank"><?=$menu_child['menu_title']?></a></li>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                        <!-- End Menu list item-->
                        <li class="list-divider"></li>                             
                    </ul>
                    <div class="margin-sidebar"></div>
                </div>
            </div>
        </div>
    </div>
</nav>
</div>