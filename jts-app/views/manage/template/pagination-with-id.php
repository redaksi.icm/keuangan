                            <?php if(count($main) > 0):?>
                            <?php 
                            $paging_url_end = '';
                            if(@$paging->url_end != '') $paging_url_end = '/'.$paging->url_end;
                            // id
                            if (@$id !='') {
                                $view_id = @$id;
                            }else{
                                $view_id = "";
                            }
                            ?>
                            <hr>
                            <div style="margin-top: -27px;">
                                <ul class="pagination pagination-xs m-top-none pull-left">
                                    <li><i class="fa fa-bookmark fa-sm"></i>  Terdapat <?=($paging->num_rows)?> data</li>
                                </ul>
                                <ul class="pagination pagination-xs m-top-none pull-right">
                                    <?php if($paging->start_link): ?>
                                        <!-- <li><a href="<?=site_url("$url/$view_p/$view_o/$view_id/$paging->c_start_link/$o". $paging_url_end) ?>">First</a></li> -->
                                        <li><a class="btn btn-sm" href="<?=site_url("$url/$view_p/$view_o/$view_id/$paging->start_link/$o". $paging_url_end) ?>">First</a></li>
                                    <?php endif; ?>
                                    <?php if($paging->prev): ?>
                                        <li><a class="btn btn-sm" href="<?=site_url("$url/$view_p/$view_o/$view_id/$paging->prev/$o". $paging_url_end) ?>">Prev</a></li>
                                    <?php endif; ?>

                                    <?php for($i = $paging->c_start_link; $i <= $paging->c_end_link; $i++): ?>
                                        <li <?php jecho($p, $i, "class='active'") ?>><a class="btn btn-sm" href="<?=site_url("$url/$view_p/$view_o/$view_id/$i/$o". $paging_url_end) ?>"><?=$i ?></a></li>
                                    <?php endfor; ?>

                                    <?php if($paging->next): ?>
                                        <li><a class="btn btn-sm" href="<?=site_url("$url/$view_p/$view_o/$view_id/$paging->next/$o". $paging_url_end) ?>">Next</a></li>
                                    <?php endif; ?>
                                    <?php if($paging->end_link): ?>
                                        <li><a class="btn btn-sm" href="<?=site_url("$url/$view_p/$view_o/$view_id/$paging->end_link/$o". $paging_url_end) ?>">Last</a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <?php else:?>
                                <?php if (@$news != 'news'): ?>
                                    <div class="alert alert-info">
                                        <strong>Kosong !</strong> Data tidak ditemukan
                                    </div>
                                <?php endif; ?>
                            <?php endif;?>