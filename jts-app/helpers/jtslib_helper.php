<?php 
date_default_timezone_set('Asia/Jakarta');
//
function app($id=null) {
    $data['per_page'] = '10';
    $data['password_reset'] = 'admin';
    return $data[$id];
}

function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}

function themes() {
    $data = array('default','red','green');
    return $data;
}

function create_password($str) {
    if($str == "") $str = date('Y-m-d H:i:s');
    $result = md5(md5(md5($str)));
    return $result;
}

function get_polling_bar($n, $result) {
    $colors = array('1'=>'red','green','blue','yellow','brown');
    if($result == '') $result = 0;
    $html = '<div style="display:block; width:90%; border-radius:5px; moz-border-radius:5px; background-color:'.$colors[$n].'">&nbsp;</div>';
    echo $html;
}

function convert_base64($path=null) {
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
    return $base64;
}

function clean_url($url=null) {
    $url = strtolower($url);
    $url = anti_injection_image($url);
    return $url;
}

function anti_injection_image($str=null) {
    $str = str_replace('"', " ", $str);
    $str = str_replace("'", ' ', $str);
    $str = str_replace("`", ' ', $str);
    $str = str_replace("?", '', $str);
    $str = str_replace(" ", '-', $str);
    $str = str_replace("/", '-', $str);
    $str = str_replace(",", '-', $str);
    $str = str_replace("&", '-', $str);
    $str = str_replace('“', '', $str);
    $str = str_replace('”', '', $str);
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
}

function anti_injection($str=null, $act=null) {
    $str = str_replace('"', " ", $str);
    $str = str_replace("'", ' ', $str);
    $str = str_replace("`", ' ', $str);
    $str = str_replace("?", '', $str);
    // $str = str_replace(" ", '-', $str);
    $str = str_replace("/", '-', $str);
    $str = str_replace(",", '-', $str);
    $str = str_replace("&", '-', $str);
    $str = str_replace('“', '', $str);
    $str = str_replace('”', '', $str);
    $str = str_replace('(', '', $str);
    $str = str_replace(')', '', $str);
    $str = str_replace('{', '', $str);
    $str = str_replace('}', '', $str);
    $str = str_replace('.', '', $str);
    $str = str_replace(':', '', $str);
    $str = str_replace(';', '', $str);
    // tambahan
    if ($act == 'all') {
        $str = str_replace('<', '', $str);
        $str = str_replace('>', '', $str);
        $str = str_replace('[', '', $str);
        $str = str_replace(']', '', $str);
        $str = str_replace('%', '', $str);
        $str = str_replace('$', '', $str);
        $str = str_replace('!', '', $str);
        $str = str_replace('`', '', $str);
        $str = str_replace('~', '', $str);
        $str = str_replace('=', '', $str);
        $str = str_replace('script', '', $str);
    }
    //
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
}

function replace_prosentase($str=null) {
    $str = str_replace('-', '', $str);
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
}

function remove_digit($str=null) {
    $str = str_replace('.', '', $str);
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
}

function replace_img_komoditas($str=null) {
    $str = str_replace('_', '.', $str);
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
}

function replace_space_to_underscore($str=null) {
    $str = str_replace(" ", '_', $str);
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
}

function replace_pasar_nm($str=null) {
    $str = strtolower($str);
    $str = str_replace(' ', '_', $str);
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
}

function replace_anti_br($str=null) {
    $str = str_replace("<br />", "\n", $str);
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
}

function clear_injection($str=null) {
    $str = str_replace("+", " ", $str);
    return $str;
}

function url_target_blank($url) {
    $url = str_replace('http://', '', $url);
    $url = 'http://' . $url;
    $url = trim($url);
    return $url;
}

function active_st_img($st=null) {
    if($st == '1') {
        echo "<img src='".base_url()."assets/images/icon/icon-checked.png'>";
    } else if($st == '0') {
        echo "<img src='".base_url()."assets/images/icon/icon-banned.png'>";
    }
}

function list_st_usulan() {
    $data = array(
        'belum' => 'Berkas Diterima',
        'sedang' => 'Dalam Proses',
        'selesai' => 'Selesai Diproses',
    );
    return $data;
}

function key_st_usulan($id) {
    if($id == 'belum') $result = '0';
    elseif($id == 'sedang') $result = '1';
    elseif($id == 'selesai') $result = '2';
    return $result;
}

function get_st_usulan($st) {
    switch ($st) {
        case '0':
            $result = 'Belum';
            break;
        case '1':
            $result = 'Proses';
            break;
        case '2':
            $result = 'Selesai';
            break;        
        default:
            $result = '';
            break;
    }
    return $result;
}

function set_st_usulan($st=null) {
    switch ($st) {
        case '0': // proses
            $html = '<span class="label label-info" style="font-weight:normal">Berkas Diterima</span>';
            break;        
        case '1': // disposisi
            $html = '<span class="label label-success" style="font-weight:normal">Dalam Proses</span>';
            break;        
        case '2': // selesai
            $html = '<span class="label label-danger" style="font-weight:normal">Selesai Diproses</span>';
            break;        
        default:
            $html = '';
            break;
    }
    echo $html;
}

function slice_text($text=null, $num=null) {
    if($num == '') $num = '800';
    $result = substr($text, 0, $num);
    if(strlen($text) > $num) $result = $result . '...';
    return $result;
}

function min_text($txt=null) {
    $txt = substr($txt, 0, 25);
    return $txt;
}

function zerofill($id=null,$num=null) {
    $len = strlen($id);
    $r = '';
    if($num == 2){
        if($len == '1') $r = '0'.$id; 
        elseif($len == '2') $r = $id;
        else $r = $id;
    }elseif($num == 4){
        if($len == '1') $r = '000'.$id; 
        elseif($len == '2') $r = '00'.$id;
        elseif($len == '3') $r = '0'.$id;    
        elseif($len == '4') $r = $id;    
        else $r = $id;
    }elseif($num == 5){
        if($len == '1') $r = '0000'.$id; 
        elseif($len == '2') $r = '000'.$id;
        elseif($len == '3') $r = '00'.$id;    
        elseif($len == '4') $r = '0'.$id;    
        elseif($len == '5') $r = $id;    
        else $r = $id;
    }elseif($num == 6){
        if($len == '1') $r = '00000'.$id; 
        elseif($len == '2') $r = '0000'.$id;
        elseif($len == '3') $r = '000'.$id;    
        elseif($len == '4') $r = '00'.$id;    
        elseif($len == '5') $r = '0'.$id;    
        elseif($len == '6') $r = $id;    
        else $r = $id;
    }elseif($num == 7){
        if($len == '1') $r = '000000'.$id; 
        elseif($len == '2') $r = '00000'.$id;
        elseif($len == '3') $r = '0000'.$id;    
        elseif($len == '4') $r = '000'.$id;    
        elseif($len == '5') $r = '00'.$id;    
        elseif($len == '6') $r = '0'.$id;    
        elseif($len == '7') $r = $id;    
        else $r = $id;
    }elseif($num == 8){
        if($len == '1') $r = '0000000'.$id; 
        elseif($len == '2') $r = '000000'.$id;
        elseif($len == '3') $r = '00000'.$id;    
        elseif($len == '4') $r = '0000'.$id;    
        elseif($len == '5') $r = '000'.$id;    
        elseif($len == '6') $r = '00'.$id;    
        elseif($len == '6') $r = '0'.$id;    
        elseif($len == '7') $r = $id;    
        else $r = $id;
    }else{
        if($len == '1') $r = '00'.$id; 
        elseif($len == '2') $r = '0'.$id;
        elseif($len == '3') $r = $id;    
        else $r = $id;
    }    
    return $r;
}

function isset_session($sess_name=null, $default_value=null) {
    if(isset($_SESSION[$sess_name])) {
        return $_SESSION[$sess_name];
    } else {
        if($default_value !='') {
            return $default_value;
        } else {
            return false;            
        }
    }
}

function unset_session($sess_name=null) {
    $arr_sess = explode(',', $sess_name);
    foreach($arr_sess as $ses) {
        unset($_SESSION[$ses]);        
    }
}

function outp_result($outp=null,$tp=null) {
    if ($outp) {
        if($tp == 'delete') {
            return $_SESSION['success'] = 2;
        }elseif($tp == 'activated') {
            return $_SESSION['success'] = 3;
        }elseif($tp == 'non_activated') {
            return $_SESSION['success'] = 4;
        }elseif($tp == 'success_register') {
            return $_SESSION['success'] = 5;
        }elseif($tp == 'failed_register') {
            return $_SESSION['failed'] = 1;
        }elseif($tp == 'pesan') {
            return $_SESSION['success'] = 6;
        }elseif($tp == 'image') {
            return $_SESSION['success'] = 7;
        }elseif($tp == 'token') {
            return $_SESSION['success'] = 8;
        } else {
            return $_SESSION['success'] = 1;
        }        
    } else {
        if($tp == 'delete') {
            return $_SESSION['success'] = -2;
        }elseif($tp == 'activated') {
            return $_SESSION['success'] = -3;
        }elseif($tp == 'non_activated') {
            return $_SESSION['success'] = -4;
        }elseif($tp == 'success_register') {
            return $_SESSION['success'] = -5;
        }elseif($tp == 'failed_register') {
            return $_SESSION['failed'] = -1;
        }elseif($tp == 'pesan') {
            return $_SESSION['failed'] = -6;
        }elseif($tp == 'image') {
            return $_SESSION['failed'] = -7;
        }elseif($tp == 'token') {
            return $_SESSION['failed'] = -8;
        } else {
            return $_SESSION['success'] = -1;
        }        
    }
}

function outp_notification($type=null) {
    $outp = @$_SESSION['success'];
    if($outp != false) {
        // reguler
        if($outp == 1) {
            $msg = 'Data berhasil disimpan.';
            $css = 'alert alert-success';
        } elseif($outp == -1) {
            $msg = 'Data gagal disimpan.';
            $css = 'alert alert-success';
        }
        // delete
        elseif($outp == 2) {
            $msg = 'Data berhasil dihapus.';
            $css = 'alert alert-danger';
        } elseif($outp == -2) {
            $msg = 'Data gagal dihapus.';
            $css = 'alert alert-danger';
        }
        // activated
        elseif($outp == 3) {
            $msg = 'Pengguna berhasil di aktifkan.';
            $css = 'alert alert-success';
        } elseif($outp == -3) {
            $msg = 'Pengguna gagal di aktifkan.';
            $css = 'alert alert-success';
        }
        // non-activated
        elseif($outp == 4) {
            $msg = 'Pengguna berhasil di nonaktifkan.';
            $css = 'alert alert-success';
        } elseif($outp == -4) {
            $msg = 'Pengguna gagal di nonaktifkan.';
            $css = 'alert alert-success';
        }
        // success register
        elseif($outp == 5) {
            $msg = 'Anda berhasil melakukan registrasi, silahkan cek email Anda.';
            $css = 'alert alert-success';
        } elseif($outp == -5) {
            $msg = 'Anda gagal melakukan registrasi.';
            $css = 'alert alert-success';
        }
        // pesan
        elseif($outp == 6) {
            $msg = 'Pesan Anda berhasil dikirim, Terimakasih.';
            $css = 'alert alert-success';
        } elseif($outp == -6) {
            $msg = 'Pesan Anda gagal dikirim, silahkan ulangi lagi.';
            $css = 'alert alert-danger';
        }
        // image
        elseif($outp == 7) {
            $msg = 'Format Gambar Anda Salah !.';
            $css = 'alert alert-danger';
        } elseif($outp == -7) {
            $msg = 'Format Gambar Anda Salah !.';
            $css = 'alert alert-danger';
        }
        // token
        elseif($outp == 8) {
            $msg = 'Kirim Pesan Gagal !, Silahkan Coba Lagi.';
            $css = 'alert alert-danger';
        } elseif($outp == -8) {
            $msg = 'Kirim Pesan Gagal !, Silahkan Coba Lagi.';
            $css = 'alert alert-danger';
        }
        //
        if($type == 'top') {
            $html = '<div class="mainnav-backdrop"></div>
                    <div id="floating-top-container" class="floating-container msg-notification">
                        <div id="floating-top-right">
                            <div class="alert-wrap animated bounce in">
                                <div class="alert '.$css.'" role="alert">
                                    <div class="media">
                                        <div class="media-left">
                                            <span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="ti-info-alt icon-2x"></i></span>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="alert-title">Informasi !</h4>
                                            <p class="alert-message" style="margin-right: 150px;"><span class="text-semibold">Message!</span> '.$msg.'</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                    $(function() {
                        setTimeout(function(){ 
                            $(".msg-notification").bind("click",function() {
                                $(this).fadeOut();
                            }).css("cursor","pointer").fadeOut(2000);
                        }, 8000);
                    });
                    </script>';
        } else {
            $html = '<div class="alert '.$css.'" id="msg-notification">
                        <strong>Message!</strong> '.$msg.'
                    </div>
                    <script>
                    $(function() {
                        setTimeout(function(){ 
                            $("#msg-notification").bind("click",function() {
                                $(this).fadeOut();
                            }).css("cursor","pointer").fadeOut(2000);
                        }, 3000);
                    });
                    </script>
                    ';
        }
        
    } else {
        $html = false;
    }    
    //
    $_SESSION['success']=0;
    //
    return $html;
}

function msg_failed() {
    $outp = @$_SESSION['failed'];
    if($outp != false) {
        // reguler
        if($outp == 1) {
            $msg = 'Anda gagal melakukan registrasi.';
            $css = 'alert alert-danger';
        }elseif($outp == -1) {
            $msg = 'Anda gagal melakukan registrasi.';
            $css = 'alert alert-danger';
        }
        //
        $html = '<div class="alert '.$css.' msg-notification">
                    <strong>Message!</strong> '.$msg.'
                </div>
                <script>
                $(function() {
                    window.setTimeout(function () {
                        $(".msg-notification").bind("click",function() {
                            $(this).fadeOut();
                        }).css("cursor","pointer").fadeOut(5000);
                    }, 6000);
                });
                $(document).ready(function(){
                  window.setTimeout(function () {
                    $("#myModalRegister").modal();
                  }, 100);
                });
                </script>
                ';
        
    } else {
        $html = false;
    }    
    //
    $_SESSION['failed']=0;
    //
    return $html;
}

function msg_captcha() {
    $outp = @$_SESSION['captcha'];
    if($outp != false) {
        // reguler
        if($outp == 1) {
            $msg = 'Kode Captcha Salah.';
            $css = 'alert alert-danger';
        }
        //
        if ($outp == 1) {
            $html = '<div class="alert '.$css.'" id="msg-notification">
                        <strong>Message!</strong> '.$msg.'
                    </div>
                    <script>
                    $(function() {
                        $("#msg-notification").bind("click",function() {
                            $(this).fadeOut();
                        }).css("cursor","pointer").fadeOut(6000);
                    });
                    </script>
                    ';
        }
        
    } else {
        $html = false;
    }    
    //
    $_SESSION['captcha']=0;
    //
    return $html;
}

function msg_captcha_two() {
    $outp = @$_SESSION['captcha_two'];
    if($outp != false) {
        // reguler
        if($outp == 2) {
            $msg = 'Kode Captcha Salah.';
            $css = 'alert alert-danger';
        }
        //
        if ($outp == 2) {
            $html = '<div class="alert '.$css.'" id="msg-notification">
                        <strong>Message!</strong> '.$msg.'
                    </div>
                    <script>
                    $(function() {
                        window.setTimeout(function () {
                            $("#msg-notification").bind("click",function() {
                                $(this).fadeOut();
                            }).css("cursor","pointer").fadeOut(5000);
                        }, 6000);
                    });
                    $(document).ready(function(){
                      window.setTimeout(function () {
                        $("#myModalRegister").modal();
                      }, 100);
                    });
                    </script>
                    ';
        }
        
    } else {
        $html = false;
    }    
    //
    $_SESSION['captcha_two']=0;
    //
    return $html;
}

function msg_type_img() {
    $outp = @$_SESSION['type_img'];
    if($outp != false) {
        // reguler
        if($outp == 1) {
            $msg = 'Tipe Gambar Salah.';
            $css = 'alert alert-danger';
        }elseif($outp == 2) {
            $msg = 'Tipe Gambar Salah.';
            $css = 'alert alert-danger';
        }
        //
        if ($outp == 1) {
            $html = '<div class="alert '.$css.'" id="msg-notification">
                        <strong>Message!</strong> '.$msg.'
                    </div>
                    <script>
                    $(function() {
                        $("#msg-notification").bind("click",function() {
                            $(this).fadeOut();
                        }).css("cursor","pointer").fadeOut(6000);
                    });
                    </script>
                    ';
        }elseif ($outp == 2) {
            $html = '<div class="alert '.$css.' msg-notification">
                        <strong>Message!</strong> '.$msg.'
                    </div>
                    <script>
                    $(function() {
                        window.setTimeout(function () {
                            $(".msg-notification").bind("click",function() {
                                $(this).fadeOut();
                            }).css("cursor","pointer").fadeOut(5000);
                        }, 6000);
                    });
                    $(document).ready(function(){
                      window.setTimeout(function () {
                        $("#myModalRegister").modal();
                      }, 100);
                    });
                    </script>
                    ';
        }
        
    } else {
        $html = false;
    }    
    //
    $_SESSION['type_img']=0;
    //
    return $html;
}

function a_sort($data=null) {
    $idxa = @$data['idxa'];
    $idxb = @$data['idxb'];
    //
    $get_url = @$data['url'];
    $arr_url = explode('?', $get_url);    
    $url = @$arr_url[0];    
    //
    $title = @$data['title'];
    $order = (@$data['order'] == $idxa ? $idxb : $idxa);   
    //
    if(@$data['order'] == '' || (@$data['order'] != $idxa && @$data['order'] != $idxb)) {
        $fa = 'fa-sort';
    } else {
        $fa = ($order == $idxa) ? 'fa-sort-down' : 'fa-sort-up';
    }    
    //
    $set_url = $url.'/'.$order;
    //
    if(@$arr_url[1] != '') { // if exist end link
        $arr_url_end = explode('=', @$arr_url[1]);        
        $set_url .= '/'.@$arr_url_end[1];
    }
    //
    $html = '<a href="'.$set_url.'" title="Sort">'.$title.' <i class="fa '.$fa.'"></i></a>';
    return $html;
}

function get_posting_st($id=null) {
    if($id == '1') $st = 'Ya';
    else $st = '';
    return $st;
}

function get_post_st($id=null) {
    $arr = list_post_st();
    foreach($arr as $key => $val) {
        if($id == $key) {
            return $val;
        }
    }    
}

function digit($inp = '') {
    if($inp != '') {
        $inp = abs($inp);
        return number_format($inp, 0, ',', '.');
    }else {
        return '0';
    }
}

function rupiah($angka){
    if ($angka !='') {
        $hasil_rupiah = "Rp. " . number_format($angka,2,',','.');
        return $hasil_rupiah;
    }
}

function awal_besar($huruf) {
    if ($huruf !='') {
        $result_first_big = ucwords(strtolower($huruf));
        return $result_first_big;
    }
}

function centang($data) {
    if ($data !='') {
        if ($data == '1') {
            return "x";
        }else{
            return " ";
        }
    }
}

function list_post_st($ses_usergroup=null) {    
    if($ses_usergroup == '1') { // admin
        $arr = array(
            '1' => 'Publish',
            '2' => 'Draft',
            '3' => 'Not-Publish'
        );
    } elseif($ses_usergroup == '2') { // publisher
        $arr = array(
            '1' => 'Publish',
            '2' => 'Draft',
            '3' => 'Not-Publish'
        );
    } elseif($ses_usergroup == '3') { // creator
        $arr = array(
            '2' => 'Draft',
        );
    } else {
        $arr = array(
            '1' => 'Publish',
            '2' => 'Draft',
            '3' => 'Not-Publish'
        );
    }    
    return @$arr;
}

function rupiah_no_rp($angka){
    if ($angka !='') {
        $hasil_rupiah = number_format($angka,2,',','.');
        return $hasil_rupiah;
    }
}

function clear_zero($inp = '') {
    return ($inp == '0' ? '' : $inp);
}

function clear_numeric($id=null) {
    $result = str_replace('.', '', $id);
    return $result;
}

function clear_prefix_no($id) {
    $arr = explode('. ', $id);
    return (@$arr[1] != '' ? @$arr[1] : @$arr[0]);
}

function replace_no_to_symbol($data=null, $to=null) {
    $data = str_replace('1.', $to, $data);
    $data = str_replace('2.', $to, $data);
    $data = str_replace('3.', $to, $data);
    $data = str_replace('4.', $to, $data);
    $data = str_replace('5.', $to, $data);
    return $data;
}

function replace_to_nbs($data=null, $to=null) {
    $data = str_replace('_', '&nbsp;', $data);
    return $data;
}

function list_bulan() {
    $data = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember',
    );
    return $data;
}

function convert_date($date=null,$sp=null,$tp=null) {
    if($date != '' && $date != '0000-00-00') {
        if($tp == 'date') {
            $arr_date = explode(' ', $date);
            $date = $arr_date[0];
        } elseif($tp == 'full_date') {
            $arr_date = explode(' ', $date);
            $date = $arr_date[0];
            $time = $arr_date[1];
        }
        $arr = explode('-', $date);
        if($sp != '') {
            $result = $arr[2].$sp.$arr[1].$sp.$arr[0];
        } else {
            $result = $arr[2].'-'.$arr[1].'-'.$arr[0];
        }
        if($tp == 'full_date') {
            $result .= ' '.$time;
        }
    } else {
        $result = '';
    }    
    return $result;
}

function convert_date_indo($tgl='') {
    if($tgl != '') {
        $tanggal = substr($tgl, 8, 2);
        $jam = substr($tgl, 11, 8);
        $bulan = bulan(substr($tgl, 5, 2));
        $tahun = substr($tgl, 0, 4);
        if($jam != '') {
            return $tanggal . ' ' . $bulan . ' ' . $tahun . ' ' . $jam . ' WIB';
        } else {
            return $tanggal . ' ' . $bulan . ' ' . $tahun;
        }    
    }    
}

function bulan($bln) {
    switch ($bln)
    {
        case 1:
            return "Januari";
            break;
        case 2:
            return "Februari";
            break;
        case 3:
            return "Maret";
            break;
        case 4:
            return "April";
            break;
        case 5:
            return "Mei";
            break;
        case 6:
            return "Juni";
            break;
        case 7:
            return "Juli";
            break;
        case 8:
            return "Agustus";
            break;
        case 9:
            return "September";
            break;
        case 10:
            return "Oktober";
            break;
        case 11:
            return "November";
            break;
        case 12:
            return "Desember";
            break;
    }
}

function bulan_romawi($id=null) {
    $id = ($id != '') ? $id : date('m');
    if($id == '01') $r = 'I';
    elseif($id == '02') $r = 'II';
    elseif($id == '03') $r = 'III';
    elseif($id == '04') $r = 'IV';
    elseif($id == '05') $r = 'V';
    elseif($id == '06') $r = 'VI';
    elseif($id == '07') $r = 'VII';
    elseif($id == '08') $r = 'VIII';
    elseif($id == '09') $r = 'IX';
    elseif($id == '10') $r = 'X';
    elseif($id == '11') $r = 'XI';
    elseif($id == '12') $r = 'XII';
    return $r;
}

function js_chosen() {    
    $html = "<script>
             $(document).ready(function() {
                $('.chosen-select').select2();
            });
             </script>";
    return $html;
}

function js_datepicker() {    
    $html = '<script type="text/javascript"> ';
    $html .= "$('.datepicker').datepicker({dateFormat:'yy-mm-dd'});";
    $html .= '</script> ';
    return $html;
}

function js_currency() {    
    $html = '<script type="text/javascript"> ';
    $html .= '$(".currency").autoNumeric({ aSep: ".", aDec: ",", vMax: "999999999999999", vMin: "0" });';
    $html .= '</script> ';
    return $html;
}

function convert_size_txt($size=null) {
    // bytes
    if($size < 1024) { $size_convert = $size." byte"; }
    // kilobytes
    if($size > 1024) { $size_convert = number_format(($size/1024), 2, '.', '')." Kb"; }
    // mega bytes
    if($size_convert > 1024) { $size_convert = number_format(($size_convert/1024), 2, '.', '')." Mb"; }
    // giga bytes
    if($size_convert > 1024) { $size_convert = number_format(($size_convert/1024), 2, '.', '')." Gb"; }

    return $size_convert;
}

function captcha($length = 6) {
    $characters = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

function password($length = 4) {
    $characters = '123456789abcdefghijklmnopqrsutvwxyz';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 2)];
    }
    return $randomString;
}

function terbilang($x) {
        
    $x = abs((int) $x);
    
    if($x >= 0) {
                        
        $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam","Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($x < 12) {
            return " " . $abil[$x];
        } elseif ($x < 20) {
            return terbilang($x - 10) . " Belas";
        } elseif ($x < 100) {
            return terbilang($x / 10) . " Puluh" . terbilang($x % 10);
        } elseif ($x < 200) {
            return " Seratus" . terbilang($x - 100);
        } elseif ($x < 1000) {
            return terbilang($x / 100) . " Ratus" . terbilang($x % 100);
        } elseif ($x < 2000) {
            return " Seribu" . terbilang($x - 1000);
        } elseif ($x < 1000000) {
            return terbilang($x / 1000) . " Ribu" . terbilang($x % 1000);
        } elseif ($x < 1000000000) {
            return terbilang($x / 1000000) . " Juta" . terbilang($x % 1000000);       
        }else{
            return '';  
        }
    }   

}

function nbs($x) {
    for($i=0;$i<=$x;$i++) {
        echo '&nbsp;';
    }
}

function jecho($a, $b, $str) {
    if ($a == $b) {
        echo $str;
    }
}

function create_nama_lengkap($gelar_depan=null, $nama=null, $gelar_belakang=null) {
    if($gelar_depan != '') {
        $prefix = $gelar_depan.' ';
    } else {
        $prefix = '';
    }
    //
    if($gelar_belakang != '') {
        $suffix = ', '.$gelar_belakang;
    } else {
        $suffix = '';
    }
    $result = @$prefix.$nama.@$suffix;
    return $result;
}

function create_negeri($text=null) {
    $arr = explode(" ", $text);
    $first_word = str_replace("N"," Negeri",@$arr[0]);
    //
    if (@$arr[3] !='') {
        $arr_3 = ' '.@$arr[3];
    }else{
        $arr_3 = '';
    }

    if (@$arr[4] !='') {
        $arr_4 = ' '.@$arr[4];
    }else{
        $arr_4 = '';
    }

    if (@$arr[5] !='') {
        $arr_5 = ' '.@$arr[5];
    }else{
        $arr_5 = '';
    }

    if (@$arr[6] !='') {
        $arr_6 = ' '.@$arr[6];
    }else{
        $arr_6 = '';
    }

    if (@$arr[7] !='') {
        $arr_7 = ' '.@$arr[7];
    }else{
        $arr_7 = '';
    }

    if (@$arr[8] !='') {
        $arr_8 = ' '.@$arr[8];
    }else{
        $arr_8 = '';
    }

    if (@$arr[9] !='') {
        $arr_9 = ' '.@$arr[9];
    }else{
        $arr_9 = '';
    }

    if (@$arr[10] !='') {
        $arr_10 = ' '.@$arr[10];
    }else{
        $arr_10 = '';
    }
    //
    $result = $first_word.' '.@$arr[1].' '.@$arr[2].$arr_3.$arr_4.$arr_5.$arr_6.$arr_7.$arr_8.$arr_9.$arr_10;
    //
    return $result;
}

function date_dayname($id=null) {
    $arr = array(
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu',
    );
    if($id != '') return $arr[$id];
    else return $arr;
}

function date_now($date=null) {
    if($date == '') $date = date('Y-m-d H:i:s');
    $datetime = DateTime::createFromFormat('Y-m-d H:i:s', $date);
    $dayindex = $datetime->format('D');
    $dayname  = @date_dayname($dayindex);
    //
    $result = $dayname.', '.convert_date_indo($date);
    return $result;
}

function convert_day_name($date=null) {
    if($date == '') $date = date('Y-m-d');
    $datetime = DateTime::createFromFormat('Y-m-d', $date);
    $dayindex = $datetime->format('D');
    $dayname  = @date_dayname($dayindex);
    //
    $result = $dayname.' / '.convert_date_indo($date);
    return $result;
}

function get_st_bayar($data=null) {
    if ($data == '1') {
        $result = "Terbayar";
    }else{
        $result = "Belum";
    }
    //
    return $result;
}

function str($str,$a,$b) {
    return substr($str, $a, $b);
}                    

function tgl_indo_out($tgl)
{
    $tanggal = substr($tgl, 8, 2);
    $bulan = substr($tgl, 5, 2);
    $tahun = substr($tgl, 0, 4);
    return $tanggal . '-' . $bulan . '-' . $tahun;
}

function tgl_indo_in($tgl)
{
    $tanggal = substr($tgl, 0, 2);
    $bulan = substr($tgl, 3, 2);
    $tahun = substr($tgl, 6, 4);
    return $tahun . '-' . $bulan . '-' . $tanggal;
}

function tgl_indo($tgl)
{
    $tanggal = substr($tgl, 8, 2);
    $bulan = getBulan(substr($tgl, 5, 2));
    $tahun = substr($tgl, 0, 4);
    return $tanggal . ' ' . $bulan . ' ' . $tahun;
}

function getBulan($bln)
{
    switch ($bln)
    {
        case 1:
            return "Januari";
            break;
        case 2:
            return "Februari";
            break;
        case 3:
            return "Maret";
            break;
        case 4:
            return "April";
            break;
        case 5:
            return "Mei";
            break;
        case 6:
            return "Juni";
            break;
        case 7:
            return "Juli";
            break;
        case 8:
            return "Agustus";
            break;
        case 9:
            return "September";
            break;
        case 10:
            return "Oktober";
            break;
        case 11:
            return "November";
            break;
        case 12:
            return "Desember";
            break;
    }
}

function pembagian($angka_1='', $angka_2=''){
    if ($angka_2 == '0') {
        return 0;
    }else{
        $result = $angka_1 / $angka_2;
        return digit($result);
    }
}

function convert_status_indo($status=null) {
    if ($status == 'up') {
        $result = 'Naik';
    }elseif ($status == 'down') {
        $result = 'Turun';
    }elseif ($status == 'fixed') {
        $result = 'Tetap';
    }
    //
    return $result;
}

function get_font_status($status=null) {
    if ($status == 'up') {
        $result = 'fa fa-caret-up';
    }elseif ($status == 'down') {
        $result = 'fa fa-caret-down';
    }elseif ($status == 'fixed') {
        $result = 'fa fa-minus';
    }
    //
    return $result;
}

function get_status_aktif($status=null) {
    if ($status == '1') {
        $result = '<i class="fa fa-check text-success" data-toggle="tooltip" data-placement="top" data-original-title="Aktif"></i>';
    }else{
        $result = '<i class="fa fa-times text-danger" data-toggle="tooltip" data-placement="top" data-original-title="Tidak Aktif"></i>';
    }
    //
    return $result;
}

function session_get_id($data=null) {
    $array = explode('#', $data);
    //
    return @$array[0];
}

function session_get_name($data=null) {
    $array = explode('#', $data);
    //
    return @$array[1];
}

function get_tgl($tgl=null) {
    $result = substr($tgl,0,-8);
    //
    return $result;
}

function explode_status_harga($status=null, $st=null) {
    $array = explode('#', $status);
    //
    if ($st == 'get_color') {
        $result = $array[0];
    }elseif ($st == 'get_desc') {
        $result = $array[1];
    }elseif ($st == 'get_icon') {
        $result = $array[2];
    }else{
        $result = $status;
    }
    //
    return $result;
}

function get_min($values) {
    return min(array_diff(array_map('floatval', @$values), array(0)));
}

function get_max($values) {
    return max(array_diff(array_map('floatval', @$values), array(0)));
}

function convert_size($bytes, $precision = 2) { 
    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow = min($pow, count($units) - 1); 

    // Uncomment one of the following alternatives
    // $bytes /= pow(1024, $pow);
    $bytes /= (1 << (10 * $pow)); 

    return round($bytes, $precision) . ' ' . $units[$pow]; 
} 

function image_transparent($image_file='') {
    $img = imagecreatetruecolor(200, 200);
    imagesavealpha($img, true);
    $color = imagecolorallocatealpha($img, 0, 0, 0, 127);
    imagefill($img, 0, 0, $color);
    $result = imagepng($img, $image_file);
    //
    return $result;
}

function getWeeks($date, $rollover)
{
    $cut = substr($date, 0, 8);
    $daylen = 86400;

    $timestamp = strtotime($date);
    $first = strtotime($cut . "00");
    $elapsed = ($timestamp - $first) / $daylen;

    $weeks = 1;

    for ($i = 1; $i <= $elapsed; $i++)
    {
        $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
        $daytimestamp = strtotime($dayfind);

        $day = strtolower(date("l", $daytimestamp));

        if($day == strtolower($rollover))  $weeks ++;
    }

    return $weeks;
}

function weeks($month, $year){
    $num_of_days = date("t", mktime(0,0,0,$month,1,$year)); 
    $lastday = date("t", mktime(0, 0, 0, $month, 1, $year)); 
    $no_of_weeks = 0; 
    $count_weeks = 0; 
    while($no_of_weeks < $lastday){ 
        $no_of_weeks += 7; 
        $count_weeks++; 
    } 
    return $count_weeks;
} 

function numWeeks($year, $month, $start=0){
    $unix = strtotime("$year-$month-01");
    $numDays = date('t', $unix);
    if ($start===0){
        $dayOne = date('w', $unix); // sunday based week 0-6
    } else {
        $dayOne = date('N', $unix); //monday based week 1-7
        $dayOne--; //convert for 0 based weeks
    }
    
    //if day one is not the start of the week then advance to start
    $numWeeks = floor(($numDays - (6 - $dayOne))/7);
    return $numWeeks;
}

function week_from_monday($date) {
    // Assuming $date is in format DD-MM-YYYY
    list($day, $month, $year) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for 7 days from Monday (inclusive)
    for($i=0; $i<7; $i++)
    {
        $dates[$i] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
    }

    return $dates;
}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
}

function get_range_date($date_start='', $date_end='') {
    $begin = new DateTime($date_start);
    $end = new DateTime($date_end);
    $end = $end->modify('+1 day'); 

    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);

    return $daterange;
}

function get_ip_address() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function get_device() {
    // Include or require the Mobile_Detect.php file from mobiledetect.net
    include_once APPPATH . '/third_party/detect/Mobile_Detect.php';

    // Include or require the class file
    include_once APPPATH . '/third_party/detect/detect.php';

    // Any mobile device (phones or tablets)
    if (Detect::isMobile()) {

    }

    // Gets the device type ('Computer', 'Phone' or 'Tablet')
    $result['device_type'] = Detect::deviceType();

    // Any phone device
    if (Detect::isPhone()) {

    }

    // Any tablet device
    if (Detect::isTablet()) {

    }

    // Any computer device (desktops or laptops)
    if (Detect::isComputer()) {

    }

    // Get the name & version of operating system
    $result['os_type'] = Detect::os();

    // Get the name & version of browser
    $result['browser_type'] = Detect::browser();
    //
    return $result;
}

function create_token($length=16) {
    $result = bin2hex(openssl_random_pseudo_bytes($length));
    return $result;
}

function img_perangkat($name='') {
    if ($name == 'Computer') {
        $result = 'bg-blue.png';
    }elseif ($name == 'Phone') {
        $result = 'bg-green.png';
    }else{
        $result = 'bg-red.png';
    }
    //
    return $result;
}

function icon_perangkat($name='') {
    if ($name == 'Computer') {
        $result = 'ti-desktop';
    }elseif ($name == 'Phone') {
        $result = 'ti-mobile';
    }else{
        $result = 'ti-menu';
    }
    //
    return $result;
}

function time_elapsed($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'tahun',
        'm' => 'bulan',
        'w' => 'minggu',
        'd' => 'hari',
        'h' => 'jam',
        'i' => 'menit',
        's' => 'detik',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' yang lalu' : 'baru saja';
}

function font_icon_st($st=null) {
    if ($st == '1') {
        echo '<i class="ion-checkmark-round text-success"></i>';
    }else{
        echo '<i class="ion-close-round text-danger"></i>';
    }
}