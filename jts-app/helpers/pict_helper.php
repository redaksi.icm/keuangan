<?php
function get_file_type($file_name=null) {
    $arr = explode('.', $file_name);
    $len = count($arr)-1;
    $file_type = $arr[$len];
    return $file_type;
}

function upload_user_image($subdomain, $date, $image_no, $path_dir, $tmp_name, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    $file_name = $subdomain . '.' . $date . '-' . $image_no. '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    move_uploaded_file($tmp_name, $vfile_upload);
    //
    return @$file_name;
}

function no_upload_images($subdomain, $date, $image_no, $path_dir, $tmp_name, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    $file_name = $subdomain . '.' . $date . '-' . $image_no. '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    // move_uploaded_file($tmp_name, $vfile_upload);
    //
    return @$file_name;
}

function upload_komoditas_image($subdomain, $date, $image_no, $path_dir, $tmp_name, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    $file_name = $subdomain . '.' . $date . '-' . $image_no. '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    move_uploaded_file($tmp_name, $vfile_upload);
    //
    return @$file_name;
}

function upload_verifikasi_sppt_image($subdomain, $date, $image_no, $path_dir, $tmp_name, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    $file_name = $subdomain . '.' . $date . '-' . $image_no. '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    move_uploaded_file($tmp_name, $vfile_upload);
    //
    return @$file_name;
}

function upload_post_image($subdomain, $date, $image_no, $path_dir, $tmp_name, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    if ($file_type == 'jpeg') {
        $file_name = $subdomain . '.' . $date . '-' . $image_no. '.' . 'jpg';
    }else{
        $file_name = $subdomain . '.' . $date . '-' . $image_no. '.' . $file_type;
    }
    $vfile_upload = $path_dir . $file_name;
    move_uploaded_file($tmp_name, $vfile_upload);
    //
    return @$file_name;
}

function upload_move_video($subdomain, $date, $image_no, $path_dir, $tmp_name, $fupload_name, $pasar_id, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    $file_name = $subdomain . '.' . $date . '-' . $pasar_id. '-' . $image_no. '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    //
    if (!$tmp_name) { // if file not chosen
        echo "ERROR: Please browse for a file before clicking the upload button.";
        exit();
    }
    if(move_uploaded_file($tmp_name, $vfile_upload)){
        echo "$fupload_name upload is completed";
    } else {
        echo "move_uploaded_file function failed";
    }

    //
    return @$file_name;
}

function upload_post_image_api($subdomain, $date, $image_no, $path_dir, $image_pos, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $explode_1 = explode('/', $image_pos);
    $explode_2 = explode(';', $explode_1[1]);
    //
    $file_type = get_file_type($fupload_name);
    $file_name = $subdomain . '.' . $date . '-' . $image_no. '.' . $explode_2[0];
    //
    // base64_to_image($image_pos, $path_dir.$file_name);

    // $get_size = getimagesize($image_pos);
    // var_dump($get_size);
    // exit();

    // $imgData = resize_image($path_dir.$file_name, 300, 300);
    //
    // $resizedFilename = $path_dir.'resize.png';
    // imagepng($imgData, $resizedFilename);

    compress_image($image_pos, $path_dir.$file_name, 20);
    //
    return @$file_name;
}

function base64_to_image($base64_string, $output_file) {
    $ifp = fopen($output_file, 'wb'); 
    $data = explode(',', $base64_string);
    fwrite($ifp, base64_decode($data[1]));
    fclose($ifp); 
    //
    return $output_file; 
}

function compress_image($source_url, $destination_url, $quality) {

   $info = getimagesize($source_url);

    if ($info['mime'] == 'image/jpeg')
          $image = imagecreatefromjpeg($source_url);

    elseif ($info['mime'] == 'image/gif')
          $image = imagecreatefromgif($source_url);

  elseif ($info['mime'] == 'image/png')
          $image = imagecreatefrompng($source_url);

    imagejpeg($image, $destination_url, $quality);
return $destination_url;
}

function resize_image($file, $w, $h, $crop=false) {
    list($width, $height) = getimagesize($file);
    $r = $width / $height;
    if ($crop) {
        if ($width > $height) {
            $width = ceil($width-($width*abs($r-$w/$h)));
        } else {
            $height = ceil($height-($height*abs($r-$w/$h)));
        }
        $newwidth = $w;
        $newheight = $h;
    } else {
        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }
    }
    
    //Get file extension
    $exploding = explode(".",$file);
    $ext = end($exploding);
    
    switch($ext){
        case "png":
            $src = imagecreatefrompng($file);
        break;
        case "jpeg":
        case "jpg":
            $src = imagecreatefromjpeg($file);
        break;
        case "gif":
            $src = imagecreatefromgif($file);
        break;
        default:
            $src = imagecreatefromjpeg($file);
        break;
    }
    
    $dst = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    return $dst;
}

function upload_post_file($subdomain, $date, $file_no, $path_dir, $tmp_name, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    $file_name = $subdomain . '.' . $date . '-' . $file_no. '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    move_uploaded_file($tmp_name, $vfile_upload);
    //
    return @$file_name;
}

function upload_gallery_image($subdomain, $date, $image_no, $path_dir, $tmp_name, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    $file_name = $subdomain . '.' . $date . '-' . $image_no. '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    move_uploaded_file($tmp_name, $vfile_upload);
    //
    return @$file_name;
}

function upload_slideshow_image($subdomain, $date, $image_no, $path_dir, $tmp_name, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    $file_name = $subdomain . '.' . $date . '-' . $image_no. '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    move_uploaded_file($tmp_name, $vfile_upload);
    //
    return @$file_name;
}

function upload_document($subdomain, $date, $title, $path_dir, $tmp_name, $fupload_name, $old_file=null) {
    if($old_file != "") {
        unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    // $file_name = $subdomain . '.' . $date . '-' . $title. '.' . $file_type;
    $file_name = $subdomain. '.'. $title. '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    move_uploaded_file($tmp_name, $vfile_upload);
    //
    return @$file_name;
}


// ----------

function UploadFoto($fupload_name, $old_foto, $files=null)
{
    if($files == '') $files = "foto";

    $vdir_upload = "assets/images/photo/";
    if ($old_foto != "")
        unlink($vdir_upload . "kecil_" . $old_foto);

    $vfile_upload = $vdir_upload . $fupload_name;
    move_uploaded_file($_FILES[$files]["tmp_name"], $vfile_upload);
    $im_src = imagecreatefromjpeg($vfile_upload);
    $src_width = imageSX($im_src);
    $src_height = imageSY($im_src);
    if ($src_width < $src_height)
    {
        $dst_width = 100;
        $dst_height = ($dst_width / $src_width) * $src_height;
        $cut_height = ($dst_height - 100) / 2;
        $im = imagecreatetruecolor(100, 100);
        imagecopyresampled($im, $im_src, 0, -($cut_height), 0, $cut_height, $dst_width, $dst_height, $src_width, $src_height);
    }
    else
    {
        $dst_height = 100;
        $dst_width = ($dst_height / $src_height) * $src_width;
        $cut_width = ($dst_width - 100) / 2;
        $im = imagecreatetruecolor(100, 100);
        imagecopyresampled($im, $im_src, -($cut_width), 0, $cut_width, 0, $dst_width, $dst_height, $src_width, $src_height);
    }
    imagejpeg($im, $vdir_upload . "kecil_" . $fupload_name);
    imagedestroy($im_src);
    imagedestroy($im);

    unlink($vfile_upload);
    return true;
}


?>
