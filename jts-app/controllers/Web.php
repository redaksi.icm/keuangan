<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web extends CI_Controller{

    function __construct() {
        parent::__construct();
        //
        // $this->load->model('post_model');
    }

    var $post_st = '1';
    var $active_menu = 'home_publik';

    function index() {  
        redirect('manage/login');
    }

    function location($act=null,$id1=null,$id2=null,$id3=null) {
        // clear
        $clear = $this->input->get('clear');
        if($clear == 'ses_txt_search') {
            $_SESSION['ses_search']['ses_txt_search'] = '';
        } else {
            unset_session('ses_search,success,captcha,captcha_two,type_img');
        }       
        // active_menu
        $active_menu = $this->input->get('active_menu');
        if($active_menu != '') {
            $_SESSION['ses_search']['ses_active_menu'] = $active_menu;
        } else {
            $_SESSION['ses_search']['ses_active_menu'] = '';
        }
        // modal_register
        $modal_register = $this->input->get('modal_register');
        if($modal_register != '') {
            $_SESSION['ses_search']['ses_modal_register'] = $modal_register;
        } else {
            $_SESSION['ses_search']['ses_modal_register'] = '';
        }
        // general
        if($id1 != '') {
            $url = $act.'/index/'.$id1;
            if($id2 != '') $url .= '/'.$id2;
            if($id3 != '') $url .= '/'.$id3;
            redirect($url); 
        } else {
            redirect($act);
        }       
    }
    
}