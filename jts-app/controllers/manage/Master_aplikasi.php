<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_aplikasi extends CI_Controller{

	var $active_menu = 'konfigurasi';
	var $menu_url = 'master_aplikasi';

	function __construct() {
		parent::__construct();
		//
		$this->config_model->validate_login(@$this->menu_url);
		//
        $this->load->model('config_model');
	}
	
	function index() {	
		redirect('manage/master_aplikasi/form');
	}

	function form() {	
		$header = $this->config_model->general();
		$footer = $this->config_model->footer();
		// get dinamis menu
		$header['top_menu_dashboard'] = $this->menu_dashboard_show_model->get_all_menu_parent_show('1');
		//
		$data['main'] = $this->config_model->get_config();
		$data['form_action'] = site_url('manage/master_aplikasi/update');
		//
		$this->load->view('manage/template/header',$header);		
		$this->load->view('manage/master_aplikasi/form',$data);
		$this->load->view('manage/template/footer',$footer);
	}

	function update() {
		$this->config_model->update_config();
		redirect('manage/master_aplikasi/form');
	}
	
}