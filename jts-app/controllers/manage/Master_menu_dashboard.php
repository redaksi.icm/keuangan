<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_menu_dashboard extends CI_Controller{

	var $active_menu = 'master_user';
	var $menu_url = 'master_menu_dashboard';

	function __construct() {
		parent::__construct();
		//
		$this->config_model->validate_login(@$this->menu_url);
		//
        $this->load->model('menu_dashboard_model');
        $this->load->model('label_menu_model');
	}
	
	function index($p=1, $o=0) {	
		$header = $this->config_model->general();
		$header['title_web'] = 'Data Menu Dashoard | '.$header['config']['app_title'];
		$data['validate_menu'] = $this->validate_menu_model->validate_menu(@$this->menu_url, @$this->active_menu, $header['profile']['user_group']);
		//
		$data['p'] = $p;
		$data['o'] = $o;
		$data['ses_search'] = @$_SESSION['ses_search'];
		$data['title'] = 'Data Menu Dashoard';
		//
		$data['paging'] = $this->menu_dashboard_model->paging_menu_dashboard($p,$o);
		$data['main'] = $this->menu_dashboard_model->list_menu_dashboard($o, $data['paging']->offset, $data['paging']->per_page);
		$data['get_all_menu_dashboard'] = $this->menu_dashboard_model->get_all_menu_dashboard();
		//
		$this->load->view('manage/template/header',$header);		
		$this->load->view('manage/master_menu_dashboard/index',$data);
		$this->load->view('manage/template/footer');
	}

	function form($p=1, $o=0, $id=null) {	
		$header = $this->config_model->general();
		$header['title_web'] = 'Form Menu Dashboard | '.$header['config']['app_title'];
		// validate menu
		if ($id !='') {
			$this->validate_menu_model->validate_menu(@$this->menu_url, @$this->active_menu, $header['profile']['user_group'], 'edit', 'redirect');
		}else{
			$this->validate_menu_model->validate_menu(@$this->menu_url, @$this->active_menu, $header['profile']['user_group'], 'add', 'redirect');
		}
		//
		$data['p'] = $p;
		$data['o'] = $o;
		$data['ses_search'] = @$_SESSION['ses_search'];
		$data['title'] = 'Data Menu Dashoard';
		//
		if($id != '') {
			$data['main'] = $this->menu_dashboard_model->get_menu_dashboard($id);
			$data['form_action'] = site_url('manage/master_menu_dashboard/update/'.$p.'/'.$o.'/'.$id);
		} else {
			$data['main'] = array();
			$data['form_action'] = site_url('manage/master_menu_dashboard/insert');
		}
		//
		$data['list_menu_parent'] = $this->menu_dashboard_model->get_all_menu_parent('1');
		$data['list_label_menu'] = $this->label_menu_model->get_all_label_menu();
		//
		$this->load->view('manage/template/header',$header);		
		$this->load->view('manage/master_menu_dashboard/form',$data);
		$this->load->view('manage/template/footer');
	}

	function get_list_label_menu() {
	    $data = $_POST;
	    //
	    $header = $this->config_model->general();
	    $result['list_label_menu'] = $this->label_menu_model->get_all_label_menu();
	    //
		echo json_encode($result);
	}

	function insert_label_menu() {
	    $data = $_POST;
	    //
	    $data_label_menu = array(
	      'label_menu_nm' => $data['label_menu_nm'],
	      'label_menu_order' => $data['label_menu_order'],
	      'created_date' => date('Y-m-d H:i:s')
	    );
	    $this->label_menu_model->insert($data_label_menu);
	}

	function update_label_menu_show() {
	    $label_menu_id = $this->input->post('label_menu_id');
	    $data = $this->label_menu_model->get_label_menu($label_menu_id);
	    echo json_encode($data);
	}

	function update_label_menu() {
	    $data = $_POST;
	    $label_menu_id = $data['label_menu_id'];
	    //
	    $data_label_menu = array(
	      'label_menu_nm' => $data['label_menu_nm'],
	      'label_menu_order' => $data['label_menu_order'],
	      'updated_date' => date('Y-m-d H:i:s')
	    );
	    $this->label_menu_model->update($label_menu_id, $data_label_menu);
	}

	public function delete_label_menu() {
	    $label_menu_id = $this->input->post('label_menu_id');
	    $this->label_menu_model->delete($label_menu_id);
	}
	
	function search() {
		$ses_txt_search = $this->input->post('ses_txt_search');		
		$ses_menu_dashboard_id = $this->input->post('ses_menu_dashboard_id');		
		//
		$_SESSION['ses_search']['ses_txt_search'] = ($ses_txt_search != '') ? $ses_txt_search : false;
		$_SESSION['ses_search']['ses_menu_dashboard_id'] = ($ses_menu_dashboard_id != '') ? $ses_menu_dashboard_id : false;
		//
		redirect('manage/master_menu_dashboard/index');
	}

	function insert() {
		$this->menu_dashboard_model->insert();
		redirect('manage/master_menu_dashboard/index');
	}

	function insert_partial($menu_parent=null) {
		$menu = $this->menu_dashboard_model->insert_partial($menu_parent);
		if($menu == false) {
			$prepend = "false";
		} else {
			$prepend = "<option value='".$menu['menu_id']."' selected='selected'>".$menu['menu_title']."</option>";	
		}		
		//
		echo json_encode(array(
			'prepend' => $prepend,
		));
	}

	function update($p, $o, $id) {
		$this->menu_dashboard_model->update($id);
		redirect('manage/master_menu_dashboard/index');
	}

	function delete($p, $o, $id) {
		$header = $this->config_model->general();
		// validate menu
		$this->validate_menu_model->validate_menu(@$this->menu_url, @$this->active_menu, $header['profile']['user_group'], 'delete', 'redirect');
		//
		$this->menu_dashboard_model->delete($id);
		redirect('manage/master_menu_dashboard/index');
	}

	function delete_all($p, $o) {
		$this->menu_dashboard_model->delete_all();
		redirect('manage/master_menu_dashboard/index');
	}

	function ajax($id = null) {
		if($id == 'permalink') {
			$menu_title = $this->input->get('menu_title');
			$permalink = clean_url($menu_title);
			//
			echo json_encode(array(
				'permalink'	=> $permalink
			));	
		}elseif($id == 'get_sub_menu_level_1') {
            $menu_id = $this->input->get('menu_id');
            $sub_menu_level_1 = $this->input->get('sub_menu_level_1');
            //
            $list_sub_menu_level_1 = $this->menu_dashboard_model->get_sub_menu_level_1('1', $menu_id);
            //
            $html = '';
            $html.= '<select name="sub_menu_level_1" id="sub_menu_level_1" class="form-control input-sm chosen-select">';
            $html.= '<option value="">- None -</option>';
            foreach($list_sub_menu_level_1 as $row) {
                if($sub_menu_level_1 == $row['menu_id']) {
                    $html.= '<option value="'.$row['menu_id'].'" selected>'.$row['menu_title'].'</option>';  
                } else {
                    $html.= '<option value="'.$row['menu_id'].'">'.$row['menu_title'].'</option>';
                }               
            }
            $html.= '</select>';
            $html.= js_chosen();
            //
            echo json_encode(array(
                'html' => $html
            ));
        }
	}
	
}