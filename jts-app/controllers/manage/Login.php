<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller{

	function __construct() {
		parent::__construct();
	}

	function index() {
		$header = $this->config_model->general();		
		// Hapus Log
		$file = "application/logs/qrcode.png-errors.txt";
		$folder_file = "application/logs/";
		$filesize = filesize($file);
		if ($filesize >= 4000000) {
		    file_put_contents($file, ''); // Hapus isi file dari $file
		} 
		//
		$this->config_model->set_login(@$header['profile']['user_id'], 'offline');
		//
		$this->config_model->clear_login_session();
		// create session token
		$_SESSION['_token'] = create_token(32);
		//
		$this->config_model->insert_token_login();
		$data['token_val'] = $this->config_model->get_token_login();
		$data['_token'] = $_SESSION['_token'];
		//
		$this->load->view('manage/template/header-login',$header);		
		$this->load->view('manage/login/login',$data);
		$this->load->view('manage/template/footer-login');
	}	

	function ajax($id = null) {
		// authentikasi login
		if($id == 'auth_login') {
			$t_username = anti_injection($this->input->post('t_username'));
			$t_password = anti_injection($this->input->post('t_password'));
			$t_captcha_hide = anti_injection($this->input->post('t_captcha_hide'));
			$t_captcha = anti_injection($this->input->post('t_captcha'));
			$t_token_login = anti_injection($this->input->post('t_token_login'));
			$_token = anti_injection($this->input->post('_token'));
			//
			if($t_username != '' && $t_password != '' && $t_captcha_hide != '' && $t_captcha != '' && $t_token_login != '') {
				$validate = $this->config_model->auth_login($t_username, $t_password, $t_captcha_hide, $t_captcha);
				// get_token
				$get_token = $this->config_model->get_token_login();
				//
				if ($t_token_login != $get_token['token_login']) {
					echo json_encode(array(
						'result' 	=> 'false', 
						'message' 	=> '<i class="fa fa-warning"></i> Anda Gagal Login ! Silahkan Refresh Halaman ini & Coba Lagi', 
					));
				}else{
					if ($_token != $_SESSION['_token']) {
						echo json_encode(array(
							'result' 	=> 'false', 
							'message' 	=> '<i class="fa fa-warning"></i> Anda Gagal Login ! Silahkan Refresh Halaman ini & Coba Lagi', 
						));
					} else{
						if($validate == '1') {
							echo json_encode(array(
								'result' 	=> 'true', 
								'redirect' 	=> site_url('manage/index')
							));
						} elseif($validate == '2') {
							echo json_encode(array(
								'result' 	=> 'false', 
								'message' 	=> '<i class="fa fa-warning"></i> Maaf, Username ini tidak aktif !', 
							));
						} elseif($validate == '3') {
							echo json_encode(array(
								'result' 	=> 'false', 
								'message' 	=> '<i class="fa fa-warning"></i> Maaf, Captcha Salah !', 
							));
						} else {
							echo json_encode(array(
								'result' 	=> 'false', 
								'message' 	=> '<i class="fa fa-warning"></i> Maaf, Username dan atau password anda salah !', 
							));
						}
					}
				}
			} else {
				redirect('');
			}			
		}
	}

	function refresh_captcha() {
        echo json_encode(array(
            'callback'      => 'true',
            'captcha_img'   => captcha()
        ));
    }
	
	
}