<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller{

    var $active_menu = 'index';
    var $menu_url = 'index';

    function __construct() {
        parent::__construct();
        //
        $this->config_model->validate_login(@$this->menu_url);
        //
    }

    function index() {  
        $header = $this->config_model->general();
        $footer = $this->config_model->footer();
        //
        $data = "";
        //
        $this->load->view('manage/template/header',$header);        
        $this->load->view('manage/home/index',$data);
        $this->load->view('manage/template/footer',$footer);
        //
    }

    function location($act=null,$id1=null,$id2=null,$id3=null) {
        // clear
        $clear = $this->input->get('clear');
        if($clear == 'ses_txt_search') {
            $_SESSION['ses_search']['ses_txt_search'] = '';
        } else {
            unset_session('ses_search,success,captcha,captcha_two,type_img');
        }       
        // active_menu
        $active_menu = $this->input->get('active_menu');
        if($active_menu != '') {
            $_SESSION['ses_search']['ses_active_menu'] = $active_menu;
        } else {
            $_SESSION['ses_search']['ses_active_menu'] = '';
        }
        //
        // general
        if($id1 != '') {
            $url = 'manage/'.$act.'/index/'.$id1;
            if($id2 != '') $url .= '/'.$id2;
            if($id3 != '') $url .= '/'.$id3;
            redirect($url); 
        } else {
            redirect('manage/'.$act);
        }       
    }
    
}