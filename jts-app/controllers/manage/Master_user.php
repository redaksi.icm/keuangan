<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_user extends CI_Controller{

	var $active_menu = 'master_user';
	var $menu_url = 'master_user';

	function __construct() {
		parent::__construct();
		//
		$this->config_model->validate_login(@$this->menu_url);
		//
        $this->load->model('user_model');
	}
	
	function index($p=1, $o=0) {	
		$header = $this->config_model->general();
		$footer = $this->config_model->footer();
		// get dinamis menu
		$header['top_menu_dashboard'] = $this->menu_dashboard_show_model->get_all_menu_parent_show('1');
		//
		$data['p'] = $p;
		$data['o'] = $o;
		$data['ses_search'] = @$_SESSION['ses_search'];
		//
		$data['paging'] = $this->user_model->paging_user($p,$o);
		$data['main'] = $this->user_model->list_user($o, $data['paging']->offset, $data['paging']->per_page);
		//
		$data['list_usergroup'] = $this->user_model->list_usergroup();
		//
		$this->load->view('manage/template/header',$header);        
        $this->load->view('manage/master_user/index',$data);
        $this->load->view('manage/template/footer',$footer);
	}

	function form($p=1, $o=0, $id=null) {	
		$header = $this->config_model->general();
		$footer = $this->config_model->footer();
		// get dinamis menu
		$header['top_menu_dashboard'] = $this->menu_dashboard_show_model->get_all_menu_parent_show('1');
		//
		$data['p'] = $p;
		$data['o'] = $o;
		$data['id'] = $id;
		//
		if($id != '') {
			$data['main'] = $this->user_model->get_user($id);
			$data['form_action'] = site_url('manage/master_user/update/'.$p.'/'.$o.'/'.$id);
		} else {
			$data['main'] = array();
			$data['form_action'] = site_url('manage/master_user/insert');
			$data['main']['is_pegawai'] = $this->input->get('is_pegawai');
		}
		//
		$data['list_usergroup'] = $this->user_model->list_usergroup();	
		//
		$this->load->view('manage/template/header',$header);		
		$this->load->view('manage/master_user/form',$data);
		$this->load->view('manage/template/footer',$footer);
	}

	function detail($view_p=1, $view_o=0, $id=null, $p=1, $o=0) {	
		$header = $this->config_model->general();
		$footer = $this->config_model->footer();
		// get dinamis menu
		$header['top_menu_dashboard'] = $this->menu_dashboard_show_model->get_all_menu_parent_show('1');
		//
		$data['view_p'] = $view_p;
		$data['view_o'] = $view_o;
		$data['p'] = $p;
		$data['o'] = $o;
		$data['id'] = $id;
		//
		$data['main'] = $this->user_model->get_user($id);
		$data['paging'] = $this->user_model->paging_log_login($p,$o,$id);
		$data['list_log_login'] = $this->user_model->list_log_login($p, $data['paging']->offset, $data['paging']->per_page,$id);
		//
		$this->load->view('manage/template/header',$header);		
		$this->load->view('manage/master_user/detail',$data);
		$this->load->view('manage/template/footer',$footer);
	}
	
	function search() {
		$ses_txt_search = $this->input->post('ses_txt_search');		
		$ses_user_st = $this->input->post('ses_user_st');		
		$ses_user_group = $this->input->post('ses_user_group');		
		//
		$_SESSION['ses_search']['ses_txt_search'] = ($ses_txt_search != '') ? anti_injection($ses_txt_search,'all') : false;
		$_SESSION['ses_search']['ses_user_st'] = ($ses_user_st != '') ? anti_injection($ses_user_st,'all') : false;
		$_SESSION['ses_search']['ses_user_group'] = ($ses_user_group != '') ? anti_injection($ses_user_group,'all') : false;
		//
		redirect('manage/master_user/index');
	}

	function insert() {
		$this->user_model->insert();
		redirect('manage/master_user/index');
	}

	function update($p, $o, $id) {
		$this->user_model->update($id);
		redirect('manage/master_user/index');
	}

	function status($p, $o, $id, $status) {
		$this->user_model->change_status($id, $status);
		redirect('manage/master_user/index');
	}

	function delete($p, $o, $id) {
		$this->user_model->delete($id);
		redirect('manage/master_user/index');
	}

	function delete_all($p, $o) {
		$this->user_model->delete_all();
		redirect('manage/master_user/index');
	}

	function ajax($id=null) {
		if($id == 'delete_photo') {
			$user_id = $this->input->get('user_id');
			$user_photo = $this->input->get('user_photo');
			//
			$result = $this->user_model->delete_photo($user_id, $user_photo);
			$callback = 'false';
			if($result) $callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}
	}
	
}