<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_usergroup extends CI_Controller{

	var $active_menu = 'master_user';
	var $menu_url = 'master_usergroup';

	function __construct() {
		parent::__construct();
		//
		$this->config_model->validate_login(@$this->menu_url);
		//
        $this->load->model('usergroup_model');
        $this->load->model('menu_dashboard_model');
	}
	
	function index($p=1, $o=0) {	
		$header = $this->config_model->general();
		$footer = $this->config_model->footer();
		// get dinamis menu
		$header['top_menu_dashboard'] = $this->menu_dashboard_show_model->get_all_menu_parent_show('1');
		//
		$data['p'] = $p;
		$data['o'] = $o;
		$data['ses_search'] = @$_SESSION['ses_search'];
		//
		$data['paging'] = $this->usergroup_model->paging_usergroup($p,$o);
		$data['main'] = $this->usergroup_model->list_usergroup($o, $data['paging']->offset, $data['paging']->per_page);
		$data['profile']  = $this->config_model->get_profile_user_login();
		//
		$this->load->view('manage/template/header',$header);		
		$this->load->view('manage/master_usergroup/index',$data);
		$this->load->view('manage/template/footer',$footer);
	}

	function form($p=1, $o=0, $id=null) {	
		$header = $this->config_model->general();
		$footer = $this->config_model->footer();
		// get dinamis menu
		$header['top_menu_dashboard'] = $this->menu_dashboard_show_model->get_all_menu_parent_show('1');
		//
		$data['id'] = $id;
		$data['p'] = $p;
		$data['o'] = $o;
		//
		if($id != '') {
			$data['main'] = $this->usergroup_model->get_usergroup_md5($id);
			$data['form_action'] = site_url('manage/master_usergroup/update/'.$p.'/'.$o.'/'.$data['main']['usergroup_id']);
		} else {
			$data['main'] = array();
			$data['form_action'] = site_url('manage/master_usergroup/insert');
		}
		//
		$this->load->view('manage/template/header',$header);		
		$this->load->view('manage/master_usergroup/form',$data);
		$this->load->view('manage/template/footer',$footer);
	}

	function config($p=1, $o=0, $id=null) {	
		$header = $this->config_model->general();
		$footer = $this->config_model->footer();
		// get dinamis menu
		$header['top_menu_dashboard'] = $this->menu_dashboard_show_model->get_all_menu_parent_show('1');
		//
		$data['p'] = $p;
		$data['o'] = $o;
		$data['id'] = $id;
		//
		$data['main'] = $this->usergroup_model->get_usergroup_md5($id);
		$data['list_menu_dashboard'] = $this->menu_dashboard_model->get_all_menu_dashboard($data['main']['usergroup_id']);
		$data['form_action'] = site_url('manage/master_usergroup/update_config/'.$p.'/'.$o.'/'.$data['main']['usergroup_id']);
		//
		$this->load->view('manage/template/header',$header);		
		$this->load->view('manage/master_usergroup/config',$data);
		$this->load->view('manage/template/footer',$footer);
	}
	
	function search() {
		$ses_txt_search = $this->input->post('ses_txt_search');			
		//
		$_SESSION['ses_search']['ses_txt_search'] = ($ses_txt_search != '') ? anti_injection($ses_txt_search,'all') : false;
		//
		redirect('manage/master_usergroup/index');
	}

	function update_config($p, $o, $id) {
		$this->usergroup_model->insert_config($id);
		redirect('manage/master_usergroup/index');
	}

	function insert() {
		$this->usergroup_model->insert();
		redirect('manage/master_usergroup/index');
	}

	function update($p, $o, $id) {
		$this->usergroup_model->update($id);
		redirect('manage/master_usergroup/index');
	}

	function delete($p, $o, $id) {
		$this->usergroup_model->delete($id);
		redirect('manage/master_usergroup/index');
	}

	function delete_all($p, $o) {
		$this->usergroup_model->delete_all();
		redirect('manage/master_usergroup/index');
	}
	
}