<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Not_found_404 extends CI_Controller{

    function __construct() {
        parent::__construct();
        //
    }

    function index() {  
        $header = $this->config_model->general();
        $footer = $this->config_model->footer();
        //
        $data = "";
        //
        $this->load->view('not_found_404/header',$header);        
        $this->load->view('not_found_404/index',$data);
        $this->load->view('not_found_404/footer',$footer);
    }
    
}